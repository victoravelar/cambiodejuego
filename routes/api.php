<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api', 'throttle:120,1')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => '/v1', 'namespace' => 'InternalAPI'], function () {
    Route::get('/match/bar', 'MatchBarController@matchbar');
    Route::get('/match/results', 'MatchBarController@results');
    Route::get('/competitions', 'CompetitionsController@leagues');
    Route::get('/user/local/team', 'ProfileController@local');
    Route::get('/user/uefa/team', 'ProfileController@uefa');
    Route::get('/blog', 'BlogController@posts');
    Route::get('sponsorsPerSection', 'DataController@sponsorsPerSection');
});

Route::group(['prefix' => '/v1', 'namespace' => 'Helium'], function () {
    Route::namespace('Wizards')->group(function () {
        // Related to new seasons wizard
        Route::get('/wizards/new_season/teams/{competition}', 'NewSeasonController@getTeams');
        Route::post('/wizards/new_season/{competition}', 'NewSeasonController@newSeason');

        Route::post('/wizards/new_season/teams/{season}', 'NewSeasonController@saveTeams');
        // Get possible games for the season.
        Route::get('/wizards/new_season/games/{season}', 'NewSeasonController@getGames');
    });
});

Route::group(['namespace' => 'Website'], function () {
    Route::get('/match/bar/sponsor', 'SiteController@getBarSponsor');
});

Route::prefix('v2')->namespace('SPA')->group(function () {
    Route::get('settings', 'SettingsController@settings');
    Route::prefix('fixtures')->group(function () {
        Route::get('/upcoming', 'FixturesController@upcoming');
        Route::get('/results', 'FixturesController@results');
        Route::get('/timeline', 'FixturesController@timeline');
    });
    Route::get('sponsors', 'SponsorsController@static');
    Route::prefix('sections')->group(function () {
        Route::get('/', 'SectionsController@sections');
        Route::get('render', 'SectionsController@render');
    });

    Route::get('feed', 'FeedController@feed');
    Route::get('boards', 'LeaderboardController@boards');
});
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

/**
 * Website routes.
 */
Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});


Route::get('/home', 'HomeController@index')->name('home');

/*
 * Internal API
 */
Route::group(['namespace' => 'Website'], function () {
    Route::get('/', 'SiteController@frontPage')->name('website.frontpage');
    Route::get('/sharePost', 'PostController@sharePost')->name('website.sharePost');
    Route::get('/blog', 'PostController@blog')->name('website.blog');
    Route::get('/nota/{post}', 'PostController@article')->name('website.article');
    Route::resource('profile', 'ProfileController')->only(['store', 'create', 'edit']);
    Route::get('/match/results', 'MatchsController@results')->name('games.results');
    Route::get('/match/results/{game}', 'MatchsController@single')->name('games.single');
    Route::get('/section/{section}', 'SectionController@show')->name('section.show');
    Route::get('leagues/{competition}', 'SiteController@showLeague')->name('website.competition.show');
});

Route::group(['prefix' => 'helium', 'namespace' => 'Helium'], function () {
    // Getters
    Route::get('/formal', 'APIController@formal');
    Route::post('article/bodyContent/image', 'APIController@bodyImage');
    Route::post('newArticle', 'PostController@store');
    Route::put('/aprove/article/{post}', 'PostController@approve')->name('posts.approve');
    Route::put('/review/article/{post}', 'PostController@review')->name('posts.review');
    Route::get('/teams/{team}/edit', 'DataController@editTeam')->name('team.edit');
    Route::put('/teams/{team}/update', 'DataController@updateTeam')->name('team.update');
    Route::get('/add-result/games/{game}', 'GamesController@create')->name('games.result');
    Route::post('/add-result/games/{game}', 'GamesController@store')->name('games.result-add');
    Route::get('/games/{game}', 'GamesController@show')->name('games.show');
    Route::get('/games/{game}/edit', 'GamesController@edit')->name('games.edit');
    Route::post('/games/{game}/edit', 'GamesController@update')->name('games.update');

    Route::middleware('auth')->prefix('sections/{section}/control')->group(function () {
        Route::resource('sponsors', 'SponsorController');
    });
});

Route::middleware('auth')->prefix('helium')->namespace('Helium')->group(function () {
    Route::get('/', 'HomeController@index')->name('helium.dashboard');
    Route::resource('posts', 'PostController');
    Route::resource('competitions', 'CompetitionsController');
    Route::resource('sections', 'SectionsController');
    Route::get('details/{competition}/create', 'DetailsController@create')->name('details.create');
    Route::post('details/{competition}/create', 'DetailsController@store')->name('details.store');
    Route::get('details/{detail}/edit', 'DetailsController@edit')->name('details.edit');
    Route::put('details/{detail}', 'DetailsController@update')->name('details.update');
    Route::get('board/generate/season/{season}', 'BoardController@init')->name('board.init');
    Route::get('board/{leaderboard}/edit', 'BoardController@edit')->name('board.edit');
    Route::put('board/{leaderboard}', 'BoardController@update')->name('board.update');
    Route::resource('products', 'ProductController');
    Route::get('wizards', 'WizardController@entry')->name('wizards');
    Route::namespace('Wizards') ->group(function () {
        Route::get('wizards/new_season', 'NewSeasonController@start')->name('wizards.new_season');
        Route::post('/wizards/new_season/{season}/games', 'NewSeasonController@saveGames')->name('wizard.load.games');
    });
});

Route::group(['namespace' => 'InternalAPI'], function () {
    Route::get('/products/list', 'ProductController@index')->name('product.list');
});

/*
 * Providers
 */

//Routes for socialite
Route::get('auth/{provider}', 'LoginController@redirectToProvider');
Route::get('auth/{provider}/callback', 'LoginController@handleProviderCallback');

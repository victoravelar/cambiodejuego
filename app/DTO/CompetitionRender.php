<?php
/**
 * Created by PhpStorm.
 * User: vavelar
 * Date: 31.07.18
 * Time: 22:28
 */

namespace App\DTO;


class CompetitionRender implements \JsonSerializable
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $logo;

    /**
     * @var integer
     */
    protected $id;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return CompetitionRender
     */
    public function setName(string $name): CompetitionRender
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getLogo(): string
    {
        return $this->logo;
    }

    /**
     * @param string $logo
     * @return CompetitionRender
     */
    public function setLogo(string $logo): CompetitionRender
    {
        $this->logo = $logo;
        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return CompetitionRender
     */
    public function setId(int $id): CompetitionRender
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'logo' => $this->getLogo()
        ];
    }
}
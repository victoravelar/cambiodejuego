<?php

namespace App\DTO;

class PresentSections implements \JsonSerializable
{
    /**
     * @var SectionRender[]
     */
    protected $sections;

    /**
     * @return SectionRender[]
     */
    public function getSections(): array
    {
        return $this->sections;
    }

    /**
     * @param SectionRender[] $sections
     * @return PresentSections
     */
    public function setSections(array $sections): PresentSections
    {
        $this->sections = $sections;
        return $this;
    }

    /**
     * @param SectionRender $section
     * @return PresentSections
     */
    public function addSection(SectionRender $section): PresentSections
    {
        $this->sections[] = $section;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return $this->getSections();
    }
}
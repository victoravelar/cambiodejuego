<?php

namespace App\DTO;

class SectionRender implements \JsonSerializable
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $media;

    /**
     * @var CompetitionRender[]
     */
    protected $competitions;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return SectionRender
     */
    public function setName(string $name): SectionRender
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getMedia(): string
    {
        return $this->media;
    }

    /**
     * @param string $media
     * @return SectionRender
     */
    public function setMedia(string $media): SectionRender
    {
        $this->media = $media;
        return $this;
    }

    /**
     * @return CompetitionRender[]
     */
    public function getCompetitions(): array
    {
        return $this->competitions;
    }

    /**
     * @param CompetitionRender[] $competitions
     * @return SectionRender
     */
    public function setCompetitions(array $competitions): SectionRender
    {
        $this->competitions = $competitions;
        return $this;
    }

    /**
     * @param CompetitionRender $competition
     * @return SectionRender
     */
    public function addCompetition(CompetitionRender $competition): SectionRender
    {
        $this->competitions[] = $competition;
        return $this;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return [
            'name' => $this->getName(),
            'media' => $this->getMedia(),
            'competitions' => $this->getCompetitions()
        ];
    }
}
<?php

namespace App\BusinessCase\Board;

use App\Leaderboard;
use App\Season;

class PositionsBusinessCase
{
    /**
     * Adjusts the positions based on points and goals difference.
     *
     * @param Season $season
     */
    public function adjustPositions(Season $season)
    {
        // Step 1: Sort based on points > goals difference
        $newBoard = Leaderboard::where('season_id', $season->id)->orderByDesc('points')->orderByDesc('goalDifference')->orderByDesc('playedGames')->get();

        // Step 2: Save the board entries.
        foreach ($newBoard as $key => $item) {
            $newPos = $key + 1;
            $this->updateDiscrimination($newPos, $item);
        }
    }

    /**
     * When the position is not the same updates the board record.
     *
     * @param int $newPos
     * @param Leaderboard $item
     */
    private function updateDiscrimination($newPos, Leaderboard $item)
    {
        $item->update(['position' => $newPos]);
    }
}
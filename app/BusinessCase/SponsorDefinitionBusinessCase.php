<?php

namespace App\BusinessCase;

use App\Section;
use App\Sponsor;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class SponsorDefinitionBusinessCase
{
    /**
     * Returns an object with the sponsors for this impression.
     *
     * @param Section $section
     * @param bool $mergeWithMain
     * @return Collection
     */
    public function getSponsorsForSection(Section $section, $mergeWithMain = false)
    {
        $presentableSponsors = $this->sponsorsForImpression(
            $this->checkSponsorIsStillValid(
                $section->sponsor->where('is_active', true)
            )
        );

        return $presentableSponsors;
    }

    /**
     * Check to see if the active sponsors are still valid.
     *
     * @param Collection $activeSponsors
     * @return Collection
     */
    private function checkSponsorIsStillValid(Collection $activeSponsors)
    {
        $sponsors = $activeSponsors->filter(function (Sponsor $sponsor, $key) {
            $firstDayOfValidity = Carbon::parse($sponsor->valid_from)->setTime(0,0,0)->getTimestamp();
            $lastDayOfValidity = Carbon::parse($sponsor->valid_to)->getTimestamp();
            $now = Carbon::today()->setTime(23,59,59)->getTimestamp();
            return $lastDayOfValidity >= $now && $firstDayOfValidity <= $now;
        });

        return $sponsors;
    }

    private function sponsorsForImpression(Collection $validSponsors)
    {
        $result = [];
        /**
         * @var string $zone
         * @var Collection $sponsors
         */
        foreach ($validSponsors->groupBy('zone') as $zone => $sponsors) {
            $result[$zone] = $sponsors->random();
        }

        return collect($result);
    }
}
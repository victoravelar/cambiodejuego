<?php

namespace App\BusinessCase;

use App\Detail;
use App\Game;
use App\Leaderboard;
use Illuminate\Support\Facades\DB;

class GameResultBusinessCase
{
    /**
     * @var int
     */
    const POINTS_WINNER = 3;

    /**
     * @var int
     */
    const POINTS_TIE = 1;

    /**
     * @var Game
     */
    protected $game;

    /**
     * @var array
     */
    protected $data;

    /**
     * @var string
     */
    protected $result;

    /**
     * @var Detail
     */
    private $details;

    /**
     * GameResultBusinessCase constructor.
     *
     * @param Game $game
     * @param array $data
     * @param $details
     */
    public function __construct(Game $game, array $data, Detail $details)
    {
        $this->game = $game;
        $this->data = $data;
        $this->result = $this->resolveGameResult();
        $this->details = $details;
    }

    /**
     * @return string
     */
    public function getResult(): string
    {
        return $this->result;
    }

    /**
     * Performs the distribution according to the result.
     *
     * @param string $result
     */
    public function distributePointsForResult(string $result)
    {
        $this->saveScore();

        switch ($result) {
            case $result === "homeTeam":
                $this->executeHomeIsWinner();
                break;
            case $result === "awayTeam":
                $this->executeAwayIsWinner();
                break;
            case $result === "tie":
                $home = $this->game->season->board->where('external_team_id', $this->game->homeTeam->external_id)->first();
                $home = DB::table('leaderboards')->whereId($home->id);
                $away = $this->game->season->board->where('external_team_id', $this->game->awayTeam->external_id)->first();
                $away = DB::table('leaderboards')->whereId($away->id);
                $this->executeTie($home, 'home');
                $this->executeTie($away, 'away');
                break;
        }
    }

    /**
     * Executes the main changes in the leaderboard.
     *
     * @param Leaderboard $boardEntryWinner
     * @param Leaderboard $boardEntryLoser
     * @param int $goalsDifferenceWinner
     * @param $winner
     * @param $loser
     */
    protected function executeBoardChangesForWinnerAndLooser($boardEntryWinner, $boardEntryLoser, $goalsDifferenceWinner, $winner, $loser)
    {
        $boardEntryWinner->increment('playedGames');
        $boardEntryWinner->increment('goals', $this->data["goals_{$winner}_total"]);
        $boardEntryWinner->increment('goalsAgainst', $this->data["goals_{$loser}_total"]);
        $boardEntryWinner->increment('wins');

        if (is_null($this->details->kickoff_from) or !is_null($this->details->kickoff_from) and $this->details->kickoff_from > $this->game->matchDay) {
            $boardEntryWinner->increment('points', self::POINTS_WINNER);
        }

        $boardEntryLoser->increment('playedGames');
        $boardEntryLoser->increment('goals', $this->data["goals_{$loser}_total"]);
        $boardEntryLoser->increment('goalsAgainst', $this->data["goals_{$winner}_total"]);
        $boardEntryLoser->increment('losses');

        $boardEntryWinner->increment('goalDifference', $goalsDifferenceWinner);
        $boardEntryLoser->decrement('goalDifference', $goalsDifferenceWinner);
    }

    /**
     * Resolve the game result to proceed with the
     * points assignment.
     *
     * @return string
     */
    private function resolveGameResult()
    {
        if ($this->data['goals_home_total'] > $this->data['goals_away_total']) {
            return "homeTeam";
        } else if ($this->data['goals_home_total'] === $this->data['goals_away_total']) {
            return "tie";
        } else {
            return "awayTeam";
        }
    }

    /**
     * Saves or updates the score instance for the given result.
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    private function saveScore()
    {
        $this->game->update(['finished' => true]);
        return $this->game->score()->updateOrCreate(
            ['game_id' => $this->game->id],
            [
                'goals_home_total' => $this->data['goals_home_total'],
                'goals_away_total' => $this->data['goals_away_total'],
                'goals_home_halftime' => $this->data['goals_home_halftime'],
                'goals_away_halftime' => $this->data['goals_away_halftime']
            ]
        );
    }

    /**
     * Execute when the home team wins.
     */
    private function executeHomeIsWinner()
    {
        /** @var Leaderboard $boardEntryWinner */
        $boardEntryWinner = $this->game->season->board->where('external_team_id', $this->game->homeTeam->external_id)->first();

        $boardEntryWinner = DB::table('leaderboards')->whereId($boardEntryWinner->id);

        /** @var Leaderboard $boardEntryLoser */
        $boardEntryLoser = $this->game->season->board->where('external_team_id', $this->game->awayTeam->external_id)->first();

        $boardEntryLoser = DB::table('leaderboards')->whereId($boardEntryLoser->id);

        $goalsDifferenceWinner = (int) $this->calculateGoalDifference((int) $this->data['goals_home_total'], (int) $this->data['goals_away_total']);

        $this->executeBoardChangesForWinnerAndLooser($boardEntryWinner, $boardEntryLoser, $goalsDifferenceWinner, 'home', 'away');
    }

    /**
     * Execute when the away team wins.
     */
    private function executeAwayIsWinner()
    {
        /** @var Leaderboard $boardEntryWinner */
        $boardEntryWinner = $this->game->season->board->where('external_team_id', $this->game->awayTeam->external_id)->first();

        $boardEntryWinner = DB::table('leaderboards')->whereId($boardEntryWinner->id);

        /** @var Leaderboard $boardEntryLoser */
        $boardEntryLoser = $this->game->season->board->where('external_team_id', $this->game->homeTeam->external_id)->first();

        $boardEntryLoser = DB::table('leaderboards')->whereId($boardEntryLoser->id);

        $goalsDifferenceWinner = (int) $this->calculateGoalDifference((int) $this->data['goals_away_total'], (int) $this->data['goals_home_total']);

        $this->executeBoardChangesForWinnerAndLooser($boardEntryWinner, $boardEntryLoser, $goalsDifferenceWinner, 'away', 'home');
    }

    /**
     * Execute when the game ends in a draw.
     *
     * @param $board
     * @param string $t
     */
    private function executeTie($board, string $t)
    {
        $alt = "away";

        if ($t !== "home") {
            $alt = "home";
        }

        $board->increment('playedGames');
        $board->increment('goals', $this->data["goals_{$t}_total"]);
        $board->increment('goalsAgainst', $this->data["goals_{$alt}_total"]);
        $board->increment('draws');
        if (is_null($this->details->kickoff_from) or !is_null($this->details->kickoff_from) and $this->details->kickoff_from > $this->game->matchDay) {
            $board->increment('points', self::POINTS_TIE);
        }
    }

    /**
     * Calculates the goal difference for the winner.
     *
     * @param int $winner
     * @param int $looser
     * @return integer
     */
    private function calculateGoalDifference($winner, $looser)
    {
        return $winner - $looser;
    }
}
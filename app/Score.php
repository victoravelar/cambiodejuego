<?php

namespace App;

use App\Traits\WithUuid;
use Illuminate\Database\Eloquent\Model;

class Score extends Model
{
    use WithUuid;

    /**
     * {@inheritdoc}
     */
    protected $fillable = [
        'goals_home_total', 'goals_away_total', 'goals_home_halftime', 'goals_away_halftime',
        'external_source',
    ];

    /**
     * {@inheritdoc}
     */
    protected $hidden = [
        'external_source', 'game_id',
    ];

    /**
     * A game must have a result.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function game()
    {
        return $this->belongsTo(Game::class);
    }
}

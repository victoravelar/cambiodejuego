<?php

namespace App\Console\Commands;

use App\Game;
use Avelar\OpenFutData\Client;
use Illuminate\Console\Command;

class UpdateOutdatedGamesCommand extends Command
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * @var int
     */
    protected $total = 0;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cdj:games-update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Moves the games from next games to finished games';

    /**
     * Create a new command instance.
     *
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        parent::__construct();

        $this->client = $client;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $games = Game::concluded()->get();

        $games = $games->each(function ($match) {
            $fixture = $this->client->getFixtureById($match->game_external_id);

            if ($fixture['fixture']->status == 'FINISHED') {
                $this->markGameAsFinished($match);

                $score = $this->scoreBuilder($fixture['fixture']);

                $match->score()->create($score);

                $this->total++;
            }
            sleep(1);
        });

        $this->output->writeln("<info>A total of {$this->total} games where updated!</info>");
    }

    /**
     * Builds a new score entry.
     *
     * @param $game
     * @return array
     */
    protected function scoreBuilder($game): array
    {
        dump($game->status);

        if (isset($game->result->halfTime)) {
            $scoreEntry = [
                'goals_home_total' => $game->result->goalsHomeTeam,
                'goals_away_total' => $game->result->goalsAwayTeam,
                'goals_home_halftime' => $game->result->halfTime->goalsHomeTeam,
                'goals_away_halftime' => $game->result->halfTime->goalsAwayTeam,
                'external_source' => 'football-data.org',
            ];
        } else {
            $scoreEntry = [
                'goals_home_total' => $game->result->goalsHomeTeam,
                'goals_away_total' => $game->result->goalsAwayTeam,
                'external_source' => 'football-data.org',
            ];
        }

        return $scoreEntry;
    }

    protected function markGameAsFinished($match)
    {
        $match->update(['finished' => true]);
    }
}

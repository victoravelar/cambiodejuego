<?php

namespace App\Console\Commands;

use App\Team;
use Avelar\OpenFutData\Client;
use Illuminate\Console\Command;

class ImportPlayersForTeam extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cdj:team-players {id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'imports players for failed teams.';

    /**
     * @var Client
     */
    protected $client;

    /**
     * Create a new command instance.
     *
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        parent::__construct();
        $this->client = $client;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $players = $this->client->getTeamPlayers($this->argument('id'));

        /** @var Team $team */
        $team = Team::firstOrCreate(['external_id' => (int) $this->argument('id')]);

        $addPlayer = [];

        foreach ($players['players'] as $player) {
            $p = $this->buildPlayerEntry($player, $this->argument('id'));

            $team->players()->create($p);


            $this->output->writeln($player->name);
        }
        $this->output->writeln('Players saved');
    }

    /**
     * @param $player
     * @param $teamId
     * @return array
     */
    protected function buildPlayerEntry($player, $teamId)
    {
        return [
            'name' => $player->name,
            'position' => $player->position,
            'birthday' => $player->dateOfBirth,
            'nationality' => $player->nationality,
            'contract_until' => $player->contractUntil,
            'marketValue' => $player->marketValue,
            'number' => $player->jerseyNumber,
            'team_external_id' => $teamId,
            'external_source' => 'football-data.org',
        ];
    }
}

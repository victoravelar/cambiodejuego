<?php

namespace App\Console\Commands;

use App\Competition;
use App\Detail;
use App\Game;
use App\Leaderboard;
use App\Player;
use App\Score;
use App\Season;
use App\Section;
use App\Team;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use TCG\Voyager\Models\Setting;

class CleanupExternalDataAndReRunImport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cdj:cleanup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cleanup the open football data and rerun all the required commands';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Game::truncate();
        $this->output->writeln('Games truncated');
        Team::truncate();
        $this->output->writeln('Teams truncated');
        Player::truncate();
        $this->output->writeln('Players truncated');
        Competition::truncate();
        $this->output->writeln('Competitions truncated');
        Season::truncate();
        $this->output->writeln('Seasons truncated');
        Leaderboard::truncate();
        $this->output->writeln('Leaderboard truncated');
        Score::truncate();
        $this->output->writeln('Score truncated');
        Detail::truncate();
        $this->output->writeln('Details truncated');
        Setting::truncate();
        $this->output->writeln('Settings truncated');
        Section::truncate();
        $this->output->writeln('Sections truncated');
        DB::table('season_team')->truncate();
        $this->output->writeln('Seasons Team pivot truncated');

//         Run in order manually for better response and performance

//        Artisan::call('cdj:competitions-fbd');
//        Artisan::call('db:seed', ['--class' => 'CambioDeJuegoDataSeeder']);
//        Artisan::call('cdj:single-import', ['id' => 467]);
//        Artisan::call('cdj:league-tables');
    }
}

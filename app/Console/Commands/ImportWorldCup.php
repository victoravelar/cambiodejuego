<?php

namespace App\Console\Commands;

use App\Season;
use App\Team;
use App\Competition;
use Avelar\OpenFutData\Client;
use DateTime;
use Illuminate\Console\Command;

class ImportWorldCup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cdj:single-import {id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to import all the worldcup related data from FBD';

    /**
     * The Football Data API wrapper.
     *
     * @var Client
     */
    protected $client;

    /**
     * Create a new command instance.
     *
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        parent::__construct();

        $this->client = $client;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $competition = $this->client->getCompetition($this->argument('id'));

        $this->saveData($competition);
    }

    protected function saveData($competition)
    {
        $cId = $this->parseExternalId($competition);

        $entry = $this->buildCompEntry($competition, $cId);

        $worldCup = Competition::create($entry);
        $worldCup->detail()->create([
            'champions_positions' => 0,
            'champions_rematch' => 0,
            'europa_positions' => 0,
            'downgrade_positions' => 0,
            'is_uefa' => false,
            'is_cup' => true,
        ]);

        $season = $worldCup->seasons()->create(['year' => 2018, 'is_current' => true]);

        $this->output->writeln("<info>{$worldCup->name} imported to the database.</info>");

        $this->saveNationalTeamsClasified($cId, $season);

        $this->saveFixturesData($cId, $season);
    }

    /**
     * Extracts the external id from the API link.
     *
     * @param $competition
     * @return int
     */
    protected function parseExternalId($competition): int
    {
        return (int) str_replace('http://api.football-data.org/v1/competitions/', '', $competition['_links']->self->href);
    }

    /**
     * Transforms the object into our DB structure.
     *
     * @param $competition
     * @param $cId
     * @return array
     */
    protected function buildCompEntry($competition, $cId): array
    {
        return [
            'section_id' => 7,
            'name' => $competition['caption'],
            'acronym' => $competition['league'],
            'total_games' => $competition['numberOfGames'],
            'total_teams' => $competition['numberOfTeams'],
            'total_rounds' => $competition['numberOfMatchdays'],
            'external_source' => 'football-data.org',
            'external_id' => (int) $cId,
        ];
    }

    /**
     * Saves the national teams classified to the worldcup.
     *
     * @param int $externalId
     * @param Season $worldCup
     */
    protected function saveNationalTeamsClasified($externalId, $worldCup)
    {
        $teams = $this->client->getCurrentTeams($externalId);

        foreach ($teams['teams'] as $team) {
            $teamId = $this->parseTeamId($team);

            $teamEntry = $this->buildTeamEntry($team, $teamId);

            $newTeam = Team::firstOrCreate($teamEntry, $teamEntry);

            $worldCup->teams()->attach($newTeam->id);
        }
    }

    /**
     * Extract the external id from the API link.
     *
     * @param $team
     * @return int
     */
    protected function parseTeamId($team): int
    {
        return (int) str_replace('http://api.football-data.org/v1/teams/', '', $team->_links->self->href);
    }

    /**
     * Transforms the object into our DB structure.
     *
     * @param $team
     * @param $teamId
     * @return array
     */
    protected function buildTeamEntry($team, $teamId)
    {
        return [
            'name' => $team->name,
            'acronym' => (! $team->code == null) ? $team->code : strtoupper(substr($team->name, '0', '3')),
            'shortName' => $team->name,
            'team_logo' => $team->crestUrl,
            'external_source' => 'football-data.org',
            'external_id' => (int) $teamId,
            'active' => true,
        ];
    }

    /**
     * @param $competitionExternalId
     * @param $season
     * @throws \Exception
     */
    protected function saveFixturesData($competitionExternalId, $season)
    {
        $fixtures = $this->client->getLeagueFixtures($competitionExternalId);

        foreach ($fixtures['fixtures'] as $fixture) {
            $fixtureEntry = $this->buildFixtureEntry($fixture, $competitionExternalId);

            $season->games()->create($fixtureEntry);
        }
    }

    /**
     * @param $fixture
     * @param $competitionExternalId
     * @return array
     */
    protected function buildFixtureEntry($fixture, $competitionExternalId)
    {
        $fixtureId = $this->parseFixtureId($fixture);

        $date = new DateTime($fixture->date);

        $fixtureEntry = [
            'finished' => ($date < new DateTime('now')) ? true : false,
            'game_external_id' => (int) $fixtureId,
            'competition_external_id' => (int) $competitionExternalId,
            'homeTeam_external_id' => str_replace('http://api.football-data.org/v1/teams/', '', $fixture->_links->homeTeam->href),
            'awayTeam_external_id' => str_replace('http://api.football-data.org/v1/teams/', '', $fixture->_links->awayTeam->href),
            'gameDate' => $date->format('Y-m-d'),
            'gameTime' => $date->format('H:i'),
            'matchDay' => $fixture->matchday,
            'external_source' => 'football-data.org',
        ];

        return $fixtureEntry;
    }

    /**
     * @param $fixture
     * @return string
     */
    protected function parseFixtureId($fixture): string
    {
        return str_replace('http://api.football-data.org/v1/fixtures/', '', $fixture->_links->self->href);
    }
}

<?php

namespace App\Console\Commands;

use App\Team;
use Avelar\OpenFutData\Client;
use Illuminate\Console\Command;

class ImportPlayers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cdj:players {comp}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * @var Client
     */
    private $client;

    /**
     * Create a new command instance.
     *
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        parent::__construct();
        $this->client = $client;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $teams = Team::where('competition_id', $this->argument('comp'))->get();

        foreach ($teams as $team) {
//            dd($team->external_id);
            $this->saveTeamCurrentPlayers((int) $team->external_id, $team);
            $this->output->writeln("<info>Players for team {$team->name} saved!</info>");
        }
    }

    protected function saveTeamCurrentPlayers($teamId, $team)
    {
        $players = $this->client->getTeamPlayers($teamId);

        foreach ($players['players'] as $player) {
            $playerEntry = $this->buildPlayerEntry($player, $teamId);

            $this->savePlayerData($team, $playerEntry);
        }
    }

    protected function buildPlayerEntry($player, $teamId)
    {
        return [
            'name' => $player->name,
            'position' => $player->position,
            'birthday' => $player->dateOfBirth,
            'nationality' => $player->nationality,
            'contract_until' => $player->contractUntil,
            'marketValue' => $player->marketValue,
            'number' => $player->jerseyNumber,
            'team_external_id' => $teamId,
            'external_source' => 'football-data.org',
        ];
    }

    protected function savePlayerData($team, $playerEntry)
    {
        $team->players()->create($playerEntry);
    }
}

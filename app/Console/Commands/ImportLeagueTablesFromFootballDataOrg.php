<?php

namespace App\Console\Commands;

use App\Competition;
use App\Leaderboard;
use Avelar\OpenFutData\Client;
use Illuminate\Console\Command;

class ImportLeagueTablesFromFootballDataOrg extends Command
{
    const SUPPORTED = [
        'Primera Division 2017',
        'Premier League 2017/18',
        'Serie A 2017/18',
        '1. Bundesliga 2017/18',
        'Ligue 1 2017/18',
        'Champions League 2017/18',
        'Eredivisie 2017/18',
        'DFB-Pokal 2017/18',
        'World Cup 2018 Russia',
    ];

    /**
     * @var Client
     */
    protected $client;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cdj:league-tables';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update the supported leagues tables.';

    /**
     * Create a new command instance.
     *
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        parent::__construct();

        $this->client = $client;
    }

    /**
     * Execute the console command.
     * @throws \Exception
     */
    public function handle()
    {
        Leaderboard::truncate();

        $comps = Competition::whereIn('name', self::SUPPORTED)->with('detail')->get();

        foreach ($comps as $comp) {
            try {
                $table = $this->client->getCompetitionTable($comp->external_id);
            } catch (\Exception $exception) {
                if ($exception->getCode() === 404) {
                    continue;
                } else if ($exception->getCode() === 403) {
                    echo $exception->getMessage();
                }

                throw new \Exception($exception->getMessage(), $exception->getCode());
            }

            $matchday = $table['matchday'];
            $season = $comp->seasons->where('is_current', true)->first();

            if ($comp->detail->is_cup) {
                $positions = collect($table['standings']);


                foreach ($positions as $group) {
                    foreach ($group as $team) {
                        try {
                            $boardEntry = [
                                'external_competition_id' => $comp->external_id,
                                'matchday' => ($matchday) ? $matchday : 0,
                                'position' => $team->rank,
                                'external_team_id' => $team->teamId,
                                'playedGames' => $team->playedGames,
                                'points' => $team->points,
                                'goals' => $team->goals,
                                'goalsAgainst' => $team->goalsAgainst,
                                'goalDifference' => $team->goalDifference,
                                'wins' => null,
                                'draws' => null,
                                'losses' => null,
                                'group' => $team->group,
                            ];

                            $season->board()->create($boardEntry);
                        } catch (\Exception $exception) {
                            echo $exception->getMessage().PHP_EOL;
                        }
                    }
                }
            } else {
                $positions = collect($table['standing']);

                try {
                    foreach ($positions as $position) {
                        $boardEntry = [
                            'external_competition_id' => $comp->external_id,
                            'matchday' => ($matchday) ? $matchday : 0,
                            'position' => $position->position,
                            'external_team_id' => (int) $this->parseExternalTeamId($position),
                            'playedGames' => $position->playedGames,
                            'points' => $position->points,
                            'goals' => $position->goals,
                            'goalsAgainst' => $position->goalsAgainst,
                            'goalDifference' => $position->goalDifference,
                            'wins' => $position->wins,
                            'draws' => $position->draws,
                            'losses' => $position->losses,
                            'group' => 'league',
                        ];

                        $season->board()->create($boardEntry);
                    }
                } catch (\Exception $exception) {
                    echo $exception->getMessage().PHP_EOL;
                }
            }

            $this->output->writeln("{$comp->name} table in the database.");
        }
    }

    /**
     * @param $position
     * @return string
     */
    protected function parseExternalTeamId($position): string
    {
        return str_replace('http://api.football-data.org/v1/teams/', '', $position->_links->team->href);
    }
}

<?php

namespace App\Console\Commands;

use App\Season;
use App\Team;
use DateTime;
use App\Competition;
use Avelar\OpenFutData\Client;
use Illuminate\Console\Command;

class ImportCompetitionsFootballDataOrg extends Command
{
    /**
     * Competitions supported by Cambio de Juego.
     *
     * @var array
     */
    const SUPPORTED = [
        'Primera Division 2017',
        'Premier League 2017/18',
        'Serie A 2017/18',
        '1. Bundesliga 2017/18',
        'Ligue 1 2017/18',
        'Champions League 2017/18',
        'Eredivisie 2017/18',
        'DFB-Pokal 2017/18',
    ];

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cdj:competitions-fbd';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Imports the supported competitions from football-data.org';

    /**
     * The Football Data API wrapper.
     *
     * @var Client
     */
    protected $client;

    /**
     * Create a new command instance.
     *
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        parent::__construct();

        $this->client = $client;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        $res = $this->client->getAllCompetitions();

        $supportedCompetitions = $res->filter(function ($league) {
            return in_array($league->caption, self::SUPPORTED);
        });

        $data = $this->saveData($supportedCompetitions);

        return $data;
    }

    /**
     * @param $competitions
     * @return array
     * @throws \Exception
     */
    public function saveData($competitions): array
    {
        return $this->saveCompetitionsData($competitions);
    }

    /**
     * @param $competitions
     * @return array
     * @throws \Exception
     */
    protected function saveCompetitionsData($competitions): array
    {
        $data = [];

        foreach ($competitions as $league) {
            try {
                $competitionExternalId = $this->parseCompetitionId($league);

                $entry = $this->buildLeagueEntry($league, $competitionExternalId);

                $comp = Competition::create($entry);

                $this->output->writeln("<info>{$comp->name} imported to the database.</info>");

                $season = $this->saveCompetitionsSeasonData($comp, $league);

                $this->saveCompetitionsCurrentTeamsData($competitionExternalId, $season);

                $this->output->writeln("<info>{$comp->name} teams imported to the database.</info>");

                $this->saveFixturesData($competitionExternalId, $season);

                $this->output->writeln("<info>{$comp->name} fixtures imported to the database.</info>");

                $data[] = $entry;
            } catch (\Throwable $exception) {
                if ($exception->getCode() < 500) {
                    echo $exception->getMessage();
                }
            }
        }

        $total = count($data);

        $this->output->writeln("<info> {$total} competitions are now in your local database </info>");

        return $data;
    }

    /**
     * @param $comp
     * @param $league
     * @return mixed
     */
    protected function saveCompetitionsSeasonData($comp, $league)
    {
        return $comp->seasons()->create(['year' => $league->year, 'is_current' => true]);
    }

    /**
     * @param $externalId
     * @param Season $comp
     */
    protected function saveCompetitionsCurrentTeamsData($externalId, $comp)
    {
        $teams = $this->client->getCurrentTeams($externalId);

        foreach ($teams['teams'] as $team) {
            $teamId = $this->parseTeamId($team);

            $teamEntry = $this->buildTeamEntry($team, $teamId);

            $newTeam = Team::firstOrCreate($teamEntry, $teamEntry);

            $comp->teams()->attach($newTeam->id);

            $this->output->writeln('Created: '. $newTeam->name);

            if ($newTeam->wasRecentlyCreated) {
                $this->saveTeamCurrentPlayers($teamId, $newTeam);
            }
        }
    }

    /**
     * @param $team
     * @param $teamId
     * @return array
     */
    protected function buildTeamEntry($team, $teamId): array
    {
        $teamEntry = [
            'name' => $team->name,
            'acronym' => (! $team->code == null) ? $team->code : strtoupper(substr($team->name, '0', '3')),
            'shortName' => $team->shortName,
            'team_logo' => $team->crestUrl,
            'external_source' => 'football-data.org',
            'external_id' => (int) $teamId,
            'active' => true,
        ];

        return $teamEntry;
    }

    /**
     * @param $league
     * @param $competitionExternalId
     * @return array
     */
    protected function buildLeagueEntry($league, $competitionExternalId): array
    {
        $entry = [
            'name' => $league->caption,
            'acronym' => $league->league,
            'total_games' => $league->numberOfGames,
            'total_teams' => $league->numberOfTeams,
            'total_rounds' => $league->numberOfMatchdays,
            'external_source' => 'football-data.org',
            'external_id' => (int) $competitionExternalId,
        ];

        if ($league->league == 'PD') {
            $entry = array_merge($entry, ['section_id' => 2]);
        }

        if ($league->league == 'PL') {
            $entry = array_merge($entry, ['section_id' => 3]);
        }

        if ($league->league == 'SA') {
            $entry = array_merge($entry, ['section_id' => 4]);
        }

        if ($league->league == 'BL1' or $league->league == 'DFB') {
            $entry = array_merge($entry, ['section_id' => 5]);
        }

        if ($league->league == 'FL1') {
            $entry = array_merge($entry, ['section_id' => 6]);
        }

        if ($league->league == 'CL') {
            $entry = array_merge($entry, ['section_id' => 8]);
        }

        if ($league->league == 'DED') {
            $entry = array_merge($entry, ['section_id' => 8]);
        }

        return $entry;
    }

    /**
     * @param $league
     * @return string
     */
    protected function parseCompetitionId($league): string
    {
        return str_replace('http://api.football-data.org/v1/competitions/', '', $league->_links->self->href);
    }

    /**
     * @param $fixture
     * @return string
     */
    protected function parseFixtureId($fixture): string
    {
        return str_replace('http://api.football-data.org/v1/fixtures/', '', $fixture->_links->self->href);
    }

    /**
     * @param $team
     * @return string
     */
    protected function parseTeamId($team): string
    {
        return str_replace('http://api.football-data.org/v1/teams/', '', $team->_links->self->href);
    }

    /**
     * @param $fixture
     * @param $competitionExternalId
     * @return array
     */
    protected function buildFixtureEntry($fixture, $competitionExternalId): array
    {
        $fixtureId = $this->parseFixtureId($fixture);

        $date = new DateTime($fixture->date);

        $fixtureEntry = [
            'finished' => ($date < new DateTime('now')) ? true : false,
            'game_external_id' => (int) $fixtureId,
            'competition_external_id' => (int) $competitionExternalId,
            'homeTeam_external_id' => str_replace('http://api.football-data.org/v1/teams/', '', $fixture->_links->homeTeam->href),
            'awayTeam_external_id' => str_replace('http://api.football-data.org/v1/teams/', '', $fixture->_links->awayTeam->href),
            'gameDate' => $date->format('Y-m-d'),
            'gameTime' => $date->format('H:i'),
            'matchDay' => $fixture->matchday,
            'external_source' => 'football-data.org',
        ];

        return $fixtureEntry;
    }

    /**
     * @param $game
     * @return array
     */
    protected function scoreBuilder($game): array
    {
        if (isset($game->result->halfTime)) {
            $scoreEntry = [
                'goals_home_total' => $game->result->goalsHomeTeam,
                'goals_away_total' => $game->result->goalsAwayTeam,
                'goals_home_halftime' => $game->result->halfTime->goalsHomeTeam,
                'goals_away_halftime' => $game->result->halfTime->goalsAwayTeam,
                'external_source' => 'football-data.org',
            ];
        } else {
            $scoreEntry = [
                'goals_home_total' => $game->result->goalsHomeTeam,
                'goals_away_total' => $game->result->goalsAwayTeam,
                'external_source' => 'football-data.org',
            ];
        }

        return $scoreEntry;
    }

    /**
     * @param $fixture
     * @param $game
     */
    protected function saveScoreData($fixture, $game)
    {
        if ($fixture->status == 'FINISHED') {
            $scoreEntry = $this->scoreBuilder($fixture);

            $game->score()->create($scoreEntry);
        }
    }

    /**
     * @param $competitionExternalId
     * @param $season
     * @throws \Exception
     */
    protected function saveFixturesData($competitionExternalId, $season)
    {
        $fixtures = $this->client->getLeagueFixtures($competitionExternalId);

        foreach ($fixtures['fixtures'] as $fixture) {
            $fixtureEntry = $this->buildFixtureEntry($fixture, $competitionExternalId);

            $game = $season->games()->create($fixtureEntry);

            $this->saveScoreData($fixture, $game);
        }
    }

    protected function saveTeamCurrentPlayers($teamId, $team)
    {
        $players = $this->client->getTeamPlayers($teamId);

        foreach ($players['players'] as $player) {
            $playerEntry = $this->buildPlayerEntry($player, $teamId);

            $this->savePlayerData($team, $playerEntry);
        }
    }

    /**
     * @param $player
     * @param $teamId
     * @return array
     */
    protected function buildPlayerEntry($player, $teamId)
    {
        return [
            'name' => $player->name,
            'position' => $player->position,
            'birthday' => $player->dateOfBirth,
            'nationality' => $player->nationality,
            'contract_until' => $player->contractUntil,
            'marketValue' => $player->marketValue,
            'number' => $player->jerseyNumber,
            'team_external_id' => $teamId,
            'external_source' => 'football-data.org',
        ];
    }

    /**
     * @param $team
     * @param $playerEntry
     */
    protected function savePlayerData($team, $playerEntry)
    {
        $player = $team->players()->create($playerEntry);

        $this->output->writeln('<info>'.$player->name.'</info>');
    }
}

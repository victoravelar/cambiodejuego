<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detail extends Model
{
    /**
     * {@inheritdoc}
     */
    protected $fillable = [
      'competition_id', 'champions_positions', 'champions_rematch',
        'europa_positions', 'downgrade_positions', 'is_uefa', 'is_cup', 'kickoff_from' ];

    /**
     * Each detail belongs to a competition.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function competition()
    {
        return $this->belongsTo(Competition::class, 'competition_id');
    }

    /**
     * Field generator.
     *
     * @return array
     */
    public function fieldType()
    {
        return [
            'champions_positions' => ['type' => 'number', 'label' => 'Posiciones para copa de campeones de la confederación'],
            'champions_rematch' => ['type' => 'number', 'label' => 'Reclasificación para copa de campeones de la confederación'],
            'europa_positions' => ['type' => 'number', 'label' => 'Posiciones para copa de europa'], //Solo ligas europeas
            'downgrade_positions' => ['type' => 'number', 'label' => 'Numero de equipos que descienden por temporada'],
            'is_uefa' => ['type' => 'checkbox', 'label' => 'Competicion de la UEFA'],
            'is_cup' => ['type' => 'checkbox', 'label' => 'Esta competición es una copa?'],
            'kickoff_from' => ['type' => 'number', 'label' => 'Desde que jornada comienza el knockout stage'],
        ];
    }
}

<?php

namespace App;

use App\Traits\Sponsored;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Cviebrock\EloquentSluggable\Sluggable;

class Section extends Model
{
    use Sluggable,
        Sponsored;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'active', 'description', 'slug', 'section_id', 'level'
    ];

    /**
     * Eager load the posts for each section.
     *
     * @var array
     */
    protected $with = [
        'posts',
    ];

    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function path()
    {
        return "/section/{$this->slug}";
    }

    /**
     * One section can have many posts.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    /**
     * One section can have many competitions.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function competitions()
    {
        return $this->hasMany(Competition::class);
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name',
                'onUpdate' => true,
                'unique' => true,
            ],
        ];
    }

    /**
     * Returns only the active sections.
     *
     * @param Builder $builder
     * @return Builder
     */
    public function scopeActivated(Builder $builder)
    {
        return $builder->where('active', true);
    }
}

<?php

namespace App;

use App\Traits\RoutedByUuid;
use App\Traits\WithUuid;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Season extends Model
{
    use WithUuid,
        RoutedByUuid;

    protected $fillable = [
        'year', 'is_current',
    ];

    /**
     * Each season belongs to a league.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function competition()
    {
        return $this->belongsTo(Competition::class);
    }

    /**
     * A season is composed by many games.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function games()
    {
        return $this->hasMany(Game::class);
    }

    /**
     * A season has many teams.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function teams()
    {
        return $this->belongsToMany(Team::class)->withTimestamps();
    }

    /**
     * A season board consist of many leaderboard entries.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function board()
    {
        return $this->hasMany(Leaderboard::class);
    }

    /**
     * Retrieves the current season.
     *
     * @param Builder $query
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function scopeCurrent(Builder $query)
    {
        return $query->where('is_current', true)->limit(1)->get();
    }
}

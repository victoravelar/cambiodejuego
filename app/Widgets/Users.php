<?php

namespace App\Widgets;

use App\User;
use Illuminate\Support\Str;
use Arrilot\Widgets\AbstractWidget;

class Users extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $count = User::count();
        $string = trans_choice('voyager.dimmer.user', $count);

        return view('voyager::dimmer', array_merge($this->config, [
            'icon'   => 'voyager-group',
            'title'  => "{$count} {$string}",
            'text'   => __('voyager.dimmer.user_text', ['count' => $count, 'string' => Str::lower($string)]),
            'button' => [
                'text' => __('voyager.dimmer.user_link_text'),
                'link' => route('voyager.users.index'),
            ],
            'image' => asset('widgets/users-bg.jpeg'),
        ]));
    }
}

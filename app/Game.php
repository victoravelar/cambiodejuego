<?php

namespace App;

use App\Traits\RoutedByUuid;
use App\Traits\WithUuid;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Game extends Model
{
    use WithUuid,
        RoutedByUuid;

    /**
     * {@inheritdoc}
     */
    protected $fillable = [
        'finished', 'game_external_id', 'competition_external_id', 'gameDate', 'gameTime',
        'homeTeam_external_id', 'awayTeam_external_id', 'external_source', 'matchDay',
    ];

    /**
     * {@inheritdoc}
     */
    protected $hidden = [
        'game_external_id', 'competition_external_id',
        'external_source',
        'season_id',
    ];

    protected $with = ['score', 'homeTeam', 'awayTeam'];

    /**
     * A game is part of a season.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function season()
    {
        return $this->belongsTo(Season::class);
    }

    /**
     * A game will end up with a unique score.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function score()
    {
        return $this->hasOne(Score::class);
    }

    /**
     * A game must have a home team.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function homeTeam()
    {
        return $this->hasOne(Team::class, 'external_id', 'homeTeam_external_id');
    }

    /**
     * A game must have an away team.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function awayTeam()
    {
        return $this->hasOne(Team::class, 'external_id', 'awayTeam_external_id');
    }

    /**
     * Query to filter the games to display in the matchbar.
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeMatchbar(Builder $query)
    {
        $carbon = new Carbon();
        return $query->where('finished', false)
            ->where('gameDate','>=', $carbon->format('Y-m-d'))
            ->orderBy('gameDate')
            ->orderBy('gameTime')
            ->with(['homeTeam', 'awayTeam', 'score']);
    }

    /**
     * Query to filter the games to display in the results.
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeResults(Builder $query)
    {
        return $query->where('finished', '=', true)
            ->orderByDesc('gameDate')
            ->orderByDesc('gameTime')
            ->with(['homeTeam', 'awayTeam', 'score']);
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeConcluded(Builder $query)
    {
        return $query
            ->where('finished', false)
            ->where('gameDate', '<', Carbon::now()->format('Y-m-d'));
    }

    /**
     * Route to add a result to the given name.
     * @return string
     */
    public function addResult()
    {
        return route('games.result', $this->uuid);
    }

    /**
     * Route to edit a game.
     * @return string
     */
    public function editGame()
    {
        return route('games.edit', $this->uuid);
    }
}

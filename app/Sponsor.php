<?php

namespace App;

use App\Traits\WithUuid;
use Illuminate\Database\Eloquent\Model;

class Sponsor extends Model
{
    use WithUuid;

    /**
     * {@inheritdoc}
     *
     * @var array
     */
    protected $fillable = [
        'name', 'link', 'media', 'is_active', 'valid_from', 'valid_to', 'zone',
    ];

    protected $hidden = [
        'uuid', 'sponsorable_type', 'id', 'is_active',
        'sponsorable_id', 'created_at', 'updated_at',
        'zone'
    ];

    /**
     * Many items may be subject of sponsoring, such as pages, sections,
     * or other items in the company.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function sponsorable()
    {
        return $this->morphTo();
    }

    /**
     * Returns the MEDIA CONTENT URI.
     *
     * @return string
     */
    public function mediaURI()
    {
        return "/storage/{$this->media}";
    }
}

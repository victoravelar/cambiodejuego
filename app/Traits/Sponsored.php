<?php

namespace App\Traits;

use App\Sponsor;
use Illuminate\Database\Eloquent\Relations\MorphMany;

trait Sponsored
{
    /**
     * Any item in the site can be sponsorable.
     *
     * @return MorphMany
     */
    public function sponsor(): MorphMany
    {
        return $this->morphMany(Sponsor::class, 'sponsorable');
    }
}

<?php

namespace App\Traits;

trait RoutedByUuid
{
    /**
     * Set the router path parameter to uuid instead of id.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'uuid';
    }
}
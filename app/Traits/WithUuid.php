<?php

namespace App\Traits;

use Webpatser\Uuid\Uuid;

trait WithUuid
{
    /**
     * Execute while creating the model instance.
     */
    protected static function boot() {
        parent::boot();

        static::creating(function($model) {
            $model->uuid = Uuid::generate()->string;
        });
    }
}
<?php

namespace App;

use App\Traits\WithUuid;
use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
    use WithUuid;

    /**
     * Attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'position', 'birthday', 'nationality', 'number', 'team_external_id', 'external_source',
        'contract_until', 'marketValue',
    ];

    /**
     * One player belongs to a team.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|mixed
     */
    public function team()
    {
        return $this->belongsTo(Team::class);
    }
}

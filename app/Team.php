<?php

namespace App;

use App\Traits\RoutedByUuid;
use App\Traits\WithUuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Team extends Model
{
    use WithUuid,
        RoutedByUuid;

    /**
     * {@inheritdoc}
     */
    protected $fillable = [
        'name', 'acronym', 'shortName', 'team_logo', 'external_source', 'external_id', 'active',
    ];

    /**
     * {@inheritdoc}
     */
    protected $hidden = [
        'external_source', 'active',
        'competition_id',
    ];

    protected $with = [
        'players',
    ];

    /**
     * A team belongs to a given competition.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function season()
    {
        return $this->belongsToMany(Season::class)->withTimestamps();
    }

    /**
     * A team has many players.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function players()
    {
        return $this->hasMany(Player::class);
    }

    /**
     * Returns all the teams from Liga MX.
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeLocale(Builder $query)
    {
        return $query->whereBetween('external_id', [9000,9999]);
    }

    /**
     * Return all the teams from UEFA competitions.
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeEurope(Builder $query)
    {
        return $query->whereNotIn('external_id', [900,467]);
    }

    /**
     * Returns the correct url for different logo sources.
     * @return string
     */
    public function pathForLogo()
    {
        if (preg_match('/teams/', $this->team_logo)) {
            return env('APP_URL').'/storage/'.$this->team_logo;
        }

        return $this->team_logo;
    }

    /**
     * Method to configure forms for this model.
     */
    public function fieldType()
    {
        return [
            'name' => ['type' => 'text', 'label' => 'Nombre completo del equipo'],
            'acronym' => ['type' => 'text', 'label' => 'Acronimo del equipo'],
            'shortName' => ['type' => 'text', 'label' => 'Nombre corto'],
            'active' => ['type' => 'checkbox', 'label' =>  'El equipo esta activo en esta competencia?', 'checked' => $this->active],
            'team_logo' => ['type' => 'file', 'label' => 'Actualizar logo del equipo'],
        ];
    }
}

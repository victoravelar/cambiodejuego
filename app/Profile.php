<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * The user inherits the leagues news from the selected team.
 *
 * Class Profile
 */
class Profile extends Model
{
    protected $fillable = [
        'league_id', 'local_team_id', 'uefa_team_id',
    ];
    /**
     * Loads the profile with all the preferences.
     *
     * @var array
     */
    protected $with = [
        'localTeam', 'uefaTeam', 'league',
    ];

    /**
     * Every profile belongs to only one user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * A user can select one local league team.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function localTeam()
    {
        return $this->hasOne(Team::class, 'id', 'local_team_id');
    }

    /**
     * A user can select a UEFA team.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function uefaTeam()
    {
        return $this->hasOne(Team::class, 'id', 'uefa_team_id');
    }

    /**
     * A user can select a league.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function league()
    {
        return $this->hasOne(Competition::class, 'id', 'league_id');
    }
}

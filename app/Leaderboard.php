<?php

namespace App;

use App\Traits\RoutedByUuid;
use App\Traits\WithUuid;
use Illuminate\Database\Eloquent\Model;

class Leaderboard extends Model
{
    use WithUuid,
        RoutedByUuid;

    /**
     * {@inheritdoc}
     */
    protected $fillable = [
        'external_competition_id',
        'matchday',
        'position',
        'external_team_id',
        'playedGames',
        'points',
        'goals',
        'goalsAgainst',
        'goalDifference',
        'wins',
        'draws',
        'losses',
        'group',
    ];

    protected $with = [
        'team', 'season',
    ];

    /**
     * Each leaderboard entry belongs to a season.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function season()
    {
        return $this->belongsTo(Season::class);
    }

    /**
     * Each leaderboard entry references a team.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function team()
    {
        return $this->hasOne(Team::class, 'external_id', 'external_team_id');
    }

    public function fieldType()
    {
        return [
            'matchday' => ['type' => 'text', 'label' => 'Jornada'],
            'playedGames' => ['type' => 'text', 'label' => 'Juegos jugados'],
            'wins' => ['type' => 'text', 'label' => 'Partidos ganados'],
            'draws'  => ['type' => 'text', 'label' => 'Partidos empatados'],
            'losses'  => ['type' => 'text', 'label' => 'Partidos perdidos'],
            'goals' => ['type' => 'text', 'label' => 'Goles anotados'],
            'goalsAgainst' => ['type' => 'text', 'label' => 'Goles recibidos'],
            'goalDifference' => ['type' => 'text', 'label' => 'Diferencia de goles'],
            'points' => ['type' => 'text', 'label' => 'Puntos'],
            'position' => ['type' => 'text', 'label' => 'Posición'],
            'group' => ['type' => 'text', 'label' => 'Grupo'],
        ];
    }
}

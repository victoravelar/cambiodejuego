<?php

namespace App;

use App\Traits\RoutedByUuid;
use App\Traits\WithUuid;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Competition extends Model
{
    use WithUuid,
        RoutedByUuid;

    /**
     * {@inheritdoc}
     */
    protected $fillable = [
        'section_id', 'name', 'acronym', 'total_games',
        'total_rounds', 'total_teams', 'league_logo', 'league_website',
        'external_source', 'external_id', 'is_open',
    ];

    /**
     * {@inheritdoc}
     */
    protected $hidden = [
        'external_source', 'external_id',
    ];

    protected $with = [
        'detail',
    ];

    /**
     * A competitions can have many seasons.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function seasons()
    {
        return $this->hasMany(Season::class);
    }

    /**
     * One competition can be included in a section.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function section()
    {
        return $this->belongsTo(Section::class);
    }

    /**
     * Each competition has details (think of this as config).
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function detail()
    {
        return $this->hasOne(Detail::class, 'competition_id');
    }

    /**
     * Resolves and returns the correct path for the league logo.
     *
     * @return string
     */
    public function pathForLogo()
    {
        if (preg_match('/competitions/', $this->league_logo)) {
            return env('APP_URL').'/storage/'.$this->league_logo;
        }

        return $this->league_logo;
    }

    /**
     * Retrieves the active competitions.
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeActive(Builder $query)
    {
        return $query->where('is_open', true);
    }

    /**
     * Returns whether a section is open or not.
     *
     * @return bool
     */
    public function isOpen()
    {
        return $this->is_open;
    }
}

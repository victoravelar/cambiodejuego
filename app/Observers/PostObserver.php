<?php

namespace App\Observers;

use TCG\Voyager\Models\Post;

class PostObserver
{
    /**
     * Creates a single tag per every meta keyword defined by the user.
     *
     * @param Post $post
     */
    public function created(Post $post)
    {
        //
    }

    /**
     * Creates a single tag per every meta keyword defined by the user.
     *
     * @param Post $post
     */
    public function updated(Post $post)
    {
        //
    }
}

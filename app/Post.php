<?php

namespace App;

use Carbon\Carbon;
use App\Traits\Sponsored;
use Illuminate\Database\Eloquent\Builder;
use Cviebrock\EloquentSluggable\Sluggable;
use TCG\Voyager\Models\Post as VoyagerPost;

class Post extends VoyagerPost
{
    use Sponsored,
        Sluggable;

    protected $with = ['authorId'];

    /**
     * A post belongs to a section.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function section()
    {
        return $this->belongsTo(Section::class);
    }

    /**
     * The post wildcard is a slug.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * Get the post URI.
     *
     * @return string
     */
    public function path()
    {
        return "/nota/{$this->slug}";
    }

    /**
     * Returns the full path to a post.
     *
     * @return string
     */
    public function getPostURI()
    {
        return env('APP_URL').$this->path();
    }

    /**
     * Returns the full media url.
     *
     * @return string
     */
    public function postMedia()
    {
        if (preg_match('/http/', $this->image)) {
            return $this->image;
        }

        return env('APP_URL')."/storage/{$this->image}";
    }

    /**
     * @param Builder $builder
     * @return Builder
     */
    public function scopeBlog(Builder $builder)
    {
        return $builder->where('status', '=', parent::PUBLISHED)
            ->orderByDesc('created_at');
    }

    /**
     * @param Builder $builder
     * @return Builder
     */
    public function scopeAuthored(Builder $builder)
    {
        return $builder->where('author_id', auth()->user()->id)
            ->orderByDesc('created_at');
    }

    /**
     * Display the post date on a readable format.
     *
     * @return string
     */
    public function readableAgo()
    {
        Carbon::setLocale(env('locale', 'es'));

        return Carbon::parse($this->created_at)->diffForHumans();
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title',
                'unique' => true,
                'onUpdate' => true,
            ],
        ];
    }
}

<?php

namespace App\Http\Resources\SPA;

use Illuminate\Http\Resources\Json\Resource;

class FeedResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'media' => $this->postMedia(),
            'posted_at' => $this->readableAgo(),
            'seo_title' => $this->seo_title,
            'excerpt' => $this->excerpt,
            'path' => $this->path(),
            'section_id' => $this->section_id
        ];
    }
}

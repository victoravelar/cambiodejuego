<?php

namespace App\Http\Resources\SPA;

use Illuminate\Http\Resources\Json\Resource;

class RenderableSectionsResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
           $this->name
        ];
    }
}

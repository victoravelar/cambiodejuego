<?php

namespace App\Http\Resources\SPA;

use Illuminate\Http\Resources\Json\Resource;

class SettingResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->display_name,
            'content' => $this->value,
            'key' => str_replace('.', '_', $this->key)
        ];
    }
}

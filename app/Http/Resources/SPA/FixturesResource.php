<?php

namespace App\Http\Resources\SPA;

use Illuminate\Http\Resources\Json\Resource;

class FixturesResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'fixture_time' => $this->gameTime,
            'fixture_date' => $this->gameDate,
            'home' => [
                'name' => $this->homeTeam->shortName,
                'acronym' => $this->homeTeam->acronym,
                'shield' => $this->homeTeam->pathForLogo(),
            ],
            'away' => [
                'name' => $this->awayTeam->shortName,
                'acronym' => $this->awayTeam->acronym,
                'shield' => $this->awayTeam->pathForLogo(),
            ],
            'score' => [
                'local' => optional($this->score)->goals_home_total,
                'away' => optional($this->score)->goals_away_total,
            ]
        ];
    }
}

<?php

namespace App\Http\Controllers\Helium;

use App\Post;
use App\Section;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use TCG\Voyager\Facades\Voyager;
use App\Http\Controllers\Controller;

class PostController extends Controller
{
    /**
     * PostController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Voyager::can('browse_posts')) {
            $posts = Post::authored()->get();

            return view('helium.posts.index', compact('posts'));
        }

        return redirect('home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Voyager::can('add_posts')) {
            $sections = Section::activated()->get();

            return view('helium.posts.create', compact('sections'));
        }

        return redirect('home');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function store(Request $request)
    {
        if (! $request->ajax()) {
            abort(404);
        }

        $request->validate([
            'title' => 'required',
            'image' => 'required',
            'caption' => 'required',
            'excerpt' => 'required',
            'body' => 'required',
            'meta_description' => 'required',
            'meta_keywords' => 'required',
            'seo_title' => 'required',
            'status' => 'required',
            'category_id' => 'required',
            'section_id' => 'required',
        ]);

        $hashed_path = $this->uploadMainImage($request);

        switch (auth()->user()->role->name) {
            case 'writer':
                $status = (request('status') === true) ? 'PENDING' : 'DRAFT';
                break;
            case 'editor':
            case 'owner':
            case 'admin':
                $status = (request('status') === true) ? 'PUBLISHED' : 'DRAFT';
                break;
            default:
                $status = 'DRAFT';
        }

        Post::create([
            'title' => request('title'),
            'image' => $hashed_path,
            'caption' => request('caption'),
            'excerpt' => request('excerpt'),
            'body' => request('body'),
            'meta_description' => request('meta_description'),
            'meta_keywords' => request('meta_keywords'),
            'seo_title' => request('seo_title'),
            'status' => $status,
            'category_id' => request('category_id'),
            'section_id' => request('section_id'),
            'author_id' => auth()->user()->id,
        ]);

        return response()->json(['publised' => true]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Post $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        return view('helium.posts.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Post $post
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function update(Request $request, Post $post)
    {
        $request->validate([
            'title' => 'required|max:255',
            'caption' => 'required',
            'excerpt' => 'required|min:100',
            'body' => 'required|string',
            'meta_description' => 'required',
            'meta_keywords' => 'required',
            'seo_title' => 'required|min:25|max:255',
        ]);

        $post->update([
            'title' => request('title'),
            'caption' => request('caption'),
            'excerpt' => request('excerpt'),
            'body' => request('body'),
            'meta_description' => request('meta_description'),
            'meta_keywords' => request('meta_keywords'),
            'seo_title' => request('seo_title'),
        ]);

        return redirect()->route('posts.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Post $post
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Post $post)
    {
        $post->delete();

        return redirect()->route('posts.index');
    }

    /**
     * @param Request $request
     * @return string
     * @throws \Exception
     */
    private function uploadMainImage(Request $request): string
    {
        $img = Image::make($request->input('image'))->encode('jpg', 75);

        $timestamp = (new \DateTime())->getTimestamp();

        $hashed_path = "posts/cdj-" . md5(uniqid() . str_random(25)) . "-{$timestamp}.jpg";

        try {
            Storage::disk(config('voyager.storage.disk'))->put($hashed_path, $img);
        } catch (\Exception $e) {
            throw $e;
        }
        return $hashed_path;
    }

    /**
     * Approves a post to be shown live.
     *
     * @param Post $post
     * @return \Illuminate\Http\RedirectResponse
     */
    public function approve(Post $post)
    {
        if ($post->status !== 'PUBLISHED') $post->update(['status' => 'PUBLISHED']);

        return redirect()->route('helium.dashboard');
    }

    /**
     * Sends a post to reviewed by and editor.
     *
     * @param Post $post
     * @return \Illuminate\Http\RedirectResponse
     */
    public function review(Post $post)
    {
        if ($post->status !== 'PENDING') $post->update(['status' => 'PENDING']);

        return redirect()->route('helium.dashboard');
    }
}

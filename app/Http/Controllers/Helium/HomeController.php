<?php

namespace App\Http\Controllers\Helium;

use App\Post;
use App\Http\Controllers\Controller;
use TCG\Voyager\Facades\Voyager;

class HomeController extends Controller
{
    public function index()
    {
        if(Voyager::can('add_posts')) {
            $drafts = Post::where('author_id', auth()->user()->id)->where('status', 'DRAFT')->get();
            $pub = Post::where('author_id', auth()->user()->id)->where('status', 'PUBLISHED')->get();
            $revision = Post::where('author_id', auth()->user()->id)->where('status', 'PENDING')->get();

            $toReview = [];

            if (Voyager::can('delete_posts')) {
                $toReview = Post::where('author_id', '!=', auth()->user()->id)->where('status', 'PENDING')->get();
            }

            return view('helium.dashboard.landing', compact('drafts', 'pub', 'revision', 'toReview'));
        }

        return redirect()->route('home');
    }
}

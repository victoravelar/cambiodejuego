<?php

namespace App\Http\Controllers\Helium;

use App\Http\Controllers\Controller;
use App\Http\Requests\SponsorRequest;
use App\Section;
use Illuminate\Support\Facades\Storage;

class SponsorController extends Controller
{
    const FILE_PATH = 'sponsors';

    /**
     * @param Section $section
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Section $section)
    {
        $sponsors = $section->sponsor;

        return view('helium.sponsors.index', compact('sponsors', 'section'));
    }

    /**
     * @param Section $section
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Section $section)
    {
        return view('helium.sponsors.create', compact('section'));
    }

    public function store(SponsorRequest $request, Section $section)
    {
        $mediaPath = $this->saveFileInStorage($request->file('media'), $section->name);

        $section->sponsor()->create([
            'name' => request('name'),
            'link' => request('link'),
            'valid_from' => request('valid_from'),
            'valid_to' => request('valid_to'),
            'is_active' => request('is_active'),
            'media' => $mediaPath,
            'zone' => request('zone'),
        ]);

        return redirect()->route('sponsors.index', $section->slug);
    }

    /**
     * @param $file
     * @param string $section
     * @return mixed
     */
    protected function saveFileInStorage($file, $section = 'z')
    {
        $path = Storage::disk('public')->putFile(self::FILE_PATH.'/'.$section, $file);

        return $path;
    }
}

<?php

namespace App\Http\Controllers\Helium;

use App\Section;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use TCG\Voyager\Models\Category;
use App\Http\Controllers\Controller;
use App\Http\Resources\FormalCollection;

class APIController extends Controller
{
    /**
     * APIController constructor.
     */
    public function __construct()
    {
        $this->middleware('api');
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function formal(Request $request)
    {
        if (! $request->ajax()) {
            abort(404);
        }

        $categories = Category::all();
        $sections = Section::activated()->get();

        $da = [
            'categories' => $categories,
            'sections' => $sections,
        ];

        $d = collect($da);

        return new FormalCollection($d);
    }

    /**
     * AJAX post body image upload.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function bodyImage(Request $request)
    {
        $request->validate([
            'image' => 'required|image'
        ]);

        $img = Image::make($request->file('image'))->encode('jpg', 75);

        $timestamp = (new \DateTime())->getTimestamp();

        $hashed_path = "posts/body/cdj-" . md5(uniqid() . str_random(25)) . "-{$timestamp}.jpg";

        try {
            Storage::disk(config('voyager.storage.disk'))->put($hashed_path, $img);

        } catch (\Exception $e) {
            throw $e;
        }

        return response()->json(['success' => true, 'url' => "/storage/{$hashed_path}"]);
    }
}

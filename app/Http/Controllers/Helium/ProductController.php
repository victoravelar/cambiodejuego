<?php

namespace App\Http\Controllers\Helium;

use App\Http\Requests\ProductRequest;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = DB::table('products')->orderByDesc('active')->simplePaginate(15);
        return view('helium.products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('helium.products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProductRequest $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function store(ProductRequest $request)
    {
        Product::create([
            'name' => $request->input('name'),
            'url' => $request->input('url'),
            'image' => $this->uploadMainImage($request),
            'price' => $request->input('price'),
            'active' => (bool) $request->input('active')
        ]);

        return redirect()->route('products.index');
    }

    /**
     * Display the specified resource.
     *
     * @param Product $product
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Product $product)
    {
        return view('helium.products.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Product $product
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Product $product)
    {
        return view('helium.products.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ProductRequest $request
     * @param Product $product
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function update(ProductRequest $request, Product $product)
    {

        if ($request->hasFile('image')) {
            $product->update([
                'image' => $this->uploadMainImage($request),
            ]);
        }

        $product->update([
            'name' => $request->input('name'),
            'url' => $request->input('url'),
            'price' => $request->input('price'),
            'active' => (bool) $request->input('active')
        ]);

        return redirect()->route('products.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Product $product
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Product $product)
    {
        $product->delete();

        return redirect()->route('products.index');
    }

    /**
     * @param Request $request
     * @return string
     * @throws \Exception
     */
    private function uploadMainImage(Request $request): string
    {
        $img = Image::make($request->file('image'))->encode('jpg', 75);

        $timestamp = (new \DateTime())->getTimestamp();

        $hashed_path = "products/cdj-shop-" . md5(uniqid() . str_random(25)) . "-{$timestamp}.jpg";

        try {
            Storage::disk(config('voyager.storage.disk'))->put($hashed_path, $img);
        } catch (\Exception $e) {
            throw $e;
        }
        return $hashed_path;
    }
}

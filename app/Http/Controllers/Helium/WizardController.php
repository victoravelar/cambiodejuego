<?php

namespace App\Http\Controllers\Helium;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WizardController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function entry()
    {
        return view('helium.wizards.index');
    }
}

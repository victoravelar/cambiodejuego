<?php

namespace App\Http\Controllers\Helium;

use App\Leaderboard;
use App\Season;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BoardController extends Controller
{
    /**
     * BoardController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Initializes a leaderboard for the given season.
     *
     * @param Season $season
     * @return \Illuminate\Http\RedirectResponse
     */
    public function init(Season $season)
    {
        if (is_null($season->teams)) return redirect()->route('competitions.index');

        $teams = $season->teams;

        $season->load(['competition', 'competition.detail']);

        if ($season->competition->detail->is_cup) {
            $this->createCupBoard($season, $teams);
        } else {
            $this->createLeagueBoard($season, $teams);
        }

        return redirect()->route('competitions.show', $season->competition->uuid);
    }

    /**
     * @param Season $season
     * @param $teams
     */
    protected function createLeagueBoard(Season $season, $teams)
    {
        $rank = 1;
        foreach ($teams->sortBy('name') as $team) {
           $season->board()->create([
                'external_competition_id' => $season->competition->external_id,
                'matchday' => 0,
                'position' => $rank,
                'external_team_id' => $team->external_id,
                'playedGames' => 0,
                'points' => 0,
                'goals' => 0,
                'goalsAgainst' => 0,
                'goalDifference' => 0,
                'wins' => 0,
                'draws' => 0,
                'losses' => 0,
                'group' => 'league',
            ]);
            $rank++;
        }
    }

    public function edit(Leaderboard $leaderboard)
    {
        $elements = $leaderboard->fieldType();
        $leaderboard->load('team');
        return view('helium.board.edit', compact('leaderboard', 'elements'));
    }

    public function update(Leaderboard $leaderboard, Request $request)
    {
        $leaderboard->update($request->all());

        return redirect()->route('competitions.show', $leaderboard->season->competition->uuid);
    }

    protected function createCupBoard($season, $teams)
    {
        $groups = range('A', 'H');
        $g = 0;
        $rank = 1;
        foreach ($teams->shuffle()->chunk(4) as $group) {
            foreach ($group as $team) {
                $season->board()->create([
                    'external_competition_id' => $season->competition->external_id,
                    'matchday' => 0,
                    'position' => $rank,
                    'external_team_id' => $team->external_id,
                    'playedGames' => 0,
                    'points' => 0,
                    'goals' => 0,
                    'goalsAgainst' => 0,
                    'goalDifference' => 0,
                    'wins' => 0,
                    'draws' => 0,
                    'losses' => 0,
                    'group' => $groups[$g],
                ]);
                $rank++;
            }
            $g++;
        }
    }
}

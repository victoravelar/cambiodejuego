<?php

namespace App\Http\Controllers\Helium\Wizards;

use App\Competition;
use App\Game;
use App\Season;
use App\Team;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use League\Csv\CharsetConverter;
use League\Csv\Reader;
use League\Csv\Writer;

class NewSeasonController extends Controller
{
    /**
     * Shows the wizard container & entry point.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function start()
    {
        return view('helium.wizards.new-season.container');
    }

    /**
     * Returns an object with the teams for the current season.
     *
     * @param Competition $competition
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTeams(Competition $competition, Request $request)
    {
        if (!$request->ajax()) {
            abort(403);
        }

        $teams = $competition->seasons->where('is_current', 1)->last()->teams;

        return response()->json($teams);
    }

    /**
     * Saves a new season for the given competition.
     *
     * @param Competition $competition
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function newSeason(Competition $competition, Request $request)
    {
        $request->validate([
            'year' => 'required|string|max:20'
        ]);

        $s = $competition->seasons->last();

        $s->update(['is_current' => false]);

        $newSeason = $competition->seasons()->create([
            'year' => $request->input('year'),
            'is_current' => true
        ]);

        if ($newSeason instanceof Season) {
            return response()->json(['success' => true, 'season' => $newSeason], 200);
        }

        return response()->json(['success' => false], 500);
    }

    /**
     * Saves the teams for the newly created season.
     *
     * @param Season $season
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveTeams(Season $season, Request $request)
    {
        $blacklist = ['players', 'pivot', 'created_at', 'updated_at'];

        $t = json_decode($request->input('team'), true);

        $team = array_diff_key($t, array_flip($blacklist));

        $nt = Team::firstOrCreate($team, $team);

        $season->teams()->attach($nt->id);

        if ($nt->wasRecentlyCreated) {
            $attached = true;
        } else {
            $attached = false;
        }

        return response()->json(
            ['success' => true, 'created' => $attached],
            200
        );
    }

    /**
     * Retrieves a list of all the possible game combinations for
     * the season.
     *
     * @param Season $season
     * @param Request $request
     * @throws \League\Csv\CannotInsertRecord
     */
    public function getGames(Season $season, Request $request)
    {
        $teams = $season->teams;

        $headers = ['team_1', 'team_1_id', 'team_2', 'team_2_id', 'jornada', 'fecha', 'hora', 'local_id', 'visita_id', 'grupo'];

        $csv = Writer::createFromFileObject(new \SplTempFileObject());

        $csv->insertOne($headers);

        $today = now()->addDay();

        $games = [];

        $season = $season->load(['competition', 'competition.detail']);

        $group = ($season->competition->detail->is_cup) ? 'A' : 'league';

        for ($i = 0; $i < $teams->count() - 1; $i++) {
            for ($j = $i + 1; $j < $teams->count(); $j++) {
                $games[] = [
                    $teams[$i]->name,
                    $teams[$i]->external_id,
                    $teams[$j]->name,
                    $teams[$j]->external_id,
                    null,
                    $today->format('d/m/Y'),
                    $today->format('H:i'),
                    null,
                    null,
                    $group
                ];

                if ($request->query->get('shortSeason') === false) {
                    $games[] = [
                        $teams[$j]->name,
                        $teams[$j]->external_id,
                        $teams[$i]->name,
                        $teams[$i]->external_id,
                        null,
                        $today->format('d/m/Y'),
                        $today->format('H:i'),
                        null,
                        null,
                        $group
                    ];
                }

            }
        }

        $csv->insertAll($games);

       $csv->output($season->competition->name.'.csv');
    }

    /**
     * @param Season $season
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \League\Csv\Exception
     */
    public function saveGames(Season $season, Request $request)
    {
        $csv = Reader::createFromPath($request->file('games')->getRealPath(), 'r');

        $csv->setHeaderOffset(0);

        $input_bom = $csv->getInputBOM();

        if ($input_bom === Reader::BOM_UTF16_LE || $input_bom === Reader::BOM_UTF16_BE) {
            CharsetConverter::addTo($csv, 'utf-16', 'utf-8');
        }

        foreach ($csv as $record) {

            $date = Carbon::createFromFormat('d/m/Y', $record['fecha']);
            $time = Carbon::parse($record['hora']);

            $fixtureEntry = [
                'finished' => false,
                'game_external_id' => now()->getTimestamp(),
                'competition_external_id' => (int) $season->competition->external_id,
                'homeTeam_external_id' => empty($record['local_id']) ?: null,
                'awayTeam_external_id' => empty($record['visita_id']) ?: null,
                'gameDate' => $date->format('Y-m-d'),
                'gameTime' => $time->format('H:i'),
                'matchDay' => is_null($record['jornada']) ?: 1,
                'external_source' => 'cambiodejuego.com.mx',
            ];

            $season->games()->create($fixtureEntry);
        }

        return redirect()->route('competitions.index');
    }
}

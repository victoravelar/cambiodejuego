<?php

namespace App\Http\Controllers\Helium;

use App\BusinessCase\Board\PositionsBusinessCase;
use App\BusinessCase\GameResultBusinessCase;
use App\Competition;
use App\Game;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GamesController extends Controller
{
    /**
     * @var PositionsBusinessCase
     */
    private $positionsBusiness;

    public function __construct(PositionsBusinessCase $positionsBusiness)
    {
        $this->positionsBusiness = $positionsBusiness;
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @param Game $game
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Game $game)
    {
        return view('helium.results.games', compact('game'));
    }

    /**
     * Store a newly crated resource.
     *
     * @param Game $game
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Game $game, Request $request)
    {
        $request->validate([
            'goals_home_total' => 'required|integer',
            'goals_away_total' => 'required|integer',
        ]);

        $season = $game->season->load('competition');

        $details = $season->competition->detail;

        $gameResultResolver = new GameResultBusinessCase($game, $request->all(), $details);

        $result = $gameResultResolver->getResult();

        $gameResultResolver->distributePointsForResult($result);

        $this->positionsBusiness->adjustPositions($game->season);

        return redirect()->route('competitions.show', $game->season->competition->uuid);
    }

    /**
     * Display the specified resource.
     *
     * @param Game $game
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Game $game)
    {
        return view('helium.games.show', compact('game'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Game $game
     * @return \Illuminate\Http\Response
     */
    public function edit(Game $game)
    {
        $teams = $game->season->teams;

        return view('helium.games.edit', compact('game', 'teams'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Game $game
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Game $game)
    {
        $request->validate([
            'gameDate' => 'string|required',
            'gameTime' => 'string|required'
        ]);
        $game->update($request->all());

        return redirect()->route('games.show', $game->uuid);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers\Helium;

use App\Section;
use App\Competition;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class CompetitionsController extends Controller
{
    /**
     * @var string
     */
    const FILE_PATH = 'competitions';

    /**
     * @var string
     */
    protected $filesystem;

    public function __construct()
    {
        $this->middleware('auth');
        $this->filesystem = config('voyager.storage.disk');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $competitions = Competition::all();

        return view('helium.competitions.index', compact('competitions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sections = Section::activated()->get();

        return view('helium.competitions.create', compact('sections'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'section_id' => 'required',
            'name' => 'required',
            'acronym' => 'required',
            'total_games' => 'required',
            'total_rounds' => 'required',
            'total_teams' => 'required',
            'league_logo' => 'image|required',
            'league_website' => 'required',
        ]);

        $path = $this->saveLogoInStorage($request->file('league_logo'));

        $timestamp = new \DateTime();

        $competition = Competition::create([
            'section_id' => $request->input('section_id'),
            'name' => $request->input('name'),
            'acronym' => $request->input('acronym'),
            'total_games' => $request->input('total_games'),
            'total_rounds' => $request->input('total_rounds'),
            'total_teams' => $request->input('total_teams'),
            'league_logo' => $path,
            'league_website' => $request->input('league_website'),
            'external_id' => $timestamp->getTimestamp(),
            'external_source' => 'Grupo Marotec'
        ]);

        return redirect()->route('details.create', $competition->id);
    }

    /**
     * Display the specified resource.
     *
     * @param Competition $competition
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Competition $competition)
    {
        $competition->load(['section', 'detail']);

        $season = $competition->seasons->where('is_current', true)->last();

        if ($season !== null) $season->load(['teams', 'games', 'board']);

        $elements = $competition->getFillable();

        return view('helium.competitions.show', compact('competition', 'elements', 'season'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Competition $competition
     * @return \Illuminate\Http\Response
     */
    public function edit(Competition $competition)
    {
        $sections = Section::activated()->get();

        return view('helium.competitions.edit', compact('competition', 'sections'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Competition $competition
     * @return Response
     */
    public function update(Request $request, Competition $competition)
    {
        $request->validate([
            'section_id' => 'required',
            'name' => 'required',
            'acronym' => 'required',
            'total_games' => 'required',
            'total_rounds' => 'required',
            'total_teams' => 'required',
            'league_logo' => 'image',
            'league_website' => 'required',
            'is_open' => 'boolean'
        ]);

        if ($request->hasFile('league_logo')) {
            $path = $this->saveLogoInStorage($request->file('league_logo'));
            $competition->update(['league_logo' => $path]);
        }

        if ($request->has('is_open')) {
            $competition->update(['is_open' => (bool) $request->input('is_open')]);
        } else {
            $competition->update(['is_open' => false]);
        }

        $competition->update([
            'section_id' => $request->input('section_id'),
            'name' => $request->input('name'),
            'acronym' => $request->input('acronym'),
            'total_games' => $request->input('total_games'),
            'total_rounds' => $request->input('total_rounds'),
            'total_teams' => $request->input('total_teams'),
            'league_website' => $request->input('league_website'),
        ]);

        return redirect()->route('competitions.show', $competition->uuid);
    }

    /**
     * @param $file
     * @return mixed
     */
    protected function saveLogoInStorage($file)
    {
        $path = Storage::disk($this->filesystem)->putFile(self::FILE_PATH, $file);

        return $path;
    }
}

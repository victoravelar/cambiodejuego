<?php

namespace App\Http\Controllers\Helium;

use App\Team;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class DataController extends Controller
{
    const TEAM_VALIDATION = [
        'name' => 'required|string|min:6',
        'acronym' => 'required|max:5|min:2|string',
        'shortName' => 'required|min:2|string',
        'team_logo' => 'sometimes|image',
        'active' => 'required'
    ];

    /**
     * DataController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Team $team
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editTeam(Team $team)
    {
        return view('helium.teams.edit', compact('team'));
    }

    /**
     * @param Team $team
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateTeam(Team $team, Request $request)
    {
        $request->validate(
            self::TEAM_VALIDATION
        );

        if ($file = $request->file('team_logo')) {
            $imgProcess = $this->upload($request, 'team_logo');
        } else {
            $imgProcess = ['success' => false, 'url' => ''];
        }

        if ($imgProcess['success']) {
            $team->update([
                'name' => $request->input('name'),
                'acronym' => $request->input('acronym'),
                'shortName' => $request->input('shortName'),
                'active' => (bool) $request->input('active'),
                'team_logo' => $imgProcess['url'],
            ]);
        } else {
            $team->update([
                'name' => $request->input('name'),
                'acronym' => $request->input('acronym'),
                'shortName' => $request->input('shortName'),
                'active' => (bool) $request->input('active'),
            ]);
        }

        return redirect()->route('competitions.show', $team->competition_id);
    }

    /**
     * @param Request $request
     * @param string $field
     * @return array
     */
    private function upload(Request $request, string $field): array
    {
        $img = Image::make($request->file($field))->encode('png', 75);

        $timestamp = (new \DateTime())->getTimestamp();

        $hashed_path = "teams/shields-" . md5(uniqid() . str_random(25)) . "-{$timestamp}.jpg";

        try {
            Storage::disk(config('voyager.storage.disk'))->put($hashed_path, $img);
        } catch (\Exception $e) {
            return ['success' => false, 'url' => ''];
        }
        return ['success' => true, 'url' => $hashed_path];
    }
}

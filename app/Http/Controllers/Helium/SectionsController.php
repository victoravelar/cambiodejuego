<?php

namespace App\Http\Controllers\Helium;

use App\Section;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SectionsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $sections = Section::all();

        return view('helium.sections.index', compact('sections'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|unique:sections',
            'description' => 'sometimes|max:255',
            'active' => 'boolean'
        ]);

        Section::create($request->all());

        return redirect()->route('sections.index');
    }
}

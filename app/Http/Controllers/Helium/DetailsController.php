<?php

namespace App\Http\Controllers\Helium;

use App\Competition;
use App\Detail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DetailsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create(Competition $competition)
    {
        $elements = (new Detail())->fieldType();
        return view('helium.details.create', compact('elements', 'competition'));
    }

    public function store(Competition $competition, Request $request)
    {
        $competition->detail()->create([
            'champions_positions' => (int) $request->input('champions_positions'),
            'champions_rematch' => (int) $request->input('champions_rematch'),
            'europa_positions' => (int) $request->input('europa_positions'),
            'downgrade_positions' => (int) $request->input('downgrade_positions'),
            'is_uefa' => (bool) $request->input('is_uefa'),
            'is_cup' => (bool) $request->input('is_cup'),
        ]);

        $competition->seasons()->create([
            'year' => date('Y'),
            'is_current' => true
        ]);

        return redirect()->route('competitions.show', $competition->uuid);
    }

    public function edit(Detail $detail)
    {
        $elements = (new Detail())->fieldType();
        return view('helium.details.edit', compact('elements', 'detail'));
    }

    public function update(Detail $detail, Request $request)
    {
        $detail->update([
            'champions_positions' => (int) $request->input('champions_positions'),
            'champions_rematch' => (int) $request->input('champions_rematch'),
            'europa_positions' => (int) $request->input('europa_positions'),
            'downgrade_positions' => (int) $request->input('downgrade_positions'),
            'is_uefa' => (bool) $request->input('is_uefa'),
            'is_cup' => (bool) $request->input('is_cup'),
            'kickoff_from' => (int) $request->input('kickoff_from'),
        ]);

        return redirect()->route('competitions.show', $detail->competition->uuid);
    }
}

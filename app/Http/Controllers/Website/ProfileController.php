<?php

namespace App\Http\Controllers\Website;

use App\Profile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! is_null(auth()->user()->profile)) {
            if (
                ! is_null(auth()->user()->profile->uefaTeam) &&
                ! is_null(auth()->user()->profile->localTeam) &&
                ! is_null(auth()->user()->profile->league)
            ) {
                return redirect()->route('home');
            }
        }

        return view('profile.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function store(Request $request)
    {
        $user = auth()->user();

        if (is_null($user->profile)) {
            auth()->user()->profile()->create([$request->column => $request->item]);

            return response('Created');
        } else {
            auth()->user()->profile()->update([$request->column => $request->item]);

            return response('Updated');
        }
    }

    /**
     * Edits the user's feed.
     * @param Profile $profile
     * @return \Illuminate\Http\Response
     */
    public function edit(Profile $profile)
    {
        return view('profile.create', compact('profile'));
    }
}

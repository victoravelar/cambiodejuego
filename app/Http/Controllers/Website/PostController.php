<?php

namespace App\Http\Controllers\Website;

use App\Post;
use App\Http\Controllers\Controller;
use Artesaos\SEOTools\Traits\SEOTools;

class PostController extends Controller
{
    use SEOTools;

    /**
     * Retrieves the blog page.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function blog()
    {
        $this->seo()
            ->setTitle('Cambio de Juego - Blog')
            ->setDescription('Lo mas interesante del fútbol, en un solo lugar');
        $this->seo()->opengraph()
            ->setTitle('Cambio de Juego - Blog')
            ->setDescription('Lo mas interesante del fútbol, en un solo lugar');
        $this->seo()->twitter()
            ->setTitle('Cambio de Juego - Blog')
            ->setDescription('Lo mas interesante del fútbol, en un solo lugar');

        return view('website.blog');
    }

    /**
     * Returns a simple article.
     *
     * @param Post $post
     * @return \View
     */
    public function article(Post $post)
    {
        $this->seo()
            ->setTitle($post->seo_title)
            ->setDescription($post->excerpt);
        $this->seo()->opengraph()
            ->setTitle($post->seo_title)
            ->setDescription($post->excerpt)
            ->addImage(url($post->postMedia()));
        $this->seo()->twitter()
            ->setTitle($post->seo_title)
            ->setImages(url($post->postMedia()))
            ->setDescription($post->excerpt);

        if ($post->section->posts->count() < 4) {
            $related = $post->section->posts->shuffle();
        } else {
            $related = $post->section->posts->random(4)->shuffle();
        }

        return view('website.article', compact('post', 'related'));
    }
}

<?php

namespace App\Http\Controllers\Website;

use App\Section;
use App\Http\Controllers\Controller;

class SectionController extends Controller
{
    /**
     * Show the visible side of sections.
     *
     * @param Section $section
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Section $section)
    {
        $posts = $section->posts->sortByDesc('created_at');

        return view('ctest.sections.show', compact('section', 'posts'));
    }
}

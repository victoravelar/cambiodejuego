<?php

namespace App\Http\Controllers\Website;

use App\BusinessCase\SponsorDefinitionBusinessCase;
use App\Section;
use App\Competition;
use App\Http\Controllers\Controller;
use App\Sponsor;
use Artesaos\SEOTools\Traits\SEOTools;

class SiteController extends Controller
{
    use SEOTools;

    public function frontPage()
    {
        $this->seo()
            ->setTitle('Cambio de Juego - Una nueva forma de vivir el fútbol')
            ->setDescription('Lo mas interesante del fútbol, en un solo lugar');
        $this->seo()->opengraph()
            ->setTitle('Cambio de Juego - Una nueva forma de vivir el fútbol')
            ->setDescription('Lo mas interesante del fútbol, en un solo lugar');
        $this->seo()->twitter()
            ->setTitle('Cambio de Juego - Una nueva forma de vivir el fútbol')
            ->setDescription('Lo mas interesante del fútbol, en un solo lugar')
            ->setSite('@camdejueoficial');

        $sections = Section::activated()->where('name', '!=', 'Frontpage')->get()->sortBy('level');

        $sections->load('competitions')->where('active', true);

        $comps = Competition::active()->get();

        /** @var Section $section */
        $section = Section::where('name', 'Frontpage')->first();

        $sponsors = (new SponsorDefinitionBusinessCase())->getSponsorsForSection($section);

        return view('welcome', compact('sections', 'sponsors', 'comps'));
    }

    public function showLeague(Competition $competition)
    {
        $competition->load(['section', 'seasons', 'seasons.board']);

        return view('website.league', compact('competition'));
    }

    public function getBarSponsor()
    {
        /** @var Section $section */
        $section = Section::where('name', 'Frontpage')->first();
        $sponsors = (new SponsorDefinitionBusinessCase())->getSponsorsForSection($section);

        if ($sponsors['matches'] instanceof Sponsor) {
            return response()->json($sponsors['matches']);
        }

        return response()->json(['media' => 'https://placehold.it/250x90?text=SPONSORED BY', 'link' => 'http://cambiodejuego.com.mx']);
    }
}

<?php

namespace App\Http\Controllers\Website;

use App\Game;
use App\Http\Controllers\Controller;

class MatchsController extends Controller
{
    /**
     * Retrieve all the results in the database.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function results()
    {
        $results = Game::results()->paginate(12);

        return view('games.results', compact('results'));
    }

    public function single(Game $game)
    {
        dd($game);
    }
}

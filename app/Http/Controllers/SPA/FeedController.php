<?php

namespace App\Http\Controllers\SPA;

use App\Http\Resources\SPA\FeedResource;
use App\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FeedController extends Controller
{
    public function feed(Request $request)
    {
        return (FeedResource::collection(Post::blog()->paginate(12)));
    }
}

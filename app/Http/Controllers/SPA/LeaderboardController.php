<?php

namespace App\Http\Controllers\SPA;

use App\Competition;
use App\Leaderboard;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;

class LeaderboardController extends Controller
{
    public function boards()
    {
        $competitions = Competition::active()->get();

        $seasons = $competitions->each(function ($c) {
            return optional($c->seasons->where('is_open', true)->first());
        })->each(function ($c) {
            return $c->seasons->first()->board->sortBy('position');
        });

        $results = new Collection();

        foreach ($seasons->whereIn('acronym', ['PL', 'PD', 'MX', 'SA']) as $season) {
            $entry = $this->buildCompetitionFullEntry($season);
            $results->push($entry);
        }

        return $results;

    }

    private function buildCompetitionFullEntry($season)
    {
        $board = $this->buildBoardRecords($season->seasons->first()->board);

        return [
            'name' => $season->name,
            'logo' => $season->pathForLogo(),
            'acronym' => $season->acronym,
            'section_id' => $season->section_id,
            'info' => [
                'teams_relegated' => $season->detail->downgrade_positions,
                'teams_promoted' => $season->detail->champions_positions,
                'teams_rematch' => $season->detail->champions_rematch,
                'teams_europa' => $season->detail->europa_positions,
                'cup' => (bool) $season->detail->is_cup
            ],
            'season' => [
                'tag' => $season->seasons->first()->year
            ],
            'board' => $board
        ];
    }

    private function buildBoardRecords($board)
    {
        $result = [];

        foreach ($board->sortBy('position') as $item) {
            $result[] = [
                'team' => $item->team['name'],
                'logo' => $this->teamLogo($item->team['team_logo']),
                'position' => $item->position,
                'playedGames' => $item->playedGames,
                'points' => $item->points,
                'goals' => $item->goals,
                'goalsAgainst' => $item->goalsAgainst,
                'goalDifference' => $item->goalDifference,
                'wins' => $item->wins,
                'draws' => $item->draws,
                'losses' => $item->losses,
            ];
        }

        return $result;
    }

    private function teamLogo($team_logo)
    {
        if (preg_match('/teams/', $team_logo)) {
            return env('APP_URL').'/storage/'.$team_logo;
        }

        return $team_logo;
    }
}

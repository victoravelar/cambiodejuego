<?php

namespace App\Http\Controllers\SPA;

use App\BusinessCase\SponsorDefinitionBusinessCase;
use App\Section;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SponsorsController extends Controller
{
    public function static(Request $request)
    {
        $sections = Section::where('active', true)->get();

        $results = [];

        foreach ($sections as $section) {
            $sponsors = (new SponsorDefinitionBusinessCase())->getSponsorsForSection($section);
            $results[$section->name] = $sponsors->toArray();
        }

        foreach ($results as $section => $test) {
            if ($section === 'Frontpage') continue;

            if (count($test) === 0) {
                $results[$section] = $results['Frontpage'];
            } else {
                $filled = array_merge($results['Frontpage'], $results[$section]);

                $results[$section] = $filled;
            }
        }

        foreach ($sections as $section) {
            $results[$section->name]['section_id'] = $section->id;
        }

        if ($request->query->has('section')) {
            return response()->json(['data' => $results[$request->query->get('section')]]);
        }

        return response()->json(['data' => $results]);
    }
}

<?php

namespace App\Http\Controllers\SPA;

use App\Http\Resources\SPA\SettingResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use TCG\Voyager\Models\Setting;

/**
 * Class SettingsController
 *
 * This controller retrieves information on a enterprise level
 * this means that you can use this to return information that
 * is somehow static and that is holded and managed by the
 * company itself.
 *
 * @package App\Http\Controllers\SPA
 */
class SettingsController extends Controller
{
    /**
     * Returns a list of non empty settings.
     *
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function settings(Request $request)
    {
        return (SettingResource::collection(Setting::where('value', '!=', '')
            ->get()));
    }
}

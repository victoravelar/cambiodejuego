<?php

namespace App\Http\Controllers\SPA;

use App\Competition;
use App\DTO\CompetitionRender;
use App\DTO\PresentSections;
use App\DTO\SectionRender;
use App\Http\Resources\SPA\SectionsResource;
use App\Section;
use function Couchbase\defaultDecoder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SectionsController extends Controller
{
    /**
     * Level for UEFA competitions.
     */
    const UEFA = 2;

    /**
     * Level for international competitions.
     */
    const FIFA = 5;

    /**
     * Level for CONCACAF competitions.
     */
    const CONCACAF = 1;

    public function render(Request $request)
    {
        $sections = Section::activated()->get();

        $uefa = (new SectionRender)
            ->setName('UEFA')
            ->setMedia('https://upload.wikimedia.org/wikipedia/ru/thumb/b/b5/UEFA_logo.svg/768px-UEFA_logo.svg.png');

        $fifa = (new SectionRender)
            ->setName('FIFA')
            ->setMedia('https://upload.wikimedia.org/wikipedia/de/5/5d/FIFA_Logo.svg');

        $concacaf = (new SectionRender)
            ->setName('CONCACAF')
            ->setMedia('https://upload.wikimedia.org/wikipedia/de/0/0a/CONCACAF_logo.svg');

        foreach ($sections as $section) {
            foreach ($section->competitions as $competition) {
                if ($section->level == self::UEFA && $competition->isOpen()) {
                    $uefa->addCompetition($this->competitionRenderFactory($competition));
                } elseif ($section->level == self::FIFA && $competition->isOpen()) {
                    $fifa->addCompetition($this->competitionRenderFactory($competition));
                } elseif ($section->level == self::CONCACAF && $competition->isOpen()) {
                    $concacaf->addCompetition($this->competitionRenderFactory($competition));
                }
            }
        }

        $presentable = (new PresentSections())->setSections([$uefa, $fifa, $concacaf]);

        return response()->json($presentable);
    }

    /**
     * Get all the active sections.
     *
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function sections(Request $request)
    {
        return (SectionsResource::collection(Section::activated()->get()));
    }

    /**
     * @param Competition $competition
     * @return CompetitionRender
     */
    protected function competitionRenderFactory(Competition $competition): CompetitionRender
    {
        return (new CompetitionRender)
            ->setName($competition->name)
            ->setId($competition->section_id)
            ->setLogo($competition->pathForLogo());
    }
}

<?php

namespace App\Http\Controllers\SPA;

use App\Game;
use App\Http\Resources\SPA\FixturesResource;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * This controller retrieves relevant information for the
 * match bar and the fixtures time line.
 *
 * Class FixturesController
 * @package App\Http\Controllers\SPA
 */
class FixturesController extends Controller
{
    /**
     * Returns the upcoming games from all competitions.
     *
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function upcoming(Request $request)
    {
        return (FixturesResource::collection(Game::matchbar()->get()));
    }

    /**
     * Returns the results from all competitions.
     *
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function results(Request $request)
    {
        return (FixturesResource::collection(Game::results()->limit(150)->get()));
    }

    /**
     * Returns a time line of upcoming fixtures and past results.
     *
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function timeline(Request $request)
    {
        $games = Game::query()
            ->where('gameDate', '<=', Carbon::now()->format('Y-m-d'))
            ->orderByDesc('gameDate')
            ->orderByDesc('gameTime')
            ->paginate(50);

        return (FixturesResource::collection($games));
    }
}

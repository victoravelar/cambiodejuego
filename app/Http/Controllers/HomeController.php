<?php

namespace App\Http\Controllers;

use App\Section;
use TCG\Voyager\Facades\Voyager;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application homepage.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index()
    {
        if (Voyager::can('add_posts')) return redirect()->route('helium.dashboard');

        if (is_null(auth()->user()->profile)) {
            return redirect()->route('profile.create');
        }

        $profile = auth()->user()->profile;

        $competition = $profile->league->seasons->where('is_current', true)->load(['teams', 'games']);

        $board = $competition->first()->board;

        $boardType = $board->first()->group;

        $sections = Section::activated()->get();


        return view('home', compact('profile', 'sections', 'boardType', 'competition', 'board'));
    }
}

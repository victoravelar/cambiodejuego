<?php

namespace App\Http\Controllers\InternalAPI;

use App\BusinessCase\SponsorDefinitionBusinessCase;
use App\Http\Controllers\Controller;
use App\Section;
use Illuminate\Http\Request;

class DataController extends Controller
{
    public function sponsorsPerSection(Request $request)
    {
       if (!$request->ajax()) {
           abort(404);
       }

        $sections = Section::where('active', true)->get();

        $results = [];

        foreach ($sections as $section) {
            $sponsors = (new SponsorDefinitionBusinessCase())->getSponsorsForSection($section);
            $results[$section->name] = $sponsors->toArray();
        }

        foreach ($results as $section => $test) {
            if ($section === 'Frontpage') continue;

            if (count($test) === 0) {
                $results[$section] = $results['Frontpage'];
            } else {
                $filled = array_merge($results['Frontpage'], $results[$section]);

                $results[$section] = $filled;
            }
        }

        foreach ($sections as $section) {
            $results[$section->name]['section_id'] = $section->id;
        }

        return response()->json($results);
    }

}
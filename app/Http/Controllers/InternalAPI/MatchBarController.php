<?php

namespace App\Http\Controllers\InternalAPI;

use App\Game;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\ResultCollection;
use App\Http\Resources\MatchBarCollection;

class MatchBarController extends Controller
{
    /**
     * Retrieves the games to show in the matchbar.
     *
     * @param Request $request
     * @return string
     */
    public function matchbar(Request $request)
    {
        if (! $request->ajax()) {
            abort(404);
        }

        $games = Game::matchbar()->get()->filter(function ($game) {
            return $game->homeTeam !== null && $game->awayTeam !== null;
        });

        return new MatchBarCollection($games);
    }

    /**
     *  Retrieves the games to show in the results.
     *
     * @param Request $request
     * @return ResultCollection
     */
    public function results(Request $request)
    {
        if (! $request->ajax()) {
            abort(404);
        }

        return new ResultCollection(Game::results()->limit(150)->get());
    }
}

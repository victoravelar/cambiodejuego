<?php

namespace App\Http\Controllers\InternalAPI;

use App\Http\Resources\ProductsCollection;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return ProductsCollection
     */
    public function index(Request $request)
    {
        $items = 5;

        if ($request->query->get('items')) {
            $items = (int) $request->query->get('items');
        }

        $products = Product::active()->get();

        if ($products->count() > $items) {
            $products->random($items)->shuffle();
        } else {
            $products->random($products->count())->shuffle();
        }

        return new ProductsCollection($products);
    }
}

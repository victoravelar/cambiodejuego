<?php

namespace App\Http\Controllers\InternalAPI;

use App\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BlogController extends Controller
{
    public function posts(Request $request)
    {
        if (! $request->ajax()) {
            abort(404);
        }

        return Post::blog()->paginate(10);
    }
}

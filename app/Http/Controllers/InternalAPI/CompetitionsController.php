<?php

namespace App\Http\Controllers\InternalAPI;

use App\Competition;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\LeagueCollection;

class CompetitionsController extends Controller
{
    /**
     * @param Request $request
     * @return LeagueCollection
     */
    public function leagues(Request $request)
    {
        if (! $request->ajax()) {
            abort(404);
        }

        $competitions = Competition::with('detail')->get();

        if ($request->query->get('extended') == true) {
            $competitions->load(['seasons', 'seasons.teams']);
        }

        return new LeagueCollection($competitions);
    }
}

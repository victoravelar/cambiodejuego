<?php

namespace App\Http\Controllers\InternalAPI;

use App\Team;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\UefaCollection;
use App\Http\Resources\LocalLeagueCollection;

class ProfileController extends Controller
{
    /**
     * @param Request $request
     * @return LocalLeagueCollection
     */
    public function local(Request $request)
    {
        if (! $request->ajax()) {
            abort(404);
        }

        return new LocalLeagueCollection(Team::locale()->get());
    }

    /**
     * @param Request $request
     * @return UefaCollection
     */
    public function uefa(Request $request)
    {
        if (! $request->ajax()) {
            abort(404);
        }

        return new UefaCollection(Team::europe()->get());
    }
}

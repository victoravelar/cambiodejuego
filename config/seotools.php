<?php

return [
    'meta'      => [
        /*
         * The default configurations to be used by the meta generator.
         */
        'defaults'       => [
            'title'        => 'Cambio de Juego', // set false to total remove
            'description'  => 'Somos la forma alternativa de ver el deporte', // set false to total remove
            'separator'    => ' - ',
            'keywords'     => ['futbol', 'soccer', 'mexico', 'noticias deportes', 'liga mx', 'torneo mexicano'],
            'canonical'    => null, // Set null for using Url::current(), set false to total remove
        ],

        /*
         * Webmaster tags are always added.
         */
        'webmaster_tags' => [
            'google'    => null,
            'bing'      => null,
            'alexa'     => null,
            'pinterest' => null,
            'yandex'    => null,
        ],
    ],
    'opengraph' => [
        /*
         * The default configurations to be used by the opengraph generator.
         */
        'defaults' => [
            'title'       => 'Cambio de Juego', // set false to total remove
            'description' => 'Somos la forma alternativa de ver el deporte', // set false to total remove
            'url'         => null, // Set null for using Url::current(), set false to total remove
            'type'        => 'article',
            'site_name'   => 'Cambio de Juego',
            'images'      => ['https://scontent-frx5-1.xx.fbcdn.net/v/t31.0-8/25531835_307815963044439_2493894235894574748_o.jpg?oh=cb33ecb89cb897aa8f3e3d7416493928&oe=5AEF1313'],
        ],
    ],
    'twitter' => [
        /*
         * The default values to be used by the twitter cards generator.
         */
        'defaults' => [
          'card'        => 'summary_large_image',
          'site'        => '@camdejueoficial',
            'creator'   => '@camdejueoficial',
        ],
    ],
];

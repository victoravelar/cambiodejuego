<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeaderboardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leaderboards', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('season_id')->index();
            $table->unsignedInteger('external_competition_id')->index();
            $table->string('matchday');
            $table->unsignedInteger('position');
            $table->unsignedInteger('external_team_id')->index();
            $table->unsignedInteger('playedGames')->nullable();
            $table->unsignedInteger('points');
            $table->unsignedInteger('goals');
            $table->unsignedInteger('goalsAgainst');
            $table->integer('goalDifference');
            $table->unsignedInteger('wins')->nullable();
            $table->unsignedInteger('draws')->nullable();
            $table->unsignedInteger('losses')->nullable();
            $table->string('group');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leaderboards');
    }
}

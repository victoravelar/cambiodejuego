<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scores', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('game_id')->index();
            $table->unsignedInteger('goals_home_total')->nullable();
            $table->unsignedInteger('goals_away_total')->nullable();
            $table->unsignedInteger('goals_home_halftime')->nullable();
            $table->unsignedInteger('goals_away_halftime')->nullable();
            $table->string('external_source')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scores');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompetitionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('competitions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('section_id');
            $table->string('name')->unique();
            $table->string('acronym')->unique();
            $table->unsignedInteger('total_games');
            $table->unsignedInteger('total_teams');
            $table->unsignedInteger('total_rounds');
            $table->string('league_logo')->nullable();
            $table->string('league_website')->nullable();
            $table->string('external_source')->nullable();
            $table->unsignedInteger('external_id');
            $table->boolean('is_open')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('competitions');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teams', function (Blueprint $table) {
            $table->increments('id');
//            $table->unsignedInteger('season_id')->index();
            $table->string('name')->unique();
            $table->string('acronym')->nullable();
            $table->string('shortName')->nullable();
            $table->string('team_logo')->nullable();
            $table->string('team_website')->nullable();
            $table->string('external_source')->nullable();
            $table->unsignedInteger('external_id')->index();
            $table->boolean('active')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teams');
    }
}

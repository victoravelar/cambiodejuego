<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUuidToModels extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tables = ['competitions', 'games', 'players', 'seasons', 'teams', 'leaderboards', 'scores', 'sponsors'];

        foreach ($tables as $table) {
            Schema::table($table, function (Blueprint $t) {
                $t->uuid('uuid')->after('id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $tables = ['competitions', 'games', 'players', 'seasons', 'teams', 'leaderboards'];

        foreach ($tables as $table) {
            Schema::table($table, function(Blueprint $t) {
                $t->dropColumn('uuid');
            });
        }
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('games', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('season_id')->index();
            $table->boolean('finished')->default(false);
            $table->unsignedInteger('game_external_id')->index();
            $table->unsignedInteger('competition_external_id')->index();
            $table->unsignedInteger('homeTeam_external_id')->index()->nullable();
            $table->unsignedInteger('awayTeam_external_id')->index()->nullable();
            $table->date('gameDate');
            $table->time('gameTime');
            $table->unsignedInteger('matchDay')->index();
            $table->string('external_source');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('games');
    }
}

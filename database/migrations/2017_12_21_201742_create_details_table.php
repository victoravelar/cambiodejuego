<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('details', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('competition_id')->index();
            $table->unsignedInteger('champions_positions')->default(0);
            $table->unsignedInteger('champions_rematch')->default(0);
            $table->unsignedInteger('europa_positions')->default(0);
            $table->unsignedInteger('downgrade_positions')->default(0);
            $table->boolean('is_uefa')->default(false);
            $table->boolean('is_cup')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('details');
    }
}

<?php

use App\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'name' => $faker->streetName,
        'url' => $faker->url,
        'image' => $faker->imageUrl(),
        'price' => $faker->numberBetween(4000, 10000),
        'active' => $faker->boolean(35)
    ];
});

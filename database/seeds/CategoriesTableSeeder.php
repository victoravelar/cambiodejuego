<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run()
    {
        $competitions = ['texto', 'audio', 'video', 'imagenes', 'redes'];
        $index = 1;

        foreach ($competitions as $competition) {
            $category = Category::firstOrNew([
                'slug' => str_slug($competition),
            ]);
            if (! $category->exists) {
                $category->fill([
                    'name'       => $competition,
                    'order'     => $index,
                ])->save();
            }

            $index++;
        }

//        $category = Category::firstOrNew([
//            'slug' => 'category-1',
//        ]);
//        if (! $category->exists) {
//            $category->fill([
//                'name'       => 'Category 1',
//            ])->save();
//        }
//
//        $category = Category::firstOrNew([
//            'slug' => 'category-2',
//        ]);
//        if (! $category->exists) {
//            $category->fill([
//                'name'       => 'Category 2',
//            ])->save();
//        }
    }
}

<?php

use App\Section;
use Illuminate\Database\Seeder;

class SectionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sections = [
            'Fútbol Mexicano',
            'Fútbol Español',
            'Fútbol Inglés',
            'Fútbol Italiano',
            'Fútbol Alemán',
            'Fútbol Frances',
            'FIFA',
            'UEFA',
            'CONCACAF',
            'CAF',
            'CONMEBOL',
            'OFC',
            'AFC',
            'Fútbol de sudamérica A',
            'Fútbol de Sudamérica B',
            'Fútbol de África',
            'Fútbol de Asia',
            'Fútbol de Oceania',
            'Fútbol Americano',
            'Béisbol',
            'Básquetbol',
            'Fórmula 1',
            'Tenis',
            'Artes marciales y lucha libre',
            'Deportes olímpicos',
        ];

        $index = 1;

        foreach ($sections as $section) {
            $sec = [
                'name' => $section,
            ];

            if ($index <= 8) {
                $sec = array_merge($sec, ['active' => true]);
            }

            if ($index === 1) {
                $sec = array_merge($sec, ['level' => 1]);
            }

            if ($index >= 2 && $index <= 6) {
                $sec = array_merge($sec, ['level' => 2]);
            }

            if ($index >= 7 && $index <= 13) {
                $sec = array_merge($sec, ['level' => 5]);
            }

            if ($index >= 14 && $index <= 18) {
                $sec = array_merge($sec, ['level' => 3]);
            }

            if ($index > 18) {
                $sec = array_merge($sec, ['level' => 4]);
            }

            Section::create($sec);

            $index++;

            unset($sec);
        }
    }
}

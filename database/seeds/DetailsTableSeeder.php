<?php

use App\Detail;
use App\Competition;
use Illuminate\Database\Seeder;

class DetailsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      //Updating competition pic because at this point, the image of each competition is null
        $competitions = ['Premier League 2017/18', 'Eredivisie 2017/18', 'Ligue 1 2017/18',
      '1. Bundesliga 2017/18', 'Primera Division 2017', 'Serie A 2017/18', 'DFB-Pokal 2017/18', 'Champions League 2017/18', 'Liga BBVA Bancomer MX', ];

        //champions positions
        $champions_positions = ['3', '1', '2', '3', '3', '2', '0', '0', '0'];

        //champions rematch
        $champions_rematch = ['2', '1', '1', '1', '1', ' 1', '0', '0', '0'];

        //europa positions
        $europa_positions = ['2', '2', '1', '2', '2', '2', '0', '0', '0'];

        //downgrade positions
        $downgrade_positions = ['3', '3', '3', '3', '3', '3', '0', '0', '0'];

        //is UEFA
        $is_uefa = [true, true, true, true, true, true, true, true, false];

        //is UEFA
        $is_cup = [false, false, false, false, false, false, true, true, false];

        for ($i = 0; $i < count($competitions); $i++) {
            $competition = $this->findCompetition($competitions[$i]);
            if ($competition->exists) {
                $detail = $this->findDetail($competition->id);
                if (! $detail->exists) {
                    $detail->fill([
                    'competition_id'      => $competition->id,
                    'champions_positions' => $champions_positions[$i],
                    'champions_rematch'   => $champions_rematch[$i],
                    'europa_positions'    => $europa_positions[$i],
                    'downgrade_positions' => $downgrade_positions[$i],
                    'is_uefa'             => $is_uefa[$i],
                    'is_cup'              => $is_cup[$i],
                ])->save();
                }
            }
        }
    }

    /**
     * @param [name of Competition]
     *
     * @return [firstOrNew]
     */
    protected function findCompetition($name)
    {
        return Competition::firstOrNew(['name' => $name]);
    }

    /**
     * @param [id of Competition]
     *
     * @return [firstOrNew]
     */
    protected function findDetail($competition)
    {
        return Detail::firstOrNew(['competition_id' => $competition]);
    }
}

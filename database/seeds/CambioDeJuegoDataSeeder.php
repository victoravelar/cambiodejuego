<?php

use Illuminate\Database\Seeder;

class CambioDeJuegoDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(LigaMxSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(DetailsTableSeeder::class);
        $this->call(SettingsTableSeeder::class);
        $this->call(SectionsTableSeeder::class);
    }
}

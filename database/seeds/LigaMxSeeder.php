<?php

use App\Team;
use App\Competition;
use Illuminate\Database\Seeder;

class LigaMxSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $names = ['Atlas Fútbol Club', 'Club de Fútbol América S. A. de C. V.', '	Cruz Azul Futbol Club, A.C.',
        'Club Deportivo Guadalajara S.A. de C.V.', 'Club León F.C.', 'Club de Futbol Lobos de la Benemerita Un',
        'Club de Futbol Monterrey', 'Monarcas Morelia', 'Inpulsora del Deporte Necaxa S.A de C.V',
        'Club de Futbol Pachuca', 'Puebla Futbol Club', 'Querétaro Fútbol Club', '	Club Santos Laguna S.A. de C.V.',
        'Club Tijuana Xoloitzcuintles de Caliente', 'Deportivo Toluca Fútbol Club S.A. de C.V',
        'Club de Fútbol Tigres de la Universidad Autónoma de Nuevo León', 'Club Universidad Nacional A. C.',
        'Club Tiburones Rojos de Veracruz', ];

        $acronyms = ['ATS', 'AME', 'CAZ', 'CHI', 'LEO', 'LOB',
        'MON', 'MOR', 'NEC', 'PAC', 'PUE', 'QRO', 'SAN',
        'TIJ', 'TOL', 'UAN', 'PUM', 'VER', ];

        $shortNames = ['Atlas', 'América', 'Cruz Azul', 'Chivas', 'León', 'Lobos BUAP',
        'Monterrey', 'Monarcas', 'Rayos', 'Tuzos', 'Puebla', 'Querétaro', 'Santos',
        'Tijuana', 'Toluca', 'Tigres', 'Pumas', 'Veracruz', ];

        $logos = ['https://upload.wikimedia.org/wikipedia/commons/4/49/F%C3%BAtbol_Club_Atlas.PNG',
        'https://upload.wikimedia.org/wikipedia/id/3/3a/ClubAmericaLogo-1.png',
        'https://upload.wikimedia.org/wikipedia/en/thumb/5/55/Cruz_Azul_FC.svg/768px-Cruz_Azul_FC.svg.png',
        'https://vignette1.wikia.nocookie.net/logopedia/images/6/66/Chivas_2017_%2812_stars%29.png/revision/latest?cb=20170531224854',
        'https://upload.wikimedia.org/wikipedia/ro/thumb/f/fc/Escudo_del_Club_Le%C3%B3n.png/220px-Escudo_del_Club_Le%C3%B3n.png',
        'https://upload.wikimedia.org/wikipedia/en/0/01/Lobos_BUAP_logo.png',
        'https://upload.wikimedia.org/wikipedia/commons/thumb/2/28/CF_Monterrey_Crest.png/393px-CF_Monterrey_Crest.png',
        'https://upload.wikimedia.org/wikipedia/en/thumb/2/27/Monarcas_Morelia_2.svg/761px-Monarcas_Morelia_2.svg.png',
        'https://upload.wikimedia.org/wikipedia/en/thumb/c/c3/Club_Necaxa_2.svg/830px-Club_Necaxa_2.svg.png',
        'https://upload.wikimedia.org/wikipedia/en/thumb/9/93/Pachuca_Tuzos_logo.svg/604px-Pachuca_Tuzos_logo.svg.png',
        'https://vignette.wikia.nocookie.net/logopedia/images/2/2b/Puebla_fc_2016.png/revision/latest?cb=20160808012757',
        'https://upload.wikimedia.org/wikipedia/ro/thumb/9/92/Queretaro_FC_logo.svg/794px-Queretaro_FC_logo.svg.png',
        'https://upload.wikimedia.org/wikipedia/en/a/aa/Santos_Laguna.png',
        'https://upload.wikimedia.org/wikipedia/en/thumb/2/29/Club_Tijuana_logo.svg/821px-Club_Tijuana_logo.svg.png',
        'https://upload.wikimedia.org/wikipedia/en/thumb/8/89/Deportivo_Toluca_F.C.svg/808px-Deportivo_Toluca_F.C.svg.png',
        'https://upload.wikimedia.org/wikipedia/commons/a/ad/Escudo_de_Tigres_2016.jpg',
        'https://upload.wikimedia.org/wikipedia/en/a/a7/UNAM_Pumas.png',
        'https://upload.wikimedia.org/wikipedia/en/thumb/3/3b/Tiburones_Rojos_Veracruz.png/200px-Tiburones_Rojos_Veracruz.png', ];

        //Updating competition pic because at this point, the image of each competition is null
        $competitions = ['Premier League 2017/18', 'Eredivisie 2017/18', 'Ligue 1 2017/18', '1. Bundesliga 2017/18',
        'Primera Division 2017', 'Serie A 2017/18', 'DFB-Pokal 2017/18', 'Champions League 2017/18', ];

        $competition_pics = ['https://seeklogo.com/images/P/premier-league-new-logo-D22A0CE87E-seeklogo.com.png',
        'https://upload.wikimedia.org/wikipedia/an/thumb/1/1e/Eredivisie_logo.svg/1280px-Eredivisie_logo.svg.png',
        'https://upload.wikimedia.org/wikipedia/commons/thumb/4/41/Ligue1_Conforama.svg/1200px-Ligue1_Conforama.svg.png',
        'https://upload.wikimedia.org/wikipedia/en/thumb/d/df/Bundesliga_logo_%282017%29.svg/1024px-Bundesliga_logo_%282017%29.svg.png',
        'http://files.laliga.es/seccion_logos/laliga-v-600x600.png', 'http://www.legaseriea.it/assets/legaseriea/images/logo.png',
        'https://upload.wikimedia.org/wikipedia/en/thumb/9/9f/DFB-Pokal_logo_2016.svg/844px-DFB-Pokal_logo_2016.svg.png',
        'https://upload.wikimedia.org/wikipedia/en/thumb/b/bf/UEFA_Champions_League_logo_2.svg/1067px-UEFA_Champions_League_logo_2.svg.png', ];

        //Updating competition website because at this point, it is null
        $competitions_webs = ['https://www.premierleague.com/', 'https://eredivisie.nl/nl-nl/', 'http://www.ligue1.com/', 'https://www.bundesliga.com/es/',
        'http://mex.laliga.es/', 'http://www.legaseriea.it/en', 'https://www.dfb.de/en/national-tournaments/', 'http://es.uefa.com/uefachampionsleague/#/', ];

        for ($i = 0; $i < count($competitions); $i++) {
            $competition = $this->findCompetition($competitions[$i]);
            if ($competition->exists) {
                if ($competition->league_logo == null || $competition->league_logo == '') {
                    $competition->fill([
                'league_logo' => $competition_pics[$i],
              ])->save();
                }
                if ($competition->league_website == null || $competition->league_website == '') {
                    $competition->fill([
                'league_website' => $competitions_webs[$i],
              ])->save();
                }
            }
        }

        $competition = $this->findCompetition('Liga BBVA Bancomer MX');

        if (! $competition->exists) {
            $competition->fill([
                'section_id'      => 1,
                'name'            => 'Liga BBVA Bancomer MX',
                'acronym'         => 'MX',
                'total_games'     => '153',
                'total_teams'     => '18',
                'total_rounds'    => '17',
                'league_logo'     => 'https://upload.wikimedia.org/wikipedia/en/8/8f/Liga_MX.svg',
                'league_website'  => 'http://www.ligabancomer.mx/',
                'external_source' => 'Grupo Marotec',
                'external_id'     => '900',
                'is_open'         => true,
            ])->save();

            $season = $competition->seasons()->create(['year' => 2018, 'is_current' => true]);

            if ($this->lastLeagueInserted() == 'Liga BBVA Bancomer MX') {
                for ($i = 0; $i < count($names); $i++) {
                    $team = $this->findTeam($names[$i]);
                    if (! $team->exists) {
                        $newTeam = $team->create([
//                      'season_id'  => $this->lastLeagueInsertedId(),
                      'name'            => $names[$i],
                      'acronym'         => $acronyms[$i],
                      'shortName'       => $shortNames[$i],
                      'team_logo'       => $logos[$i],
                      'team_website'    => null,
                      'external_source' => null,
                      'external_id'     => 9000 + ($i * 2),
                      'active'          => true,
                  ]);
                        $season->teams()->attach($newTeam->id);
                    }
                }
            }
        }
    }

    /**
     * @param [name of Competition]
     *
     * @return [firstOrNew]
     */
    protected function findCompetition($name)
    {
        return Competition::firstOrNew(['name' => $name]);
    }

    /**
     * @return string
     */
    protected function lastLeagueInserted()
    {
        return Competition::all()->last()->name;
    }

    protected function lastLeagueInsertedId()
    {
        return Competition::all()->last()->seasons->where('is_current', true)->first()->id;
    }

    /**
     * @param [name of team]
     *
     * @return [firstOrNew]
     */
    protected function findTeam($name)
    {
        return Team::firstOrNew(['name' => $name]);
    }
}

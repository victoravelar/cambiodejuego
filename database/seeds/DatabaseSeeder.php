<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Artisan;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Artisan::call('cdj:competitions-fbd');
        $this->call(VoyagerDatabaseSeeder::class);
        $this->call(CambioDeJuegoDataSeeder::class);
        Artisan::call('cdj:league-tables');
        Artisan::call('cdj:single-import', ['id' => 467]);
    }
}

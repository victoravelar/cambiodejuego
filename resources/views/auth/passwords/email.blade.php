@extends('layouts.ctest')

@section('content')
    <section class="login-section">
        <div class="container">
            <div class="row">
                <div class="col s12 m6 offset-m3">
                    <div class="card">
                        <div class="card-content">
                            <span class="card-title center-align">
                                Recuperar mi contraseña
                            </span>
                            @if (session('status'))
                                <div class="alert alert-success">
                                    {{ session('status') }}
                                </div>
                            @endif

                            <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                                {{ csrf_field() }}

                                <div class="input-field{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email" class="col-md-4 control-label">Correo electrónico</label>
                                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                                <div class="center">
                                    @if ($errors->has('email'))
                                    <span class="red-text text-darken-4 center">
                                    <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                </div>

                                <div class="input-field">
                                    <div class="center-align">
                                        <button type="submit" class="btn btn-primary">
                                            Enviar enlace de recuperación
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col s12 m6 offset-m3">
                    <div class="card cdj-blue">
                        <div class="card-content">
                            <p class="white-text center-align">o ingresa con tus redes sociales</p>
                            <br>
                            <div class="center">
                                <a href="{{ url('/auth/facebook') }}" class="btn grey lighten-3 blue-text text-darken-3"><i class="fa fa-facebook-official"></i></a>
                                <a href="{{ url('/auth/twitter') }}" class="btn grey lighten-3 cyan-text"><i class="fa fa-twitter"></i></a>
                                {{--                                <a href="{{ url('/auth/google') }}" class="btn grey lighten-3 deep-orange-text"><i class="fa fa-google"></i></a>--}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

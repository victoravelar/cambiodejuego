@extends('layouts.ctest')

@section('content')
    <section class="login-section">
        <div class="container">
            <div class="row">
                <div class="col s12 m6 offset-m3">
                    <div class="card">
                        <div class="card-content">
                            <span class="card-title center-align">
                                Ingresa tus nuevas credenciales
                            </span>
                            <form method="post" action="{{ route('password.request') }}">
                                {{ csrf_field() }}
                                <input type="hidden" name="token" value="{{ $token }}">
                                <div class="input-field">
                                    <input type="email" name="email" id="email" autocomplete="email" value="{{ old('email') }}">
                                    <label for="email">Correo electrónico</label>
                                </div>
                                <div class="input-field">
                                    <input type="password" name="password" id="password" value="{{ old('password') }}">
                                    <label for="password">Contraseña</label>
                                </div>
                                <div class="input-field">
                                    <input type="password" name="password_confirmation" id="password_confirmation" value="{{ old('password_confirmation') }}">
                                    <label for="password_confirmation">Confirma tu contraseña</label>
                                </div>
                                <div class="input-field">
                                    <button type="submit" class="btn cdj-blue white-text btn-large btn-full">Actualizar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    @foreach($errors->all() as $error)
                        <div class="card red darken-4 white-text">
                            <div class="card-content center-align">
                                <span class="card-title">
                                    {{ $error }}
                                </span>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
@endsection

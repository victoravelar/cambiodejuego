@extends('layouts.ctest')

@section('content')
    <section class="login-section">
        <div class="container">
            <div class="row">
                <div class="col s12 m6 offset-m3">
                    <div class="card">
                        <div class="card-content">
                            <span class="card-title center-align">
                                Bienvenido a tu cambio de juego
                            </span>
                            <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                                {{ csrf_field() }}
                                <div class="input-field col s12">
                                    <input type="text" class="validate" id="email" name="email" value="{{ old('email') }}">
                                    <label for="email">
                                        @if ($errors->has('email'))
                                            {{ $errors->first('email') }}
                                        @else
                                            Correo electrónico
                                        @endif
                                    </label>
                                </div>
                                <div class="input-field col s12">
                                    <input type="password" class="validate" id="password" name="password">
                                    <label for="password">
                                        @if ($errors->has('password'))
                                            {{ $errors->first('password') }}
                                        @else
                                            Contraseña
                                        @endif
                                    </label>
                                </div>

                                <p>
                                    <input type="checkbox" id="remember" name="remember" {{ old('remember') ? 'checked' : '' }}/>
                                    <label for="remember">Mantener mi sesión</label>
                                </p>
                                <br>
                                <div class="center">
                                    <button class="btn waves-effect waves-light" type="submit" name="action">Ingresar
                                        <i class="material-icons left">input</i>
                                    </button>
                                    <a href="{{ route('password.request') }}" class="btn grey lighten-3 btn-flat green-text text-accent-4">
                                        Olvidé mi contraseña
                                    </a>
                                </div>
                                <br>
                                <div class="center">
                                    <a href="{{ route('register') }}" class="btn cdj-blue white-text btn-full cdj-blue-text">
                                        Registrarme
                                    </a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col s12 m6 offset-m3">
                    <div class="card cdj-blue">
                        <div class="card-content">
                            <p class="white-text center-align">o ingresa con tus redes sociales</p>
                            <br>
                            <div class="center">
                                <a href="{{ url('/auth/facebook') }}" class="btn grey lighten-3 blue-text text-darken-3"><i class="fa fa-facebook-official"></i></a>
                                <a href="{{ url('/auth/twitter') }}" class="btn grey lighten-3 cyan-text"><i class="fa fa-twitter"></i></a>
{{--                                <a href="{{ url('/auth/google') }}" class="btn grey lighten-3 deep-orange-text"><i class="fa fa-google"></i></a>--}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

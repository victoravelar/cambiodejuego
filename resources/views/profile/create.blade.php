@extends('layouts.ctest')
@section('content')
    <section class="login-section">
        <div class="container">
            <h4>Define tu feed personalizado</h4>
            <h6>Elige una liga, un equipo local y un equipo de la UEFA, nosotros haremos el resto.</h6>
            <br>
            <ul class="collapsible popout" data-collapsible="accordion">
              <li id="profile-liga">
                  <div class="collapsible-header">Liga
                    @if (! is_null(auth()->user()->profile))
                      @if (! is_null(auth()->user()->profile->league))
                        <br>
                        <img src="{{ $profile->league->league_logo }}" alt="{{ $profile->league->name }}" height="30"/>
                      @endif
                    @endif
                  </div>
                  <div class="collapsible-body">
                      <div class="row">
                        <leagues></leagues>
                      </div>
                  </div>
              </li>
                <li id="profile-local">
                    <div class="collapsible-header">Equipo local
                      @if (! is_null(auth()->user()->profile))
                        @if (! is_null(auth()->user()->profile->localTeam))
                          <br> <img src="{{ $profile->localTeam->team_logo }}" alt="{{ $profile->localTeam->name }}" height="30"/>
                        @endif
                      @endif
                    </div>
                    <div class="collapsible-body">
                        <div class="row">
                            <local-teams></local-teams>
                        </div>
                    </div>
                </li>
                <li id="profile-uefa">
                    <div class="collapsible-header">Equipo UEFA
                      @if (! is_null(auth()->user()->profile))
                        @if (! is_null(auth()->user()->profile->uefaTeam))
                          <br> <img src="{{ $profile->uefaTeam->team_logo }}" alt="{{ $profile->uefaTeam->name }}" height="30"/>
                        @endif
                      @endif
                    </div>
                    <div class="collapsible-body">
                        <div class="row">
                            <uefa-teams></uefa-teams>
                        </div>
                    </div>
                </li>
            </ul>

        </div>
        <div class="container">
            <div class="center">
                <a href="{{ route('home') }}" class="btn btn-large waves-effect waves-light">
                    Ir a mi espacio
                </a>
            </div>
        </div>
    </section>
@endsection

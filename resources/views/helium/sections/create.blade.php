<div id="create-section-modal" class="modal">
    <form action="{{ route('sections.store') }}" method="post">
    <div class="modal-content">
        <h4 class="center-align">Crear sección</h4>
        <div class="row">
            {{ csrf_field() }}
                <div class="input-field col s12 m8 offset-m2">
                    <input type="text" name="name" id="name">
                    <label for="name">Nombre de la seccion</label>
                </div>
                <div class="input-field col s12 m8 offset-m2">
                    <input type="text" name="description" id="description">
                    <label for="description">Descripcion de la seccion</label>
                </div>
                <div class="col s12 m8 offset-m2">
                    <p>
                        <input type="checkbox" id="active" name="active" value="1" />
                        <label for="active">Activar en el sitio</label>
                    </p>
                </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="submit" class="btn black white-text">Guardar</button>
    </div>
    </form>
</div>
@extends('helium.layout')

@section('content')
    <section class="section">
        <div class="container">
            <h3>
                Secciones del sitio
                <span class="right">
                    <a href="#create-section-modal" class="btn cdj-blue white-text modal-trigger">Agregar sección</a>
                </span>
            </h3>
        </div>
        <div class="container section">
            @foreach($sections->sortByDesc('active')->chunk(4) as $sec)
            <div class="row">
               @foreach($sec as $s)
                   <div class="col s12 m3">
                       <div class="card">
                           <div class="card-content center-align">
                               <span class="card-title truncate">
                                   {{ $s->name }}
                               </span>
                               <div class="chip {{ $s->active ? 'green' : 'red darken-4' }}">
                                   {{ $s->active ? 'Activa' : 'Inactiva' }}
                               </div>
                           </div>
                           <div class="card-action cdj-blue center-align">
                               <a href="{{ route('sections.edit', $s->id) }}" class="white-text">
                                   Edit
                               </a>
                               <a href="{{ route('sponsors.index', $s->slug) }}" class="white-text">
                                   Sponsor
                               </a>
                           </div>
                       </div>
                   </div>
               @endforeach
            </div>
            @endforeach
        </div>
    </section>
    @include('helium.sections.create')
@endsection
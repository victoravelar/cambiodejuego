@extends('helium.layout')
@section('content')
    <section class="section">
        <div class="container">
            <h4 class="section cdj-blue white-text center-align">{{ $team->name }}</h4>
            <br>
            <form action="{{ route('team.update', $team->uuid) }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                @foreach($team->fieldType() as $field => $attr)
                    @if($errors->has($field)) {{ $errors->first($field) }} @endif
                    @switch($attr['type'])
                        @case('checkbox')
                        <p>
                            <input type="{{ $attr['type'] }}" id="{{ $field }}" name="{{ $field }}" checked="{{ $attr['checked'] }}" value="{{ $attr['checked'] }}">
                            <label for="{{ $field }}">{{ $attr['label'] }}</label>
                        </p>
                        @break
                        @case('file')
                        <div class="file-field input-field">
                            <div class="btn">
                                <span>{{ $attr['label'] }}</span>
                                <input type="{{ $attr['type'] }}" name="{{ $field }}" id="{{ $field }}">
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path validate" type="text">
                            </div>
                        </div>
                        <div class="center">
                            <p class="flow-text center-align">Logotipo actual</p>
                            <img src="{{ $team->pathForLogo() }}" alt="Current logo" height="80">
                        </div>
                        @break
                        @default
                        <div class="input-field">
                            <input type="{{ $attr['type'] }}" name="{{ $field }}" id="{{ $field }}" value="{{ $team->$field }}">
                            <label for="{{ $field }}">{{ $attr['label'] }}</label>
                        </div>
                    @endswitch
                @endforeach
                <button type="submit" class="btn btn-full black white-text">
                    Actualizar
                </button>
            </form>
        </div>
    </section>
@endsection
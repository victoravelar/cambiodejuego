@extends('helium.layout')

@section('content')
    <section class="section">
        <div class="container">
            <h3>Editar juego: {{ $game->id }}</h3>
        </div>
        <div class="container">
            <div class="row">
                <div class="col s12">
                    <form action="{{ $game->editGame() }}" method="post" autocomplete="off">
                        {{ csrf_field() }}
                        <div class="input-field">
                            <select name="homeTeam_external_id" id="homeTeam_external_id">
                                @foreach($teams->sortBy('name') as $team)
                                    <option value="{{ $team->external_id }}" @if($game->homeTeam_external_id === $team->external_id) selected @endif>{{ $team->name }}</option>
                                @endforeach
                            </select>
                            <label for="homeTeam_external_id">Equipo local</label>
                        </div>
                        <br><br>
                        <div class="input-field">
                            <select name="awayTeam_external_id" id="awayTeam_external_id">
                                @foreach($teams->sortBy('name') as $team)
                                    <option value="{{ $team->external_id }}" @if($game->awayTeam_external_id == $team->external_id) selected @endif>{{ $team->name }}</option>
                                @endforeach
                            </select>
                            <label for="awayTeam_external_id">Equipo visitante</label>
                        </div>
                        <br><br>
                        <div class="input-field">
                            <input type="text" name="gameDate" value="{{ $game->gameDate }}" id="gameDate">
                            <label for="gameDate">Game date (YYYY-mm-dd)</label>
                        </div>
                        <br><br>
                        <div class="input-field">
                            <input type="text" name="gameTime" value="{{ $game->gameTime }}" id="gameTime">
                            <label for="gameTime">Game time (HH:MM:SS)</label>
                        </div>
                        <br><br>
                        <div class="input-field">
                            <input type="text" name="matchDay" value="{{ $game->matchDay }}" id="matchday">
                            <label for="matchday">Jornada</label>
                        </div>
                        <br><br>
                        <button type="submit" class="btn btn-full black white-text">Actualizar juego</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $('select').material_select();
        });
    </script>
@endsection
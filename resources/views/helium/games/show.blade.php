@extends('helium.layout')

@section('content')
    <section class="section">
        <div class="container">
            <h3>
                Juego: {{ $game->id }}
                <a href="{{ route('competitions.show', $game->season->competition->uuid) }}" class="button btn-flat black white-text right">
                    Regresar al panel de competencia.
                </a>
            </h3>
            <div class="card grey lighten-2">
                <div class="card-content">
                    <span class="card-title center-align">
                        <?php \Carbon\Carbon::setLocale(env('APP_LOCALE')); $carbon = new \Carbon\Carbon(); ?>
                        Fecha: {{ $carbon->parse($game->gameDate)->format('d-M-Y') }}
                    </span>
                    <div class="row">
                        <div class="col s6 center-align center">
                            <img src="{{ $game->homeTeam->pathForLogo() }}" alt="Local" height="50">
                            <p class="flow-text">
                                {{ $game->homeTeam->name }}
                            </p>
                        </div>
                        <div class="col s6 center-align center">
                            <img src="{{ $game->awayTeam->team_logo }}" alt="Local" height="50">
                            <p class="flow-text">
                                {{ $game->awayTeam->name }}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col s12 m6">
                    <a href="{{ $game->addResult() }}"
                       class="btn btn-full black white-text">
                        Set result
                    </a>
                </div>
                <div class="col s12 m6">
                    <a href="{{ $game->editGame() }}"
                       class="btn btn-full deep-orange white-text">
                        Edit game
                    </a>
                </div>
            </div>
        </div>
    </section>
@endsection
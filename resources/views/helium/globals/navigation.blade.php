@php
    $activeUser = auth()->user();
    /** @var \Carbon\Carbon $carbon */
    $carbon = new \Carbon\Carbon;
@endphp
<nav class="z-depth-0">
    <div class="container">
        <div class="nav-wrapper">
            <a href="#" data-activates="mobile-demo" class="button-collapse" id="helium-menu"><i class="material-icons">menu</i></a>
            <ul class="side-nav fixed" id="mobile-demo">
                <li>
                    <div class="helium-header perfect-center cdj-blue">
                        <h5 class="white-text center-align">
                            Helium
                        </h5>
                    </div>
                </li>
                <li><a href="#!" class="subheader">Helium</a></li>
                <li><a href="{{ route('helium.dashboard') }}"><i class="material-icons left">dashboard</i> Dashboard</a></li>
                @if(Voyager::can('add_competitions'))
                    <li><a href="{{ route('competitions.index') }}"><i class="material-icons left">event_seat</i> Manager</a></li>
                    <li><a href="{{ route('wizards') }}"><i class="material-icons left">fiber_smart_record</i> Wizards</a></li>
                @endif
                @if(Voyager::can('add_sections'))
                <li><a href="{{ route('sections.index') }}"><i class="material-icons left">change_history</i> Secciones</a></li>
                @endif
                @if(Voyager::can('add_products'))
                    <li><a href="{{ route('products.index') }}"><i class="material-icons left">shopping_basket</i> Products</a></li>
                @endif
                <li><a href="#!" class="subheader">Your content</a></li>
                <li><a href="{{ route('posts.create') }}"><i class="material-icons left">note_add</i> Article</a></li>
                <li><a href="#!" class="subheader">Website</a></li>
                <li><a href="{{ route('website.frontpage') }}"><i class="material-icons left">home</i> Frontpage</a></li>
                <li><a href="#!" class="subheader">User</a></li>
                @include('ctest.navigation.login-button-regular')
                <li>
                    <p class="grey-text darken-2 center-align">
                        {{ $activeUser->name }} <br>
                        <small>Role: {{ $activeUser->role->display_name }}</small>
                    </p>
                </li>
            </ul>
        </div>
    </div>
</nav>
@extends('helium.layout')
@section('content')
    <div class="container">
        <h3>Wizards</h3>
        <div class="divider"></div>
        <br>
        <div class="row">
            <div class="col s12 m4">
                <div class="card z-depth-2">
                    <div class="card-content">
                        <span class="card-title">
                            Nueva temporada.
                        </span>
                        <p>
                            Crea una nueva temporada para una liga.
                        </p>
                    </div>
                    <div class="card-action cdj-blue white-text">
                        <a href="{{route('wizards.new_season')}}" class="btn btn-flat cdj-blue waves-effect waves-light white-text">Comenzar</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
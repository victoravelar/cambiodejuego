@extends('helium.layout')

@section('content')
    <div class="container">
        <h3>Edit product</h3>
        <div class="row">
            <div class="col s12 m8 offset-m1">
                <div class="row">
                    <form action="{{ route('products.update', $product->slug) }}" method="post" enctype="multipart/form-data" autocomplete="off">

                        {{ csrf_field() }}
                        {{ method_field('PUT') }}

                        <div class="input-field col s12">
                            <input type="text" name="name" id="name" value="{{ $product->name }}">
                            <label for="name">Product name</label>
                        </div>

                        <div class="input-field col s12">
                            <input type="text" name="url" id="url" value="{{ $product->url }}">
                            <label for="url">Product url</label>
                        </div>

                        <div class="input-field col s12">
                            <input type="number" name="price" id="price" value="{{ $product->price }}">
                            <label for="price">Product price</label>
                        </div>

                        <div class="input-field col s12">
                            <label>Product image</label> <br><br>
                            <input type="file" name="image" id="image" value="{{ $product->image }}">
                        </div>
                        <div class="switch col s12">
                            <p>
                                Product status
                            </p>
                            <label>
                                Inactive
                                <input type="checkbox" value="true" name="active" @if($product->active) checked @endif>
                                <span class="lever"></span>
                                Active
                            </label>
                        </div>
                        <div class="input-field col s12">
                            <button type="submit" class="btn btn-full black white-text">
                                Update product
                            </button>
                        </div>
                    </form>
                </div>
                <div style="margin-top: 35px;"></div>
                <div class="center">
                    <p>Current image preview</p>
                    <img src="{{ $product->image }}" alt="Product photo" height="200px">
                </div>
                <div class="row">
                    @foreach($errors->all() as $error)
                        <div class="card red darken-4 white-text">
                            <div class="card-content">
                                <p class="center-align">
                                    {{ $error }}
                                </p>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
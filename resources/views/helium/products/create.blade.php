@extends('helium.layout')

@section('content')
    <div class="container">
        <h3>Create a new product</h3>
        <div class="row">
            <div class="col s12 m8 offset-m1">
                <div class="row">
                    <form action="{{ route('products.store') }}" method="post" enctype="multipart/form-data" autocomplete="off">

                        {{ csrf_field() }}

                        <div class="input-field col s12">
                            <input type="text" name="name" id="name" value="{{ old('name') }}">
                            <label for="name">Product name</label>
                        </div>

                        <div class="input-field col s12">
                            <input type="text" name="url" id="url" value="{{ old('url') }}">
                            <label for="url">Product url</label>
                        </div>

                        <div class="input-field col s12">
                            <input type="number" name="price" id="price" value="{{ old('price') }}">
                            <label for="price">Product price (in cents)</label>
                        </div>

                        <div class="input-field col s12">
                            <label>Product image</label> <br><br>
                            <input type="file" name="image" id="image" value="{{ old('image') }}">
                        </div>
                        <div class="switch col s12">
                            <p>
                                Product status
                            </p>
                            <label>
                                Inactive
                                <input type="checkbox" value="true" name="active">
                                <span class="lever"></span>
                                Active
                            </label>
                        </div>
                        <div class="input-field col s12">
                            <button type="submit" class="btn btn-full black white-text">
                                Save new product
                            </button>
                        </div>
                    </form>
                </div>
                <div class="row">
                    @foreach($errors->all() as $error)
                    <div class="card red darken-4 white-text">
                        <div class="card-content">
                            <p class="center-align">
                                {{ $error }}
                            </p>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
@extends('helium.layout')

@section('content')
    <div class="container">
        <h4 class="center-align">This is how your product is gonna look.</h4>
        <div class="divider"></div>
        <div class="row">
            <div class="col s12 m3">
                <p class="flow-text">
                    Actions
                </p>
                <p>
                    <a href="{{ route('products.edit', $product->slug) }}" class="btn btn-full black white-text waves-light">
                        Edit product
                    </a>
                </p>
                <p>
                    <a href="#" class="btn btn-full red darken-4 white-text waves-light"
                    onclick="event.preventDefault(); document.getElementById('deleteProduct').submit()">
                        Remove product
                    </a>
                <form action="{{ route('products.destroy', $product->slug) }}" id="deleteProduct" method="post">
                    {{ method_field('DELETE') }}
                    {{ csrf_field() }}
                </form>
                </p>
            </div>
            <div class="col s12 m6">
                <p>
                    Horizontal diplay (landing and posts)
                </p>
                <div class="card horizontal z-depth-2">
                    <div class="card-image">
                        <img src="{{ $product->productImage() }}" alt="Product image">
                    </div>
                    <div class="card-stacked">
                        <div class="card-content">
                            <span class="card-title">
                                {{ $product->name }}
                            </span>
                            <p class="justify-align flow-text">
                                $ {{ $product->price / 100 }}
                            </p>
                        </div>
                        <div class="card-action">
                            <a href="{{ $product->url }}" class="btn cdj-blue white-text waves-light">
                                View in store
                            </a>
                        </div>
                    </div>
                </div>
                <p>
                    Vertical diplay (Across the blog posts)
                </p>
                <div class="card z-depth-2">
                    <div class="card-image">
                        <img src="{{ $product->productImage() }}" alt="Product image">
                    </div>
                    <div class="card-content">
                        <span class="card-title">
                            {{ $product->name }}
                        </span>
                        <p class="justify-align flow-text">
                            $ {{ $product->price / 100 }}
                        </p>
                    </div>
                    <div class="card-action right-align">
                        <a href="{{ $product->url }}" class="btn cdj-blue white-text waves-light">
                            View in store
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@extends('helium.layout')

@section('content')
    <div class="container">
        <h3>Product list <a href="{{ route('products.create') }}" class="btn black white-text right">Create a product</a></h3>
        <table class="table bordered highlight striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Status</th>
                    <th>URL</th>
                    <th>Price</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach($products as $product)
                    <tr>
                        <td>{{ $product->name }}</td>
                        <td>
                            <div class="chip">
                                {{ $product->active ? 'Active' : 'Inactive' }}
                            </div>
                        </td>
                        <td><a href="{{ $product->url }}" class="btn black white-text waves-light" target="_blank">Go to product external url</a></td>
                        <td>$ {{ $product->price / 100 }}</td>
                        <td>
                            <a href="{{ route('products.show', $product->slug) }}" class="btn black white-text waves-light">
                                View and control
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{ $products->links() }}
    </div>
@endsection
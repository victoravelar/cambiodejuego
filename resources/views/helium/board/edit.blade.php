@extends('helium.layout')
@section('content')
    <section class="section">
        <div class="container">
            <h4 class="section cdj-blue white-text center-align">
                Editar entrada de tabla <br>
                <small>{{ $leaderboard->team->shortName }}</small>
            </h4>
            <br>
            <form action="{{ route('board.update', $leaderboard->uuid) }}" method="post">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                @foreach($elements as $field => $attr)
                    @if($errors->has($field)) {{ $errors->first($field) }} @endif
                    @switch($attr['type'])
                        @case('checkbox')
                        <p>
                            <input type="{{ $attr['type'] }}" id="{{ $field }}" name="{{ $field }}" value="true" @if($leaderboard->$field) checked @endif>
                            <label for="{{ $field }}">{{ $attr['label'] }}</label>
                        </p>
                        @break
                        @case('file')
                        <div class="file-field input-field">
                            <div class="btn">
                                <span>{{ $attr['label'] }}</span>
                                <input type="{{ $attr['type'] }}" name="{{ $field }}" id="{{ $field }}">
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path validate" type="text">
                            </div>
                        </div>
                        @break
                        @default
                        <div class="input-field">
                            <input type="{{ $attr['type'] }}" name="{{ $field }}" id="{{ $field }}" value="{{ $leaderboard->$field }}" max="3">
                            <label for="{{ $field }}">{{ $attr['label'] }}</label>
                        </div>
                    @endswitch
                @endforeach
                <button type="submit" class="btn btn-full black white-text">
                    Guardar detalles
                </button>
            </form>
        </div>
    </section>
@endsection
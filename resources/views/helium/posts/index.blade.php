@extends('helium.layout')

@section('content')
    <section class="section">
        <div class="container">
            <h3 style="position: sticky; top: 0;">
                Tus publicaciones
                <a href="{{ route('posts.create') }}" class="btn right cdj-blue white-text">
                    Crear nueva publicacion
                </a>
            </h3>
            <div class="row">
                <div class="col s12">
                    @if($posts->where('status', 'PUBLISHED')->count() > 0)
                    <table class="table bordered highlight">
                        <thead>
                            <tr>
                                <th>Titulo</th>
                                <th>Status</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($posts->where('status', 'PUBLISHED') as $post)
                                <tr>
                                    <td>{{ $post->title }}</td>
                                    <td>{{ $post->status }}</td>
                                    <td>
                                        <a href="{{ route('website.article', $post->slug) }}" class="btn btn-flat grey lighten-3 cdj-blue-text">
                                            <i class="material-icons">remove_red_eye</i>
                                        </a>
                                        <a href="{{ route('posts.edit', $post->slug) }}" class="btn btn-flat cdj-blue white-text">
                                            <i class="material-icons">edit</i>
                                        </a>
                                        @if(Voyager::can('browse_roles'))
                                        <button class="btn btn-flat cdj-red white-text" onclick="$('#delete-posts-form').submit()">
                                            <i class="material-icons">delete</i>
                                        </button>
                                        <form action="{{ route('posts.destroy', $post->slug) }}" method="post" id="delete-posts-form">
                                            {{ csrf_field() }}
                                            {!! method_field('DELETE') !!}
                                        </form>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @else
                        <div class="card-panel red lighten-2">
                            <div class="card-content">
                                <p class="white-text center-align flow-text">
                                    Aún no tienes publicaciones.
                                </p>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <br>
        <div class="container">
            <h3 style="position: sticky; top: 0;">
                Tus Borradores
            </h3>
            <div class="row">
                <div class="col s12">
                    @if($posts->where('status', 'DRAFT')->count() > 0)
                    <table class="table bordered highlight responsive-table">
                        <thead>
                        <tr>
                            <th>Titulo</th>
                            <th>Status</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($posts->where('status', 'DRAFT') as $post)
                            <tr>
                                <td>{{ $post->title }}</td>
                                <td>{{ $post->status }}</td>
                                <td>
                                    <a href="{{ route('posts.edit', $post->slug) }}" class="btn btn-flat cdj-blue white-text">
                                        <i class="material-icons">edit</i>
                                    </a>
                                    <a href="{{ route('posts.destroy', $post->slug) }}" class="btn btn-flat cdj-red white-text">
                                        <i class="material-icons">delete</i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    @else
                        <div class="card-panel cdj-blue">
                            <div class="card-content">
                                <p class="white-text center-align flow-text">
                                    No tienes borradores
                                </p>
                                <p class="center">
                                    <a href="{{ route('posts.create') }}" class="black-text white btn-large">
                                        Comienza a escribir ahora
                                    </a>
                                </p>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
@endsection


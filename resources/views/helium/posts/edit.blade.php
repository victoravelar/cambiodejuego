@extends('helium.layout')

@section('content')
    <section class="section">
        <div class="container">
            <h4 class="center-align">Edición de contenido</h4>
            <br>
            <form action="{{ route('posts.update', $post->slug)  }}" autocomplete="off" method="post">
                {{ csrf_field() }}
                {!! method_field('PUT') !!}
                <div class="input-field">
                    <input type="text" name="caption" id="caption" value="{{ $post->caption }}">
                    <label for="caption">Caption de la imagen</label>
                    @if($errors->has('caption'))
                        <p class="red white-text">
                            {{ $errors->first('caption') }}
                        </p>
                    @endif
                </div>
                <div class="input-field">
                    <input type="text" name="title" id="title" value="{{ $post->title }}">
                    <label for="title">Titulo de la publicación</label>
                    @if($errors->has('title'))
                        <p class="red white-text">
                            {{ $errors->first('title') }}
                        </p>
                    @endif
                </div>
                <div class="input-field">
                    <textarea name="excerpt" id="excerpt" rows="2" class="materialize-textarea">{{ $post->excerpt }}</textarea>
                    <label for="excerpt">Extracto del post</label>
                    @if($errors->has('excerpt'))
                        <p class="red white-text">
                            {{ $errors->first('excerpt') }}
                        </p>
                    @endif
                </div>
                <textarea name="body" id="body" cols="30" rows="10">{{ $post->body }}</textarea>
                @if($errors->has('body'))
                    <p class="red white-text">
                        {{ $errors->first('body') }}
                    </p>
                @endif
                <div class="input-field">
                    <textarea name="meta_description" id="meta_description" rows="2" class="materialize-textarea">{{ $post->meta_description }}</textarea>
                    <label for="meta_description">Description para meta</label>
                    @if($errors->has('meta_description'))
                        <p class="red white-text">
                            {{ $errors->first('meta_description') }}
                        </p>
                    @endif
                </div>
                <div class="input-field">
                    <textarea name="meta_keywords" id="meta_keywords" rows="2" class="materialize-textarea">{{ $post->meta_keywords }}</textarea>
                    <label for="meta_keywords">Keywords para google</label>
                    @if($errors->has('meta_keywords'))
                        <p class="red white-text">
                            {{ $errors->first('meta_keywords') }}
                        </p>
                    @endif
                </div>
                <div class="input-field">
                    <input type="text" name="seo_title" id="seo_title" value="{{ $post->seo_title }}">
                    <label for="seo_title">Encabezado SEO</label>
                    @if($errors->has('seo_title'))
                        <p class="red white-text">
                            {{ $errors->first('seo_title') }}
                        </p>
                    @endif
                </div>
                <div class="input-field">
                    <select name="status" id="status">
                        <option value="DRAFT" @if($post->status === 'DRAFT') selected @endif>Borrador</option>
                        <option value="PUBLISHED" @if($post->status === 'PUBLISHED') selected @endif>Publicar</option>
                    </select>
                    <label for="status">Visibilidad del post</label>
                </div>
                <div class="right-align">
                    <button type="submit" class="btn black white-text btn-flat btn-large">Actualizar</button>
                </div>
            </form>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        var token = document.head.querySelector('meta[name="csrf-token"]');

        $('#body').trumbowyg({
            resetCss: true,
            semantic: true,
            removeformatPasted: true,
            autogrow: true,
            svgPath: false,
            imageWidthModalEdit: true,
            hideButtonTexts: true,
            plugins: {
                upload: {
                    serverPath: '/helium/article/bodyContent/image',
                    fileFieldName: 'image',
                    headers: {
                        'X-CSRF-TOKEN': token.content
                    },
                    urlPropertyName: 'url'
                }
            },
            btnsDef: {
                // Create a new dropdown
                image: {
                    dropdown: [
                        'insertImage',
                        'upload'],
                }
            },
            // Redefine the button pane
            btns: [
                ['viewHTML'],
                ['formatting'],
                ['strong', 'em', 'del'],
                ['superscript', 'subscript'],
                ['link'],
                ['image'], // Our fresh created dropdown
                ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
                ['unorderedList', 'orderedList'],
                ['horizontalRule'],
                ['removeformat'],
                ['fullscreen']
            ],
        });
        $('select').material_select();
    </script>
@endsection


@extends('helium.layout')

@section('content')
    <section class="section">
        <div class="container">
            <h4>Registrar una nueva competición</h4>
        </div>
        <div class="container">
            <form action="{{ route('competitions.store') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="row">
                    <div class="input-field col s12 m4">
                        <input required type="text" name="name" id="name" value="{{ old('name') }}">
                        <label for="name">Nombre</label>
                    </div>
                    <div class="input-field col s12 m2">
                        <input required type="text" name="acronym" id="acronym" value="{{ old('acronym') }}">
                        <label for="acronym">Acronimo</label>
                    </div>
                    <div class="input-field col s12 m2">
                        <input required type="number" name="total_games" id="total_games" value="{{ old('total_games') }}">
                        <label for="total_games">Total de Juegos</label>
                    </div>
                    <div class="input-field col s12 m2">
                        <input required type="number" name="total_rounds" id="total_rounds" value="{{ old('total_rounds') }}">
                        <label for="total_rounds">Total de Jornadas</label>
                    </div>
                    <div class="input-field col s12 m2">
                        <input required type="number" name="total_teams" id="total_teams" value="{{ old('total_teams') }}">
                        <label for="total_teams">Número de equipos</label>
                    </div>
                    <div class="input-field col s12 m6">
                        <input required type="text" name="league_website" id="league_website" value="{{ old('league_website') }}">
                        <label for="league_website">Sitio web</label>
                    </div>
                    <div class="file-field input-field col s12 m6">
                        <div class="btn">
                            <span>Logotipo de la competición</span>
                            <input type="file" name="league_logo" required>
                        </div>
                        <div class="file-path-wrapper">
                            <input class="file-path validate" type="text">
                        </div>
                    </div>
                    <div class="col s12 m12">
                        <h5>Sección para CDJ (Lógica semantica)</h5>
                        <p class="row">
                            @foreach($sections as $section)
                                <span class="col s12 m4">
                                    <input type="radio" value="{{ $section->id }}" name="section_id" id="{{ $section->slug.$section->id }}" @if($section->id === old('section_id')) checked @endif>
                                    <label for="{{ $section->slug.$section->id }}">{{ $section->name }}</label>
                                </span>
                            @endforeach
                        </p>
                    </div>
                    <input type="hidden" name="external_id" value="1">
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <button type="submit" class="btn black btn-full white-text">
                            Guardar Competición
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </section>
@endsection
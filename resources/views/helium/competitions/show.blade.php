@extends('helium.layout')

@section('content')
    <section class="section">
        <div class="container">
            <div class="row">
                <div class="col s12 m8 offset-m2 center">
                    <img src="{{ $competition->pathForLogo() }}" alt="{{ $competition->name }}" height="200">
                    <br>
                    <p class="flow-text">
                        {{ $competition->name }}
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col s12 m10 offset-m1">
                    <div class="row">
                        <div class="col s12">
                            <ul class="tabs tabs-fixed-width">
                                <li class="tab"><a href="#test1">Información</a></li>
                                <li class="tab"><a href="#test2">Equipos</a></li>
                                <li class="tab">
                                    <a href="#test3">
                                        @if($competition->detail->is_cup)
                                            Clasificacion
                                        @else
                                            Tabla General
                                        @endif
                                    </a>
                                </li>
                                <li class="tab"><a href="#test4">Partidos</a></li>
                                <li class="tab"><a href="#test5">Configuración</a></li>
                            </ul>
                        </div>
                        <div id="test1" class="col s12">
                            @include('helium.competitions.partials.info')
                        </div>
                        <div id="test2" class="col s12">
                            @include('helium.competitions.partials.teams')
                        </div>
                        <div id="test3" class="col s12">
                            @include('helium.competitions.partials.clasification')
                        </div>
                        <div id="test4" class="col s12">
                            @include('helium.competitions.partials.games')
                        </div>
                        <div id="test5" class="col s12">
                            @include('helium.competitions.partials.config')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        $('.tabs').tabs();
    </script>
@endsection
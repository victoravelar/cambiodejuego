@if($season && $season->board->count() > 0)
@if($season->board->first()->group === 'league')
    <table class="bordered highlight centered">
        <thead>
        <tr>
            <th>Rank</th>
            <th>Equipo</th>
            <th class="hide-on-med-and-down">JJ</th>
            <th class="hide-on-med-and-down">JG</th>
            <th class="hide-on-med-and-down">JP</th>
            <th class="hide-on-med-and-down">JE</th>
            <th class="hide-on-med-and-down">GF</th>
            <th class="hide-on-med-and-down">GC</th>
            <th>DG</th>
            <th>Pts.</th>
        </tr>
        </thead>
        <tbody>
        @foreach($season->board->sortBy('position') as $entry)
            <tr>
                <td
                    @if($entry->position <= $competition->detail->champions_positions)
                    class="cdj-blue white-text"
                    @elseif($entry->position > $competition->detail->champions_positions
                      && $entry->position <= ($competition->detail->champions_positions +
                      $competition->detail->champions_rematch))
                    class="cdj-blue lighten-3 white-text"
                    @elseif($entry->position > $competition->detail->champions_positions
                      && $entry->position <= ($competition->detail->champions_positions +
                      $competition->detail->champions_rematch +
                      $competition->detail->europa_positions))
                    class="orange darken-2"
                    @elseif( $entry->position >= (count($season->teams) - $competition->detail->europa_positions))
                    class="red darken-4 white-text"
                    @endif
                >{{ $entry->position }}</td>
                <td>
                    {{ $entry->team->name }} <a href="{{ route('board.edit', $entry->uuid) }}">[Editar]</a>
                </td>
                <td class="hide-on-med-and-down">{{ $entry->playedGames }}</td>
                <td class="hide-on-med-and-down">{{ $entry->wins }}</td>
                <td class="hide-on-med-and-down">{{ $entry->losses }}</td>
                <td class="hide-on-med-and-down">{{ $entry->draws }}</td>
                <td class="hide-on-med-and-down">{{ $entry->goals }}</td>
                <td class="hide-on-med-and-down">{{ $entry->goalsAgainst }}</td>
                <td>{{ $entry->goalDifference }}</td>
                <td>{{ $entry->points }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@elseif($season->board->first()->group !== 'league')
    @foreach($season->board->sortBy('group')->groupBy('group') as $name => $group)
        <div class="col s12">
            <div class="card">
                <div class="card-content">
                    <span class="card-title center-align">Grupo {{ $name }}</span>
                    <table class="bordered highlight centered">
                        <thead>
                        <tr>
                            <th>Equipo</th>
                            <th class="hide-on-med-and-down">JJ</th>
                            <th class="hide-on-med-and-down">JG</th>
                            <th class="hide-on-med-and-down">JP</th>
                            <th class="hide-on-med-and-down">JE</th>
                            <th class="hide-on-med-and-down">GF</th>
                            <th class="hide-on-med-and-down">GC</th>
                            <th>DG</th>
                            <th>Pts.</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($group->sortByDesc('goals')->sortByDesc('goalDifference')->sortByDesc('points')->sortByDesc('playedGames') as $entry)
                            <tr
                                    @if($loop->iteration <= 2)
                                    class="cdj-blue white-text"
                                    @endif
                            >
                                @if(Voyager::can('add_posts'))
                                <td>
                                    {{ $entry->team->name }} <a href="{{ route('board.edit', $entry->uuid) }}">[Editar]</a>
                                </td>
                                @endif
                                <td class="hide-on-med-and-down">{{ $entry->playedGames }}</td>
                                <td class="hide-on-med-and-down">{{ $entry->wins }}</td>
                                <td class="hide-on-med-and-down">{{ $entry->losses }}</td>
                                <td class="hide-on-med-and-down">{{ $entry->draws }}</td>
                                <td class="hide-on-med-and-down">{{ $entry->goals }}</td>
                                <td class="hide-on-med-and-down">{{ $entry->goalsAgainst }}</td>
                                <td>{{ $entry->goalDifference }}</td>
                                <td>{{ $entry->points }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <br>
        </div>
    @endforeach
@endif
@else
    @if($season->teams->count() > 0)
    <div class="card z-depth-0">
        <div class="card-content">
            <a href="{{ route('board.init', $season->uuid) }}" class="btn btn-full black white-text waves-effect waves-light">
                Generar tabla inicial
            </a>
        </div>
    </div>
    @endif
@endif
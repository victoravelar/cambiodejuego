<div class="card">
    <div class="card-content">
        <p class="flow-text">
            <strong>Configuración general</strong>
        </p>
        <table class="table bordered striped responsive-table">
            <thead>
            <tr>
                <th>Field</th>
                <th>Info</th>
            </tr>
            </thead>
            <tbody>
            @foreach($competition->detail->getFillable() as $element)
                <tr>
                    <td width="30%">{{ ucfirst(str_replace('_', ' ', $element)) }}</td>
                    <td>
                        {{ $competition->detail->$element }}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @if(\Voyager::can('delete_details'))
            <a href="{{ route('details.edit', $competition->detail->id) }}" class="btn btn-full black white-text">
                Editar
            </a>
        @endif
    </div>
</div>
@if($season && $season->teams->count() > 0)
<div class="card">
    <div class="card-content">
        <span class="card-title center-align">Equipos participantes</span>
        <table class="table bordered striped centered responsive-table">
            <tbody>
            @foreach($season->teams->sortBy('name') as $team)
                <tr>
                    <td width="30%"><img src="{{ $team->pathForLogo() }}" alt="{{ $team->shortName }}" height="50"></td>
                    <td>
                        <p class="flow-text">
                            {{ $team->name }}
                        </p>
                    </td>
                    @if(Voyager::can('delete_teams'))
                        <td>
                            <a class="btn black white-text modal-trigger" href="{{ route('team.edit', $team->uuid) }}">
                                Editar
                            </a>
                        </td>
                    @endif
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
@else
    <div class="row section">
        <div class="col s12 m6 offset-m3">
            <div class="card">
                <div class="card-content">
                    <span class="card-title">
                        Cargar equipos
                    </span>
                    <p>
                        Existen dos metodos de carga:
                    </p>
                </div>
                <div class="card-action center">
                    <a href="#!" class="btn black white-text">Agregar uno a uno</a>
                    <a href="#!" class="btn">Utilizar un CSV</a>
                </div>
            </div>
        </div>
    </div>
@endif
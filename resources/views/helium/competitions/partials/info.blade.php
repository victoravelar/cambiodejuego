<div class="card">
    <div class="card-content">
        <p class="flow-text">
            <strong>Información general</strong>
            <strong>Temporada: </strong> {{ $competition->seasons->where('is_current', true)->last()->year }}
        </p>
        <table class="table bordered striped responsive-table">
            <thead>
            <tr>
                <th>Field</th>
                <th>Info</th>
            </tr>
            </thead>
            <tbody>
            @foreach($elements as $element)
                <tr>
                    <td width="30%">{{ ucfirst(str_replace('_', ' ', $element)) }}</td>
                    <td>
                        @switch($element)
                            @case('section_id')
                            {{ $competition->section->name }}
                            @break
                            @case('league_logo')
                            <a href="{{ $competition->pathForLogo() }}" class="btn btn-large btn-full black white-text" target="_blank">View logo</a>
                            @break
                            @case('league_website')
                            <a href="{{ $competition->$element }}" class="btn btn-large btn-full black white-text" target="_blank">View website</a>
                            @break
                            @default
                            {{ $competition->$element }}
                        @endswitch
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <div class="card-action center">
        <a href="{{ route('competitions.edit', $competition->uuid) }}" class="btn black white-text">
            Edit competition
        </a>
    </div>
</div>
@if($season && $season->games->count() > 0)
<div class="row">
    <ul class="collapsible">
        @php
            $currentMatchDay = (int) optional($season->board->sortByDesc('playedGames')->sortBy('matchDay')->first())->playedGames;
        @endphp
        @foreach($season->games->groupBy('matchDay') as $matchday)
            @php
                $journey = $matchday->first()->matchDay;
            @endphp
            <li>
                <div class="collapsible-header">
                    <i class="material-icons">local_play</i> Jornada {{ $journey }}
                    @if($journey < $currentMatchDay)
                        <div class="chip cdj-blue white-text right-aligned" style="margin-left: 20px">
                            COMPLETADA
                        </div>
                    @elseif($journey === $currentMatchDay)
                        <div class="chip cdj-red white-text right-aligned" style="margin-left: 20px">
                            ACTUAL
                        </div>
                    @endif
                </div>
                <div class="collapsible-body">
                    <table class="striped bordered centered responsive-table">
                        <thead>
                        <tr>
                            <th>Local</th>
                            <th>Goles local</th>
                            <th>Goles visita</th>
                            <th>Visita</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($matchday as $game)
                            @if($game->score !== null)
                                <tr>
                                    <td>{{ $game->homeTeam->name }}</td>
                                    <td>{{ $game->score->goals_home_total }}</td>
                                    <td>{{ $game->score->goals_away_total }}</td>
                                    <td>{{ $game->awayTeam->name }}</td>
                                </tr>
                            @else
                                <tr>
                                    <td>{{ optional($game->homeTeam)->name }}</td>
                                    <td colspan="2">
                                        <div class="row">
                                            <div class="col s12 m6">
                                                <a href="{{ $game->addResult() }}"
                                                   class="btn btn-full black white-text">
                                                    Set result
                                                </a>
                                            </div>
                                            <div class="col s12 m6">
                                                <a href="{{ $game->editGame() }}"
                                                   class="btn btn-full deep-orange white-text">
                                                    Edit game
                                                </a>
                                            </div>
                                        </div>
                                    </td>
                                    <td>{{ optional($game->awayTeam)->name }}</td>
                                </tr>
                            @endif
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </li>
        @endforeach
    </ul>
</div>
@else
    <div class="row section">
        <div class="col s12 m6 offset-m3">
            <div class="card">
                <div class="card-content">
                    <span class="card-title center-align">
                        Cargar juegos
                    </span>
                </div>
                <div class="card-action center">
                    <form action="{{ route('wizard.load.games', $season->uuid) }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="file-field input-field">
                            <div class="btn">
                                <span>Archivo CSV</span>
                                <input type="file" name="games">
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path validate" type="text">
                            </div>
                        </div>
                        <button type="submit" class="btn-full btn black white-text">
                            Guardar juegos
                        </button>
                        <p class="teal-text">
                            El proceso puede tardar unos minutos, por favor no refresques la pagina.
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endif
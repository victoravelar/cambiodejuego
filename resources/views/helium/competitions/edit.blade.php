@extends('helium.layout')
@section('content')
    <section class="section">
        <div class="container">
            <h4>Editar {{ $competition->name }}</h4>
            <form action="{{ route('competitions.update', $competition->uuid) }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <div class="row">
                    <div class="input-field col s12 m4">
                        <input required type="text" name="name" id="name" value="{{ $competition->name }}">
                        <label for="name">Nombre</label>
                    </div>
                    <div class="input-field col s12 m2">
                        <input required type="text" name="acronym" id="acronym" value="{{ $competition->acronym }}">
                        <label for="acronym">Acronimo</label>
                    </div>
                    <div class="input-field col s12 m2">
                        <input required type="number" name="total_games" id="total_games" value="{{ $competition->total_games }}">
                        <label for="total_games">Total de Juegos</label>
                    </div>
                    <div class="input-field col s12 m2">
                        <input required type="number" name="total_rounds" id="total_rounds" value="{{ $competition->total_rounds }}">
                        <label for="total_rounds">Total de Jornadas</label>
                    </div>
                    <div class="input-field col s12 m2">
                        <input required type="number" name="total_teams" id="total_teams" value="{{ $competition->total_teams }}">
                        <label for="total_teams">Número de equipos</label>
                    </div>
                    <div class="input-field col s12 m6">
                        <input required type="text" name="league_website" id="league_website" value="{{ $competition->league_website }}">
                        <label for="league_website">Sitio web</label>
                    </div>
                    <div class="file-field input-field col s12 m6">
                        <div class="btn">
                            <span>Logotipo de la competición</span>
                            <input type="file" name="league_logo">
                        </div>
                        <div class="file-path-wrapper">
                            <input class="file-path validate" type="text">
                        </div>
                    </div>
                    <p>
                        <input type="checkbox" class="filled-in" value="1" name="is_open" id="is_open" @if($competition->is_open) checked @endif>
                        <label for="is_open">La liga esta activa?</label>
                    </p>
                    <div class="col s12 m12">
                        <h5>Sección para CDJ (Lógica semantica)</h5>
                        <p class="row">
                            @foreach($sections as $section)
                                <span class="col s12 m4">
                                    <input type="radio" value="{{ (int) $section->id }}" name="section_id" id="{{ $section->slug.$section->id }}" @if($section->id === $competition->section_id) checked @endif>
                                    <label for="{{ $section->slug.$section->id }}">{{ $section->name }}</label>
                                </span>
                            @endforeach
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <button type="submit" class="btn black btn-full white-text">
                            Guardar Competición
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </section>
@endsection
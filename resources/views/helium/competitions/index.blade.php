@extends('helium.layout')

@section('content')
    <section class="section">
        <div class="container">
            <h3>Competiciones <a href="{{ route('competitions.create') }}" class="btn black white-text right">Agregar competición</a></h3>
        </div>
        <div class="container">
            @foreach($competitions->chunk(4) as $comps)
                <div class="row">
                    @foreach($comps as $c)
                        <div class="col s12 m3">
                            <div class="card">
                                <div class="card-content center">
                                    <img src="{{ $c->pathForLogo() }}" alt="{{ $c->name }}" height="60">
                                </div>
                                <div class="card-action cdj-blue center-align">
                                    <a class="white-text" href="{{ route('competitions.show', $c->uuid) }}">Ver</a>
                                    <a class="white-text" href="{{ route('competitions.edit', $c->uuid) }}">Editar</a>
                                    @if(!$c->isOpen())
                                        <span class="red white-text" style="padding: 2px">Cerrada</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            @endforeach
        </div>
    </section>
@endsection
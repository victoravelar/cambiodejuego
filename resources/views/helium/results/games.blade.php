@extends('helium.layout')

@section('content')
    <section class="section">
        <div class="container">
            <h3>Crear resultado para el juego: {{ $game->id }}</h3>
            <div class="card grey lighten-2">
                <div class="card-content">
                    <span class="card-title center-align">
                        <?php \Carbon\Carbon::setLocale(env('APP_LOCALE')); $carbon = new \Carbon\Carbon(); ?>
                        Fecha: {{ $carbon->parse($game->gameDate)->format('d-M-Y') }}
                    </span>
                    <div class="row">
                        <div class="col s6 center-align center">
                            <img src="{{ $game->homeTeam->pathForLogo() }}" alt="Local" height="50">
                            <p class="flow-text">
                                {{ $game->homeTeam->name }}
                            </p>
                        </div>
                        <div class="col s6 center-align center">
                            <img src="{{ $game->awayTeam->team_logo }}" alt="Local" height="50">
                            <p class="flow-text">
                                {{ $game->awayTeam->name }}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col s12 m6 offset-m3">
                    <form action="{{ $game->addResult() }}" method="post" autocomplete="off">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col s12 m6">
                                <div class="input-field">
                                    <input type="number" name="goals_home_total" value="{{ old('goals_home_total') }}" id="goals_home_total" required autofocus>
                                    <label for="goals_home_total">Goals local</label>
                                </div>
                            </div>
                            <div class="col s12 m6">
                                <div class="input-field">
                                    <input type="number" name="goals_away_total" value="{{ old('goals_away_total') }}" id="goals_away_total" required>
                                    <label for="goals_away_total">Goals away</label>
                                </div>
                            </div>
                            <div class="col s12">
                                <p class="red-text text-darken-1">Score at halftime (not mandatory)</p>
                            </div>
                            <div class="col s12 m6">
                                <div class="input-field">
                                    <input type="number" name="goals_home_halftime" value="{{ old('goals_home_halftime') }}" id="goals_home_halftime">
                                    <label for="goals_home_halftime">Goals local halftime</label>
                                </div>
                            </div>
                            <div class="col s12 m6">
                                <div class="input-field">
                                    <input type="number" name="goals_away_halftime" value="{{ old('goals_away_halftime') }}" id="goals_away_halftime">
                                    <label for="goals_away_halftime">Goals away halftime</label>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-full black white-text">
                                Set result
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
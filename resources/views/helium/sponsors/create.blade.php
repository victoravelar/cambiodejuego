@extends('helium.layout')

@section('content')
    <section class="section">
        <div class="container">
            <h4>Nuevo sponsor: {{ $section->name }}</h4>
            <div class="divider"></div>
            <form enctype="multipart/form-data" action="{{ route('sponsors.store', $section->slug) }}" method="post" autocomplete="off">
                {{ csrf_field() }}
                <div class="input-field">
                    <input type="text" name="name" id="name">
                    <label for="name">Nombre</label>
                </div>

                <div class="input-field">
                    <input type="text" name="link" id="link">
                    <label for="link">Enlace de redireccion</label>
                </div>

                <div class="input-field">
                    <select name="zone" id="zone">
                        <option disabled selected>Elige el area de display</option>
                        <option value="banner">Banner principal</option>
                        <option value="matches">Barra de partidos</option>
                        <option value="stats">Cuadro sobre stats</option>
                        {{--<option value="feed">Feed de noticias</option>--}}
                        {{--<option value="feed">Feed de noticias</option>--}}
                        {{--<option value="mobile-menu">Menu de moviles</option>--}}
                        {{--<option value="mobile-fitlers">Secciones en movil</option>--}}
                        {{--<option value="b-posts">Banner en posts</option>--}}
                        {{--<option value="c-posts">Cuadro en posts</option>--}}
                    </select>
                </div>

                <div class="input-field">
                    <input type="text" class="datepicker" name="valid_from" id="valid_from">
                    <label for="valid_from">Inicio del sponsorship</label>
                </div>

                <div class="input-field">
                    <input type="text" class="datepicker" name="valid_to" id="valid_to">
                    <label for="valid_to">Fin del sponsorship</label>
                </div>

                <div class="file-field input-field">
                    <div class="btn">
                        <span>Imagen</span>
                        <input type="file" name="media">
                    </div>
                    <div class="file-path-wrapper">
                        <input class="file-path validate" type="text">
                    </div>
                </div>

                <p>
                    <input type="checkbox" class="filled-in" id="filled-in-box" name="is_active" value="1"/>
                    <label for="filled-in-box">Activo</label>
                </p>
                <button type="submit" class="btn btn-full black white-text">
                    Guardar sponsor
                </button>
            </form>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        $('.datepicker').pickadate({
            selectMonths: true, // Creates a dropdown to control month
            selectYears: 3, // Creates a dropdown of 15 years to control year,
            today: 'Hoy',
            clear: 'Cancelar',
            close: 'Aceptar',
            closeOnSelect: true,
            format: 'yyyy-mm-d'// Close upon selecting a date,
        });
        $(document).ready(function() {
            $('select').material_select();
        });
    </script>
@endsection


@extends('helium.layout')

@section('content')
    <section class="section">
        <div class="container">
            <h4>Sponsors para: {{ $section->name }} <a class="btn black white-text right" href="{{ route('sponsors.create', $section->slug) }}">Agregar</a></h4>
            <div class="divider"></div>
            @if($sponsors->count() > 0)
                <ul class="collapsible" data-collapsible="accordion">
                    @foreach($sponsors as $sponsor)
                        <li>
                            <div class="collapsible-header"><i class="material-icons">stars</i> {{ $sponsor->name }}</div>
                            <div class="collapsible-body">
                                <div class="row">
                                    <div class="col s12">
                                        <div class="card z-depth-0">
                                            <div class="card-image">
                                                <img src="{{ $sponsor->mediaURI() }}" alt="LOGO">
                                            </div>
                                            <div class="card-content">
                                                <span class="card-title">Informacion</span>
                                                <p>
                                                    Inicio: {{ $sponsor->valid_from }}
                                                </p>
                                                <p>
                                                    Final: {{ $sponsor->valid_to }}
                                                </p>
                                                <p>
                                                    Zona de display: {{ $sponsor->zone }}
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    @endforeach
                </ul>
            @else
                <p class="red-text flow-text">No hay sponsors para esta seccion</p>
            @endif
        </div>
    </section>
@endsection


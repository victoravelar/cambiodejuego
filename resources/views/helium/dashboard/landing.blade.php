@extends('helium.layout')
@section('content')
    @php
        $activeUser = auth()->user();
        /** @var \Carbon\Carbon $carbon */
        $carbon = new \Carbon\Carbon;
    @endphp
    <section>
        <h3 class="center-align">Overview</h3>
        <div class="divider"></div>
        <div class="row">
            <div class="col s12 m4">
                @if(Voyager::can('delete_posts'))
                    @if($toReview->count() > 0)
                        <ul class="collection with-header">
                            <li class="collection-header cdj-blue lighten-2 white-text">
                                <h5>Pendientes por revisar</h5>
                            </li>
                            @foreach($toReview as $p)
                                <li class="collection-item clearfix">
                                    <a target="_blank" href="{{ $p->getPostURI() }}">{{ $p->title }}</a>
                                    <button class="right black white-text" onclick="document.getElementById('approval-content-form').submit()">
                                        Approve
                                    </button>
                                    <form action="{{ route('posts.approve', $p->slug) }}" id="approval-content-form" style="display: none" method="post">
                                        {{ method_field('PUT') }}
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            @endforeach
                        </ul>
                    @else
                        <div class="card cdj-blue white-text">
                            <div class="card-content">
                                   <div class="card-title">
                                       <i class="material-icons left">notes</i> 0 para revisar.
                                   </div>
                            </div>
                        </div>
                    @endif
                @endif
            </div>
            <div class="col s12 m4">
                @if($revision->count() > 0)
                    <ul class="collection with-header">
                        <li class="collection-header">
                            <h5>Tus notas en revision</h5>
                        </li>
                        @foreach($revision as $rev)
                            <li class="collection-item">
                                <a target="_blank" href="{{ $rev->getPostURI() }}">
                                    {{ $rev->title }}
                                    <span class="right black-text">
                                           {{ $carbon->parse($rev->updated_at)->diffForHumans() }}
                                       </span>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                @else
                    <div class="card cdj-blue white-text">
                        <div class="card-content">
                               <span class="card-title">
                                   <i class="material-icons left">notes</i> 0 pending for review.
                               </span>
                        </div>
                    </div>
                @endif
            </div>
            <div class="col s12 m4">
                @if($drafts->count() > 0)
                    <ul class="collection with-header">
                        <li class="collection-header">
                            <h5>Tus borradores</h5>
                        </li>
                        @foreach($drafts as $draft)
                            <li class="collection-item">
                                <a target="_blank" href="{{ $draft->getPostURI() }}">
                                    {{ $draft->title }}
                                    <span class="black-text">
                                           {{ $carbon->parse($draft->updated_at)->diffForHumans() }}
                                       </span>
                                    <button class="right black white-text" onclick="document.getElementById('review-content-form').submit()">
                                        Send for review
                                    </button>
                                    <form action="{{ route('posts.review', $draft->slug) }}" id="review-content-form" style="display: none" method="post">
                                        {{ method_field('PUT') }}
                                        {{ csrf_field() }}
                                    </form>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                @else
                    <div class="card cdj-blue white-text">
                        <div class="card-content">
                               <span class="card-title">
                                   <i class="material-icons left">notes</i> 0 drafts.
                               </span>
                        </div>
                    </div>
                @endif
            </div>
        </div>
        <div class="divider"></div>
        <div class="row">
            <div class="col s12">
                @if($pub->count() > 0)
                    <ul class="collection with-header">
                        <li class="collection-header cdj-blue">
                            <h5 class="white-text">Notas publicadas</h5>
                        </li>
                        @foreach($pub as $p)
                            <li class="collection-item">
                                <a href="{{ $p->getPostURI() }}" target="_blank">
                                    {{ $p->title }}
                                    <span class="right">
                                           {{ $carbon->parse($p->updated_at)->diffForHumans() }}
                                       </span>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                @else
                    <div class="card cdj-blue white-text">
                        <div class="card-content">
                               <span class="card-title">
                                   <i class="material-icons left">notes</i> 0 to review.
                               </span>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </section>
@endsection
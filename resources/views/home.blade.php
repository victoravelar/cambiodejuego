@extends('layouts.ctest')
@section('content')
    <section class="section">
        @include('ctest.space.header')
        <div class="container">
            <div class="row">
                <div class="col s12">
                    <ul class="tabs tabs-fixed-width">
                        <li class="tab col s4"><a href="#profile--league-table">{{ $profile->league->name }}</a></li>
                        <li class="tab col s4"><a href="#profile--uefa-team">{{ $profile->uefaTeam->shortName }}</a></li>
                        <li class="tab col s4 disabled"><a href="#profile--local-team">{{ $profile->localTeam->shortName }}</a></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                @include('ctest.space.league')
                @include('ctest.space.uefa')
            </div>
        </div>
    </section>
@endsection

@extends('layouts.ctest')
@section('content')
    <section class="section">
        <div class="container">
            <div class="row">
                <div class="col s12 m2">
                    <div id="pinme">
                        <ul class="section table-of-contents">
                            <li><a href="#noticias">Noticias</a></li>
                            <li><a href="#equipos">Equipos</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col s12 m10">
                    <div class="perfect-center">
                        <img height="150" src="{{ $competition->pathForLogo() }}" alt="{{ $competition->name }}">
                    </div>
                    <h3 class="cdj-blue white-text center-align" style="padding: 20px 0">
                        {{ $competition->name }}
                    </h3>
                    <div id="noticias" class="scrollspy">
                        <p class="flow-text cdj-blue white-text center-align">Notas</p>
                        @foreach($competition->section->posts->shuffle()->chunk(3) as $poster)
                            <div class="row">
                                @foreach($poster as $post)
                                <div class="col s12 m4">
                                    <div class="card">
                                        <div class="card-image">
                                            <img height="200" src="{{ $post->postMedia() }}" alt="MEDIA">
                                            <p class="card-title league-post" style="font-size: 15px">
                                                {{ $post->title }}
                                            </p>
                                        </div>
                                        <div class="card-action">
                                            <a href="{{ $post->getPostURI() }}" target="_blank" class="btn btn-full btn-flat cdj-blue white-text">
                                                Leer nota
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        @endforeach
                    </div>
                    <div id="equipos" class="scrollspy">
                        <p class="flow-text cdj-blue center-align white-text">
                            Participantes
                        </p>
                        @foreach($competition->seasons->last()->teams->shuffle()->chunk(3) as $teams)
                            <div class="row">
                                @foreach($teams as $team)
                                    <div class="col s12 m4 perfect-center">
                                        <img src="{{ $team->pathForLogo() }}" class="responsive-img" style="max-width: 70%;" alt="TEAM">
                                    </div>
                                @endforeach
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>

        </div>
    </section>
@endsection
@section('scripts')
    <script>
        $('.carousel.carousel-slider').carousel({fullWidth: true});
        $('.scrollspy').scrollSpy();
        $('#pinme').pushpin({
            top: 0,
            offset: 75
        });
    </script>
@endsection
<p class="flow-text center-align black white-text">
    Te puede interesar
</p>
@foreach($related as $article)
    <a class="card">
        <div class="card-image">
            <img height="200" src="{{ $article->postMedia() }}" alt="MEDIA">
            <p class="card-title league-post" style="font-size: 15px">
                {{ $article->title }}
            </p>
        </div>
        <div class="card-action">
            <a href="{{ $article->getPostURI() }}" target="_blank" class="btn btn-full btn-flat cdj-blue white-text">
                Leer nota
            </a>
        </div>
    </a>
    <br>
@endforeach
@extends('layouts.ctest')
@section('content')
    <section class="section">
        <div class="container">
            <div class="row">
                <div class="col s12 m8">
                    <div class="card z-depth-0">
                        @if($post->image !== null)
                        <div class="card-image">
                            <img src="{{ $post->postMedia() }}" alt="{{ $post->title }}" class="responsive-img materialboxed">
                        </div>
                        @endif
                        <div class="card-content">
                            <div class="center-align">
                                <span>
                                    @if(!is_null($post->caption)) Foto: {{ $post->caption }} @endif
                                </span>
                            </div>
                            <h3 class="justify-align">
                                {{ $post->title }}
                            </h3>
                            <div class="sharethis-inline-share-buttons"></div>
                            <br>
                            <div class="article-content--body center">
                                {!! $post->body !!}
                            </div>
                        </div>
                    </div>

                    <div class="fb-comments"
                         data-href="http://cambiodejuego.com.mx/nota/{{$post->slug}}"
                         data-width="100%"
                         data-numposts="10"
                         data-order-by="time"
                    ></div>

                </div>
                <div class="col s12 m4">
                    <ins class="adsbygoogle"
                         style="display:block"
                         data-ad-format="fluid"
                         data-ad-layout-key="-6t+ed+2i-1n-4w"
                         data-ad-client="ca-pub-1593486525661699"
                         data-ad-slot="4585426740"></ins>

                    <div class="divider"></div>

                    @include('website.related')

                    <p class="flow-text black white-text center-align">
                        De nuestra tienda
                    </p>
                    <article-products :products="6" />
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script type='text/javascript' src='//platform-api.sharethis.com/js/sharethis.js#property=59f9dbef30055e00123c06e9&product=inline-share-buttons' async='async'></script>
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <script>
        $(document).ready(function () {
            $('.card-content img').addClass('responsive-img hoverable');
            $('.card-content').addClass('article-content--body');
        });
    </script>
@endsection

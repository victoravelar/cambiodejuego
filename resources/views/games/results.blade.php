@extends('layouts.ctest')
@section('content')
    @php
        $comps = \App\Competition::all();
    @endphp
    <section class="section">
        <div class="container">
            <h3 class="center-align">Resultados</h3>
        </div>
        <div class="container">
            <div class="row">
                @foreach($results as $result)
                    <div class="col s12 m3">
                        <div class="card">
                            <div class="card-content game-result--display flex-c center">
                                <span class="truncate card-title center-align">
                                    <img src="{{ $result->season->competition->league_logo }}"
                                         alt="{{ $result->season->competition->acronym }}"
                                         height="32">
                                </span>
                                <div class="game-result--display">
                                    <img src="{{ $result->homeTeam->team_logo }}" alt="{{ $result->homeTeam->acronym }}" height="32">
                                    <span class="flow-text">{{ $result->homeTeam->shortName }}</span>
                                    <span class="flow-text">{{ $result->score['goals_home_total'] }}</span>
                                </div>
                                <div class="game-result--display">
                                    <img src="{{ $result->awayTeam->team_logo }}" alt="{{ $result->homeTeam->acronym }}" height="32">
                                    <span class="flow-text">{{ $result->awayTeam->shortName }}</span>
                                    <span class="flow-text">{{ $result->score['goals_away_total'] }}</span>
                                </div>
                            </div>
                            <div class="card-action cdj-blue">
                                <p class="center-align white-text">
                                    {{ Carbon\Carbon::parse($result->gameDate)->format('d-M-Y') }}
                                </p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="row">
                <div class="col s12 center">
                    {{ $results->links() }}
                </div>
            </div>
        </div>
    </section>
@endsection
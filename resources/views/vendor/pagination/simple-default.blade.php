@if ($paginator->hasPages())
    <ul class="pagination" style="display:flex;justify-content: space-between">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <li class="waves-effect grey lighten-3 disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
        @else
            {{--<li><a href="{{ $paginator->previousPageUrl() }}" rel="prev">@lang('pagination.previous')</a></li>--}}
            <li><a href="{{ $paginator->previousPageUrl() }}" class="cdj-blue white-text waves-light" rel="next">@lang('pagination.previous') <i class="material-icons left">chevron_left</i></a></li>
        @endif

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            {{--<li><a href="{{ $paginator->nextPageUrl() }}" rel="next">@lang('pagination.next')</a></li>--}}
            <li><a href="{{ $paginator->nextPageUrl() }}" class="cdj-blue white-text waves-light" rel="next">@lang('pagination.next') <i class="material-icons right">chevron_right</i></a></li>
        @else
            {{--<li class="disabled"><span>@lang('pagination.next')</span></li>--}}
            <li class="waves-effect grey lighten-3 disabled"><a href="#!"><i class="material-icons">chevron_right</i></a></li>
        @endif
    </ul>
@endif

<ul class="side-nav" id="mobile-demo">
    <div class="site-menu-header">

    </div>
    <li><a href="{{ route('games.results') }}">Resultados</a></li>
    @php
        $sections = \App\Section::all();
    @endphp

    @foreach($sections->where('active', true) as $section)
        <li><a href="{{ $section->path() }}" class="cdj-blue-text">{{ $section->name }}</a></li>
    @endforeach
    <li class="divider"></li>
    <div class="social-holder">
        <a href="{{ setting('site.social_facebook') }}" class="social facebook"><i class="fa fa-2x  center nav-icon fa-facebook-official"></i></a>
        <a href="{{ setting('site.social_twitter') }}" class="social twitter"><i class="fa fa-2x  center nav-icon fa-twitter-square"></i></a>
        <a href="{{ setting('site.social_instagram') }}" class="social instagram"><i class="fa fa-2x  center nav-icon fa-instagram"></i></a>
    </div>

    @include('ctest.navigation.login-button-mobile')
</ul>
@guest
    <li><a href="{{ route('login') }}" class="btn btn-flat green accent-4 white-text">Ingresar</a></li>
@else
    <li><a href="{{ route('logout') }}"
           class="btn btn-flat red white-text"
           onclick="event.preventDefault();
                         document.getElementById('logout-form').submit();">
            <i class="material-icons left white-text">highlight_off</i> Salir</a></a></li>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
    </form>
@endguest
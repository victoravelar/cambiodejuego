<ul class="left hide-on-med-and-down">
    <li class="nav-icon"><a target="_blank" href="{{ setting('site.social_facebook') }}"><i class="fa fa-facebook-official"></i></a></li>
    <li class="nav-icon"><a target="_blank" href="{{ setting('site.social_twitter') }}"><i class="fa fa-twitter-square"></i></a></li>
    <li class="nav-icon"><a target="_blank" href="{{ setting('site.social_instagram') }}"><i class="fa fa-instagram"></i></a></li>
</ul>

<ul class="right hide-on-med-and-down">
{{--    <li class="nav-icon"><a href="{{ setting('site.social_youtube') }}"><i class="fa fa-youtube-play"></i></a></li>--}}
    @auth
        @if(\Voyager::can('add_posts'))
            <li><a href="{{ route('helium.dashboard') }}">Helium</a></li>
        @else
            <li><a href="{{ route('home') }}">Mi espacio</a></li>
        @endif
    @endauth
    @include('ctest.navigation.login-button-regular')
</ul>
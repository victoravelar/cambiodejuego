@php
    if(Route::currentRouteName() === 'section.show') {
        $comps = $section->competitions;
    } else {
        $comps = \App\Competition::all();
    }
@endphp

<ul class="side-nav" id="filter-demo">
    <div class="site-filters-header">

    </div>
    <li>
        <a class="subheader">Filtros</a>
    </li>
    @foreach($comps->sortBy('section_id') as $comp)
        <li>
            <a class="valign-wrapper">
                <img src="{{ $comp->pathForLogo() }}"
                     class="team-logo">
                <span class="cdj-blue-text">
                    {{ $comp->name }}
                </span>
            </a>
        </li>
        <li class="divider"></li>
    @endforeach
</ul>
<div class="navbar-fixed">
    <nav>
        <div class="container">
            <div class="nav-wrapper">
                @include('ctest.navigation.brand')
                @include('ctest.navigation.mobile-menu-trigger')
                @include('ctest.navigation.regular-menu-links')
            </div>
        </div>
    </nav>
</div>
@include('ctest.navigation.mobile-menu-links')
@include('ctest.navigation.mobile-filters')

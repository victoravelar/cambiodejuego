<div class="container hide-on-small-and-down">
    <h3>Tus preferencias <a href="{{ route('profile.edit', $profile->id) }}" class="btn waves-effect waves-light right"><i class="material-icons">create</i></a></h3>
    <div class="row">
        @if (! is_null($profile))
            @if (! is_null($profile->league))
                <div class="col s12 m4">
                    <div class="card-panel center-align">
                        <div class="card-content">
                          <span class="card-title">
                              <img src="{{ $profile->league->league_logo }}" height="50" alt="{{ $profile->league->name }}">
                              <br>
                              {{ $profile->league->name }}
                          </span>
                        </div>
                    </div>
                </div>
            @endif
        @endif
        @if (! is_null($profile))
            @if (! is_null($profile->uefaTeam))
                <div class="col s12 m4">
                    <div class="card-panel center-align">
                        <div class="card-content">
                                    <span class="card-title center-align">
                                        <img src="{{ $profile->uefaTeam->team_logo }}" alt="{{ $profile->uefaTeam->name }}" height="50">
                                        <br>
                                        {{ $profile->uefaTeam->name }} <i class="fa fa-star yellow-text"></i>
                                    </span>
                        </div>
                    </div>
                </div>
            @endif
        @endif
        @if (! is_null($profile))
            @if (! is_null($profile->localTeam))
                <div class="col s12 m4">
                    <div class="card-panel center-align">
                        <div class="card-content">
                                    <span class="card-title center-align">
                                        <img src="{{ $profile->localTeam->team_logo }}" alt="{{ $profile->localTeam->name }}" height="50">
                                        <br>
                                        {{ $profile->localTeam->shortName }} <i class="fa fa-check-circle cyan-text"></i>
                                    </span>
                        </div>
                    </div>
                </div>
            @endif
        @endif
    </div>
</div>
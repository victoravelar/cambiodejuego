<div id="profile--league-table">
    <div class="col s12 center">
        @if(!$profile->league->detail->is_cup)
            <i class="fa fa-square cdj-blue-text"></i> Champions League |
            <i class="fa fa-square cdj-blue-text text-lighten-3"></i> Repezca de UCL |
            <i class="fa fa-square orange-text text-darken-2"></i> Europa League |
            <i class="fa fa-square deep-orange-text text-lighten-2"></i> Decenso
        @else
            <i class="fa fa-square cdj-blue-text"></i> Clasifican a la siguiente ronda
            <br>
        @endif
    </div>
    <div class="col s12">
        @if($boardType === 'league')
            <table class="bordered highlight centered">
                <thead>
                <tr>
                    <th>Rank</th>
                    <th>Equipo</th>
                    <th class="hide-on-med-and-down">JJ</th>
                    <th class="hide-on-med-and-down">JG</th>
                    <th class="hide-on-med-and-down">JP</th>
                    <th class="hide-on-med-and-down">JE</th>
                    <th class="hide-on-med-and-down">GF</th>
                    <th class="hide-on-med-and-down">GC</th>
                    <th>DG</th>
                    <th>Pts.</th>
                </tr>
                </thead>
                <tbody>
                @foreach($board->sortBy('position') as $entry)
                    <tr
                            @if($entry->position <= $profile->league->detail->champions_positions)
                            class="cdj-blue white-text"
                            @elseif($entry->position > $profile->league->detail->champions_positions
                              && $entry->position <= ($profile->league->detail->champions_positions +
                              $profile->league->detail->champions_rematch))
                            class="cdj-blue lighten-3 white-text"
                            @elseif($entry->position > $profile->league->detail->champions_positions
                              && $entry->position <= ($profile->league->detail->champions_positions +
                              $profile->league->detail->champions_rematch +
                              $profile->league->detail->europa_positions))
                            class="orange darken-2"
                            @elseif( $entry->position >= (count($competition->first()->teams) - $profile->league->detail->europa_positions))
                            class="deep-orange lighten-2"
                            @endif
                    >
                        <td>{{ $entry->position }}</td>
                        <td>
                            @if($entry->team->name === $profile->uefaTeam->name)
                                <i class="fa fa-star yellow-text"></i>
                            @elseif($entry->team->name === $profile->localTeam->name)
                                <i class="fa fa-check-circle cyan-text"></i>
                            @endif
                            {{ $entry->team->name }}
                        </td>
                        <td class="hide-on-med-and-down">{{ $entry->playedGames }}</td>
                        <td class="hide-on-med-and-down">{{ $entry->wins }}</td>
                        <td class="hide-on-med-and-down">{{ $entry->losses }}</td>
                        <td class="hide-on-med-and-down">{{ $entry->draws }}</td>
                        <td class="hide-on-med-and-down">{{ $entry->goals }}</td>
                        <td class="hide-on-med-and-down">{{ $entry->goalsAgainst }}</td>
                        <td>{{ $entry->goalDifference }}</td>
                        <td>{{ $entry->points }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @elseif($boardType !== 'league')
            @foreach($profile->league->board->groupBy('group') as $name => $group)
                <div class="col s12 m6">
                    <div class="card">
                        <div class="card-content">
                            <span class="card-title center-align">Grupo {{ $name }}</span>
                            <table class="bordered highlight centered">
                                <thead>
                                <tr>
                                    <th>Rank</th>
                                    <th>Equipo</th>
                                    <th>DG</th>
                                    <th>Pts</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($group->sortByDesc('points')->sortByDesc('goalDifference') as $entry)
                                    <tr
                                            @if($loop->iteration <= 2)
                                            class="cdj-blue white-text"
                                            @endif
                                    >
                                        <td>{{ $loop->iteration }}</td>
                                        <td>
                                            @if($entry->team->name === $profile->uefaTeam->name)
                                                <i class="fa fa-star yellow-text"></i>
                                            @elseif($entry->team->name === $profile->localTeam->name)
                                                <i class="fa fa-check-circle cyan-text"></i>
                                            @endif
                                            {{ $entry->team->name }}
                                        </td>
                                        <td>{{ $entry->goalDifference }}</td>
                                        <td>{{ $entry->points }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <br>
                </div>
            @endforeach
        @endif
    </div>
</div>
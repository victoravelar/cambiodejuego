<div id="profile--uefa-team">
    <div class="col s12 m6">
        <ul class="tabs tabs-fixed-width">
            <li class="tab"><a class="active" href="#next--games">Próximos</a></li>
            <li class="tab"><a href="#results-games">Resultados</a></li>
        </ul>
        <div id="next--games" class="col s12">
            @foreach($profile->uefaTeam->season->first()->games as $game)
                @if(!$game->finished && $game->homeTeam->id == $profile->uefa_team_id || !$game->finished && $game->awayTeam->id == $profile->uefa_team_id)
                    <div class="card grey lighten-2">
                        <div class="card-content">
                                            <span class="card-title center-align">
                                                <?php \Carbon\Carbon::setLocale(env('APP_LOCALE')); $carbon = new \Carbon\Carbon(); ?>
                                                {{ $carbon->parse($game->gameDate)->format('d-M-Y') }}
                                            </span>
                            <div class="row">
                                <div class="col s6 center-align center">
                                    <img src="{{ $game->homeTeam->team_logo }}" alt="Local" height="50">
                                    <p class="flow-text">
                                        {{ $game->homeTeam->shortName }}
                                    </p>
                                </div>
                                <div class="col s6 center-align center">
                                    <img src="{{ $game->awayTeam->team_logo }}" alt="Local" height="50">
                                    <p class="flow-text">
                                        {{ $game->awayTeam->shortName }}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            @endforeach
        </div>
        <div id="results-games" class="col s12">
            <table class="table bordered">
                <thead>
                <tr>
                    <td>Jornada</td>
                    <td>Fecha</td>
                    <td>Local</td>
                    <td>Visitante</td>
                </tr>
                </thead>
                @foreach($profile->uefaTeam->season->first()->games as $game)
                    @if($game->finished && $game->homeTeam->id == $profile->uefa_team_id || $game->finished && $game->awayTeam->id == $profile->uefa_team_id)
                        <tr>
                            <td>
                                {{ $game->matchDay }}
                            </td>
                            <td>
                                <?php \Carbon\Carbon::setLocale(env('APP_LOCALE')); $carbon = new \Carbon\Carbon(); ?>
                                {{ $carbon->parse($game->gameDate)->format('d-M-Y') }}
                            </td>
                            <td>
                                <p class="flow-text">
                                    <img src="{{ $game->homeTeam->team_logo }}" alt="Local" height="18">
                                    {{ $game->score['goals_home_total'] }}
                                </p>
                            </td>
                            <td>
                                <p class="flow-text">
                                    {{ $game->score['goals_away_total'] }}
                                    <img src="{{ $game->awayTeam->team_logo }}" alt="Local" height="18">
                                </p>
                            </td>
                        </tr>
                    @endif
                @endforeach
            </table>
        </div>
    </div>
    <div class="col s12 m6">
        <h5>Plantilla</h5>
        <table class="table bordered highlighted">
            <thead>
            <tr>
                <th>Nombre</th>
                <th class="hide-on-med-and-down">Posición</th>
                <th>Número</th>
                <th class="hide-on-med-and-down">Nacionalidad</th>
            </tr>
            </thead>
            <tbody>
            @foreach($profile->uefaTeam->players->sortBy('number') as $player)
                <tr>
                    <td>{{ $player->name }}</td>
                    <td class="hide-on-med-and-down">{{ $player->position }}</td>
                    <td class="center-align">{{ $player->number }}</td>
                    <td class="hide-on-med-and-down">{{ $player->nationality }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
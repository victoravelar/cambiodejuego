<div class="card z-depth-0 center">
    <div class="divider"></div>
    <div class="card-content">
        <span class="card-title grey-text text-lighten-1">
            <span>
                Cambio de Juego
            </span>
        </span>
        @include('ctest.footer.links')
        @include('ctest.footer.copy')
    </div>
</div>
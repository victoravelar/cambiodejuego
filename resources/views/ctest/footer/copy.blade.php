<div id="copyright">
    <div class="container">
        <p class="grey-text justify-align btn-full">
            <small>
                Grupo MAROTEC &copy; 2017 - {{ date('Y') }}
            </small>
        </p>
    </div>
</div>
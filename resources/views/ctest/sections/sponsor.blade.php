<div class="collection hide-on-med-and-down">
    <p class="flow-text center-align">
        Filtros
    </p>
    @foreach($section->competitions->sortBy('section_id') as $comp)
    <a class="valign-wrapper collection-item" href="#">
        <img src="{{ $comp->league_logo }}"
             height="22">
        <span class="cdj-blue-text">
            {{ $comp->name }}
        </span>
    </a>
    @endforeach
    <div class="site-filters-header">
        @if($section->sponsor->count() > 0)
            <a href="{{ $section->sponsor->link }}" class="collection-item">
                <img src="{{ $section->sponsor->mediaURI()  }}"
                     alt="SPONSORED CONTENT"
                     class="responsive-img">
            </a>
        @endif
    </div>
</div>
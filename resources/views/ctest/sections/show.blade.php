@extends('layouts.ctest')
@section('content')
    <home></home>
    <section class="banner">
        <div class="container-fluid">
            <div class="add">
                Anuncio
            </div>
        </div>
    </section>
    <section class="news">
        <div class="container-fluid">
            <div class="row">
                <div class="col s12 m3">
                    @include('ctest.landing.searchbox')
                    @include('ctest.sections.sponsor')
                </div>
                <div class="col s12 m6">
                    @if($posts->count() > 0)
                        @include('ctest.landing.blog')
                    @else
                        <div class="card large cdj-blue valign-wrapper">
                            <div class="card-content center">
                                <div class="white-text center-align">
                                    <i class="material-icons" style="font-size: 200px">sentiment_satisfied</i>
                                </div>
                                <p class="flow-text center-align white-text">
                                    Estamos calentando, el partido comezara pronto.
                                </p>
                            </div>
                        </div>
                    @endif

                </div>
                <div class="col m3 hide-on-med-and-down" style="position: sticky; top:0;">
                    <div class="card small grey darken-2 valign-wrapper">
                        <div class="card-content">
                          <span class="white-text center-align">
                            ANUNCIO ADSENSE
                          </span>
                        </div>
                    </div>
                    @include('ctest.sections.stats')
                </div>
            </div>
        </div>
    </section>
@endsection

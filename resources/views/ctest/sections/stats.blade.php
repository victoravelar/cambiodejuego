<div class="card league-table--stats" style="position: sticky; top:0;">
    <div class="card-content">
        <ul class="tabs tabs-fixed-width">
            @foreach($section->competitions->sortBy('section_id') as $comp)
                @if($comp->detail->is_cup == false)
                    <li class="tab">
                        <a href="#{{ $comp->acronym }}">
                            <img src="{{ $comp->league_logo }}"
                                 alt="Logo"
                                 height="24">
                        </a>
                    </li>
                @endif
            @endforeach
        </ul>
        @foreach($section->competitions->sortBy('section_id') as $comp)
            @if($comp->detail->is_cup == false)
                <div id="{{ $comp->acronym }}">
                    <table class="table bordered">
                        <thead>
                        <tr>
                            <th>Rank</th>
                            <th class="center-align">Equipo</th>
                            <th>Puntos</th>
                        </tr>
                        @foreach($comp->board->sortBy('position') as $entry)
                            <tr>
                                <td class="center-align">{{ $entry->position }}</td>
                                <td class="center-align"> {{ $entry->team->name }}</td>
                                <td class="right-align">{{ $entry->points }}</td>
                            </tr>
                        @endforeach
                        </thead>
                    </table>
                </div>
            @endif
        @endforeach
    </div>
</div>
<div class="card grey lighten-4 hide-on-small-and-down" style="position: sticky; top: 0;">
    <div class="card-content">
        <ul class="tabs tabs-fixed-width tabs-filters-height">
            <li class="tab col s3"><a href="#uefa-filter">
                    <img src="https://img.uefa.com/imgml/uefacom/elements/footer/uefa-logo-footer.png"
                         alt="UEFA"
                         height="32">
                </a></li>
            <li class="tab col s3"><a href="#fifa-filter">
                    <img src="https://upload.wikimedia.org/wikipedia/de/2/2c/FIFA_Logo_%2BSlogan.svg"
                            height="32"></a></li>
            <li class="tab col s3"><a href="#mx-filter">
                    <img src="http://www.femexfut.org.mx/img/Web_FMFLogoLuto2.png"
                         alt="Federación Mexicana"
                         height="32">
                </a></li>
        </ul>
        <div id="fifa-filter">
            <ul class="collection">
                @foreach($comps->sortBy('section_id') as $comp)
                    @if($comp->id == 10)
                        <li class="collection-item">
                            <span class="valign-wrapper">
                                <img src="{{ $comp->pathForLogo() }}"
                                     height="32" style="margin-right: 10px;">
                                <span class="hide-on-med-and-down">
                                    {{ $comp->name }}
                                </span>
                            </span>
                        </li>
                    @endif
                @endforeach
            </ul>
        </div>
        <div id="mx-filter">
            <ul class="collection">
                @foreach($comps->sortBy('section_id') as $comp)
                    @if($comp->id == 9)
                        <li class="collection-item">
                            <span class="valign-wrapper">
                                <img src="{{ $comp->pathForLogo() }}"
                                     height="32" style="margin-right: 10px;">
                                <span class="hide-on-med-and-down">
                                    {{ $comp->name }}
                                </span>
                            </span>
                        </li>
                    @endif
                @endforeach
            </ul>
        </div>
        <div id="uefa-filter">
            <ul class="collection">
                @foreach($comps->sortBy('section_id') as $comp)
                    @if($comp->id < 9)
                        <li class="collection-item">
                            <span class="valign-wrapper">
                                <img src="{{ $comp->pathForLogo() }}"
                                     height="32" style="margin-right: 10px;">
                                <span class="hide-on-med-and-down">
                                    {{ $comp->name }}
                                </span>
                            </span>
                        </li>
                    @endif
                @endforeach
            </ul>
        </div>
    </div>
</div>
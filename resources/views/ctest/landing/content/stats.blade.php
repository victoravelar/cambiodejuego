<div class="card league-table--stats" style="position: sticky; top:0;">
    <div class="card-content">
        <ul class="tabs tabs-fixed-width">
            @foreach($comps->sortBy('section_id') as $comp)
                @if($comp->acronym == 'PD' or $comp->acronym == 'PL' or $comp->acronym == 'BL1')
                    <li class="tab">
                        <a href="#{{ $comp->acronym }}">
                            <img src="{{ $comp->pathForLogo() }}"
                                 alt="Logo"
                                 height="24">
                        </a>
                    </li>
                @endif
            @endforeach
        </ul>
        @foreach($comps->sortBy('section_id') as $comp)
            <div id="{{ $comp->acronym }}">
                @if($comp->acronym == 'PD' or $comp->acronym == 'PL' or $comp->acronym == 'BL1')
                    @php $currentSeason = $comp->seasons->where('is_current', true)->first(); @endphp
                    <table class="bordered highlight centered">
                        <thead>
                        <tr>
                            <th>Rank</th>
                            <th>Equipo</th>
                            <th>Puntos</th>
                        </tr>
                        @foreach($currentSeason->board->sortBy('position') as $entry)
                            @php $competition = $currentSeason->competition @endphp
                            <tr>
                                <td class="center-align
                                    @if($loop->index + 1 <= $competition->detail->champions_positions)
                                    cdj-blue white-text
                                    @elseif($loop->index + 1 > $competition->detail->champions_positions
                                      && $loop->index + 1 <= ($competition->detail->champions_positions +
                                      $competition->detail->champions_rematch))
                                    cdj-blue lighten-3 white-text
                                    @elseif($loop->index + 1 > $competition->detail->champions_positions
                                      && $loop->index + 1 <= ($competition->detail->champions_positions +
                                      $competition->detail->champions_rematch +
                                      $competition->detail->europa_positions))
                                    orange darken-2
                                    @elseif( $loop->index + 1 >= (count($currentSeason->teams) - $competition->detail->europa_positions))
                                    red darken-4 white-text
                                    @endif
                                "
                                >{{ $loop->index + 1 }}</td>
                                <td class="center-align"> {{ optional($entry->team)->shortName }}</td>
                                <td class="center-align">{{ $entry->points }}</td>
                            </tr>
                        @endforeach
                        </thead>
                    </table>
                @endif
            </div>
        @endforeach
    </div>
</div>
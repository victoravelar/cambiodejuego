<div class="card">
    <div class="card-image">
        <img src="{{ $post->postMedia() }}"
             alt="{{ $post->seo_title }}"
             class="responsive-img">
    </div>
    <div class="card-content">
        <span class="card-title cdj-blue-text">
            {{ $post->seo_title }}
        </span>
        <p>
            {{ $post->excerpt }}
        </p>
    </div>
    <div class="card-action clearfix">
        <a href="{{ $post->getPostUri() }}" class="activator btn btn-flat cdj-blue white-text waves-effect waves-light">
            Leer nota
        </a>
        <span class="grey-text right">
            {{ $post->readableAgo() }}
        </span>
    </div>
</div>
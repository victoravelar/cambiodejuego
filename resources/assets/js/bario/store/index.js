import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        isLoading: false,
        settings: [],
        fixtures: {
            results: [],
            upcoming: [],
            timeline: []
        },
        feed: [],
        sections: {
            presentable: [],
            raw: []
        },
        sponsors: [],
        reducers: [],
        competitions: []
    },
    mutations: {
        HYDRATE_SETTINGS (state, payload) {
            state.settings = payload
        },
        HYDRATE_FIXTURES_UPCOMING (state, payload) {
            state.fixtures.upcoming = payload
        },
        HYDRATE_FIXTURES_RESULTS (state, payload) {
            state.fixtures.results = payload
        },
        HYDRATE_FIXTURES_TIMELINE (state, payload) {
            state.fixtures.timeline = payload
        },
        FILL_FEED (state, payload) {
            state.feed = payload
        },
        GET_SECTIONS_FILTERS (state, payload) {
            state.sections.presentable = payload
        },
        GET_RAW_SECTIONS (state, payload) {
            state.sections.raw = payload
        },
        GET_SPONSORS_FOR_VIEW (state, payload) {
            state.sponsors = payload
        },
        ADD_FILTER (state, payload) {
            state.reducers.push(payload)
        },
        REMOVE_FILTER (state, payload) {
            let i = state.reducers.indexOf(payload);
            state.reducers.splice(i, 1)
        },
        REMOVE_ALL_FILTERS (state) {
            state.reducers = []
        },
        TOGGLE_LOADING (state) {
            state.isLoading = !state.isLoading
        },
        HYDRATE_COMPETITIONS_INFO (state, payload) {
            state.competitions = payload
        }
    },
    getters: {
        getSocialNetworks: state => {
            return state.settings.filter(s => s.key.match(/social/))
        },
        getCompetitions: state => state.competitions,
        getLeagues: state => {
            return state.competitions.filter(competition => {
                return competition.info.cup === false
            })
        }
    },
    actions: {
        getSettings ({ commit }) {
            axios.get('/api/v2/settings')
                .then(res => {
                    commit('HYDRATE_SETTINGS', res.data['data'])
                })
                .catch(err => {
                    console.log(err)
                })
        },
        getUpcomingFixtures ({ commit }) {
            axios.get('/api/v2/fixtures/upcoming')
                .then(res => {
                    commit('HYDRATE_FIXTURES_UPCOMING', res.data)
                })
                .catch(err => {
                    console.log(err)
                })
        },
        getResults ({ commit }) {
            axios.get('/api/v2/fixtures/results')
                .then(res => {
                    commit('HYDRATE_FIXTURES_RESULTS', res.data)
                })
                .catch(err => {
                    console.log(err)
                })
        },
        getTimeline ({ commit }) {
            axios.get('/api/v2/fixtures/timeline')
                .then(res => {
                    commit('HYDRATE_FIXTURES_TIMELINE', res.data)
                })
                .catch(err => {
                    console.log(err)
                })
        },
        getFeedContent ({ commit }) {
            axios.get('/api/v2/feed')
                .then(res => {
                    commit('FILL_FEED', res.data)
                })
                .catch(err => {
                    console.log(err)
                })
        },
        getPresentableSections ({ commit }) {
            axios.get('/api/v2/sections/render')
                .then(res => {
                    commit('GET_SECTIONS_FILTERS', res.data)
                })
                .catch(err => {
                    console.log(err)
                })
        },
        getRawSections ({ commit }) {
            axios.get('/api/v2/sections')
                .then(res => {
                    commit('GET_RAW_SECTIONS', res.data)
                })
                .catch(err => {
                    console.log(err)
                })
        },
        getSponsors ({ commit }) {
            axios.get('/api/v2/sponsors')
                .then(res => {
                    commit('GET_SPONSORS_FOR_VIEW', res.data)
                })
                .catch(err => {
                    console.log(err)
                })
        },
        getCompetitions ({ commit }) {
            axios.get('/api/v2/boards')
                .then(res => {
                    commit('HYDRATE_COMPETITIONS_INFO', res.data)
                })
                .catch(err => {
                    console.log(err)
                })
        }
    }
})

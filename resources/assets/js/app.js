
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('home', require('./components/Home.vue'));
Vue.component('local-teams', require('./components/Profile/LocalTeams.vue'));
Vue.component('uefa-teams', require('./components/Profile/UefaTeams.vue'));
Vue.component('leagues', require('./components/Profile/Leagues.vue'));
Vue.component('article-products', require('./components/Products/ArticleList.vue'));

import Engine from './components/Helium/Engine.vue';
import Feed from './components/Feed.vue';
import Seasons from './components/Wizards/seasons/Seasons.vue';

import store from './bario/store';

import { mapState, mapActions, mapGetters } from 'vuex';
import Ads from 'vue-google-adsense'

Vue.use(require('vue-script2'))

Vue.use(Ads.Adsense)
Vue.use(Ads.InArticleAdsense)
Vue.use(Ads.InFeedAdsense)

const app = new Vue({
    el: '#app',
    store,
    components: {
        Engine, Feed, Seasons
    },
    computed: {
        ...mapState(['settings', 'sections']),
        ...mapGetters(['getSocialNetworks'])
    },
    methods: {
        ...mapActions([
            'getSettings',
            'getResults',
            'getUpcomingFixtures',
            'getTimeline',
            'getFeedContent',
            'getPresentableSections',
            'getRawSections',
            'getSponsors',
            'getCompetitions'
        ]),
        giveMeThisSocialNetwork (network) {
            return this.getSocialNetworks.find(net => net.key === network)
        }
    },
    created () {
        this.getSettings()
        this.getFeedContent()
        this.getPresentableSections()
        this.getRawSections()
        this.getSponsors()
        this.getCompetitions()
    }
});

require('./pages/theme');